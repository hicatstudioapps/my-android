package com.lacostra.utils.notification.Permission;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.ArrayList;
import  static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.WRITE_CONTACTS;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.READ_CALL_LOG;
import static android.Manifest.permission.WRITE_CALL_LOG;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
/**
 * Created by CQ on 01/03/2016.
 */
public class PermissionActivity extends AppCompatActivity {

    private final static int COARSE_LOCATION_RESULT = 100;
    private final static int FINE_LOCATION_RESULT = 101;
    private final static int CALL_PHONE_RESULT = 102;
    private final static int CAMERA_RESULT = 103;
    private final static int WRITE_CALL_LOG_RESULT = 104;
    private final static int WRITE_CONTACS_RESULT=105;
    private final static int READ_PHONE_STATE_RESULT = 106;
    private final static int READ_CALL_LOG_RESULT = 107;
    private final static int WRITE_EXTERNAL_RESULT = 108;
    private final static int READ_EXTERNAL_RESULT = 109;
    private final static int RECORD_AUDIO_RESULT = 110;
    private final static int READ_CONTACT_RESULT=112;
    private final static int ALL_PERMISSIONS_RESULT = 111;

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String>  permissionsRejected;

    private View root;


    /**
     * This is the method that is hit after the user accepts/declines the
     * permission you requested. For the purpose of this example I am showing a "success" header
     * when the user accepts the permission and a snackbar when the user declines it.  In your application
     * you will want to handle the accept/decline in a way that makes sense.
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode){
            case FINE_LOCATION_RESULT:
                if(Permission.hasPermission(this, ACCESS_FINE_LOCATION)){
                   // permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(ACCESS_FINE_LOCATION);
                    Permission.makePostRequestSnack(this,root,permissionsRejected);
                }
                break;

            case WRITE_CONTACS_RESULT:
                if(Permission.hasPermission(this, WRITE_CONTACTS)){
                    // permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(WRITE_CONTACTS);
                    Permission.makePostRequestSnack(this,root,permissionsRejected);
                }
                break;
            case READ_CONTACT_RESULT:
                if(Permission.hasPermission(this, READ_CONTACTS)){
                    // permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(READ_CONTACTS);
                    Permission.makePostRequestSnack(this,root,permissionsRejected);
                }
                break;
            case READ_PHONE_STATE_RESULT:
                if(Permission.hasPermission(this, READ_PHONE_STATE)){
                    // permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(READ_PHONE_STATE);
                    Permission.makePostRequestSnack(this,root,permissionsRejected);
                }
                break;
            case READ_CALL_LOG_RESULT:
                if(Permission.hasPermission(this, READ_CALL_LOG)){
                    // permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(READ_CALL_LOG);
                    Permission.makePostRequestSnack(this,root,permissionsRejected);
                }
                break;

            case COARSE_LOCATION_RESULT:
                if(Permission.hasPermission(this,ACCESS_COARSE_LOCATION)){
                   // permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(ACCESS_COARSE_LOCATION);
                    Permission.makePostRequestSnack(this, root, permissionsRejected);
                }
                break;
            case CALL_PHONE_RESULT:
                if(Permission.hasPermission(this,CALL_PHONE)){
                 //   permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(CALL_PHONE);
                    Permission.makePostRequestSnack(this, root, permissionsRejected);
                }
                break;
            case CAMERA_RESULT:
                if(Permission.hasPermission(this,CAMERA)){
                   // permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(CAMERA);
                    Permission.makePostRequestSnack(this, root, permissionsRejected);
                }
                break;
            case WRITE_CALL_LOG_RESULT:
                if(Permission.hasPermission(this,WRITE_CALL_LOG)){
                  //  permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(WRITE_CALL_LOG);
                    Permission.makePostRequestSnack(this, root, permissionsRejected);
                }
                break;
            case WRITE_EXTERNAL_RESULT:
                if(Permission.hasPermission(this,WRITE_EXTERNAL_STORAGE)){
                  //  permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(WRITE_EXTERNAL_STORAGE);
                    Permission.makePostRequestSnack(this, root, permissionsRejected);
                }
                break;
            case READ_EXTERNAL_RESULT:
                if(Permission.hasPermission(this,READ_EXTERNAL_STORAGE)){
                    //  permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(READ_EXTERNAL_STORAGE);
                    Permission.makePostRequestSnack(this, root, permissionsRejected);
                }
                break;
            case RECORD_AUDIO_RESULT:
                if(Permission.hasPermission(this,RECORD_AUDIO)){
                  //  permissionSuccess.setVisibility(View.VISIBLE);
                }else{
                    permissionsRejected.add(RECORD_AUDIO);
                    Permission.makePostRequestSnack(this, root, permissionsRejected);
                }
                break;
            case ALL_PERMISSIONS_RESULT:
                boolean someAccepted = false;
                boolean someRejected = false;
                for(String perms : permissionsToRequest){
                    if(Permission.hasPermission(this,perms)){
                        someAccepted = true;
                    }else{
                        someRejected = true;
                        permissionsRejected.add(perms);
                    }
                }

                if(permissionsRejected.size()>0){
                    someRejected = true;
                }

                if(someAccepted){
                    //permissionSuccess.setVisibility(View.VISIBLE);
                }
                if(someRejected){
                    Permission.makePostRequestSnack(this, root, permissionsRejected);
                }
                break;
        }
    }

    public void askPermision(ArrayList<String> perms,int code){
        //filter out the permissions we have already accepted
        if(Permission.canMakeSmores()){
        permissionsToRequest = Permission.findUnAskedPermissions(this, perms);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = Permission.findRejectedPermissions(this, perms);

        if(permissionsToRequest.size()>0){//we need to ask for permissions
            //but have we already asked for them?
            requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), code);
            //mark all these as asked..
            for(String perm : permissionsToRequest){
                Permission.markAsAsked(this, perm);
            }
        }else{
            //show the success banner
            if(permissionsRejected.size()< perms.size()){
                //this means we can show success because some were already accepted.
               // permissionSuccess.setVisibility(View.VISIBLE);
            }

//            if(permissionsRejected.size()>0){
//                //we have none to request but some previously rejected..tell the user.
//                //It may be better to show a dialog here in a prod application
//                Snackbar.make(root, String.valueOf(permissionsRejected.size()) + " permission(s) were previously rejected", Snackbar.LENGTH_LONG)
//                        .setAction("Allow to Ask Again", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                for(String perm: permissionsRejected){
//                                    Permission.clearMarkAsAsked(getApplicationContext(), perm);
//                                }
//                            }
//                        })
//                        .show();
//            }
     }}
    }
}
