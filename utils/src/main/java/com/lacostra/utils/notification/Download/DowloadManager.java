package com.lacostra.utils.notification.Download;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;

/**
 * Created by CQ on 25/02/2016.
 */
public class DowloadManager  {

    private DownloadManager manager;
    private Context context;

    public DowloadManager(Context context) {
        manager= (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
    }

    public boolean isDownLoading(long id){
        DownloadManager.Query query= new DownloadManager.Query();
        query.setFilterById(id);
        Cursor cursor= manager.query(query);
        if(cursor.moveToFirst()){
            int status= cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
            if(status==DownloadManager.STATUS_RUNNING || DownloadManager.STATUS_PENDING==status)
                return true;
        }
        return false;
    }

    public void download(Uri url, String title,String description){
        DownloadManager.Request request= new DownloadManager.Request(url);
        request.setDestinationUri(Uri.parse(Environment.DIRECTORY_DOWNLOADS));
        request.setTitle(title);
        request.setDescription(description);
        request.setVisibleInDownloadsUi(true);
        manager.enqueue(request);
    }
}
