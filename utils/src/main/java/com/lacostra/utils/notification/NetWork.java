package com.lacostra.utils.notification;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by CQ on 26/02/2016.
 */
public class NetWork {

    public static boolean checkConnectivity(Context context) {
        boolean var1 = true;
        NetworkInfo var2 = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (var2 == null || !var2.isConnected() || !var2.isAvailable()) {
            var1 = false;
        }
        return var1;
    }
}
