package com.lacostra.utils.notification.Permission;

/**
 * Created by CQ on 01/03/2016.
 */

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.view.View;

import java.util.ArrayList;
import  static android.Manifest.permission.ACCESS_COARSE_LOCATION;
        import static android.Manifest.permission.ACCESS_FINE_LOCATION;
        import static android.Manifest.permission.CALL_PHONE;
        import static android.Manifest.permission.CAMERA;
        import static android.Manifest.permission.READ_CONTACTS;
        import static android.Manifest.permission.WRITE_CONTACTS;
        import static android.Manifest.permission.READ_PHONE_STATE;
        import static android.Manifest.permission.READ_CALL_LOG;
        import static android.Manifest.permission.WRITE_CALL_LOG;
        import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
        import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
        import static android.Manifest.permission.RECORD_AUDIO;

public class Permission {

    private final static int COARSE_LOCATION_RESULT = 100;
    private final static int FINE_LOCATION_RESULT = 101;
    private final static int CALL_PHONE_RESULT = 102;
    private final static int CAMERA_RESULT = 103;
    private final static int WRITE_CALL_LOG_RESULT = 104;
    private final static int WRITE_CONTACS_RESULT=105;
    private final static int READ_PHONE_STATE = 106;
    private final static int READ_CALL_LOG_RESULT = 107;
    private final static int WRITE_EXTERNAL_RESULT = 108;
    private final static int READ_EXTERNAL_RESULT = 109;
    private final static int RECORD_AUDIO_RESULT = 110;
    private final static int ALL_PERMISSIONS_RESULT = 111;

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String>  permissionsRejected;


    /**
     * method that will return whether the permission is accepted. By default it is true if the user is using a device below
     * version 23
     * @param permission
     * @return
     */
    public static boolean hasPermission(Context context,String permission) {
        if (canMakeSmores()) {
            return(context.checkSelfPermission(permission)== PackageManager.PERMISSION_GRANTED);
        }
        return true;
    }

    /**
     * method to determine whether we have asked
     * for this permission before.. if we have, we do not want to ask again.
     * They either rejected us or later removed the permission.
     * @param permission
     * @return
     */
    public static boolean shouldWeAsk(Context context,String permission) {
        return(PreferenceManager.getDefaultSharedPreferences(context).getBoolean(permission, true));
    }

    /**
     * we will save that we have already asked the user
     * @param permission
     */
    public static void markAsAsked(Context context,String permission) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(permission, false).apply();
    }

    /**
     * We may want to ask the user again at their request.. Let's clear the
     * marked as seen preference for that permission.
     * @param permission
     */
    public static void clearMarkAsAsked(Context context,String permission) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(permission, true).apply();
    }


    /**
     * This method is used to determine the permissions we do not have accepted yet and ones that we have not already
     * bugged the user about.  This comes in handle when you are asking for multiple permissions at once.
     * @param wanted
     * @return
     */
    public static ArrayList<String> findUnAskedPermissions(Context context,ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(context,perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * this will return us all the permissions we have previously asked for but
     * currently do not have permission to use. This may be because they declined us
     * or later revoked our permission. This becomes useful when you want to tell the user
     * what permissions they declined and why they cannot use a feature.
     * @param wanted
     * @return
     */
    public static ArrayList<String> findRejectedPermissions(Context context,ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(context,perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * Just a check to see if we have marshmallows (version 23)
     * @return
     */
    public static boolean canMakeSmores() {
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * a method that will centralize the showing of a snackbar
     */
    public static void makePostRequestSnack(final Context context,View view, final ArrayList<String> permissionsRejected){
//        Snackbar.make(view, String.valueOf(permissionsRejected.size()) + " permission(s) were rejected", Snackbar.LENGTH_LONG)
//                .setAction("Allow to Ask Again", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        for(String perm: permissionsRejected){
//                            clearMarkAsAsked(context,perm);
//                        }
//                    }
//                })
//                .show();
    }
}
