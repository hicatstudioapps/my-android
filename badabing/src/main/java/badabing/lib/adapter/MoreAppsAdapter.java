package badabing.lib.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import badabing.lib.R;
import badabing.lib.model.MoreAppsModel;

//import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

/**
 * Class that implements a default adapter for the "More Apps" functionality
 * 
 * @author Junior Buckeridge A.
 *
 */
public class MoreAppsAdapter extends BaseAdapter {

	/**
	 * The context of this adapter
	 */
	private Context context;
	/**
	 * The app models of this adapter
	 */
	private ArrayList<MoreAppsModel> list;
	/**
	 * The default typeface for the adapter
	 */
	private Typeface typeface;
	/**
	 * The default drawable for the adapter
	 */
	private int defaultDrawableRes;
	/**
	 * The color of the title text
	 */
	private int colorTitle = Color.BLACK;
	/**
	 * The color of the description text
	 */
	private int colorDescription = Color.BLACK;
	
	/**
	 * The textview title size
	 */
	private float sizeTitle = 0;
	/**
	 * The textview description size
	 */
	private float sizeDesc = 0;
	private DisplayImageOptions options;
	
	private static class ViewHolder {
		public TextView tvTitle;
		public TextView tvDesc;
		public ImageView ivIcon;
	}
	
	public MoreAppsAdapter(Context context, ArrayList<MoreAppsModel> list) {
		this(context, list, 0);
	}
	
	public MoreAppsAdapter(Context context, ArrayList<MoreAppsModel> list, int defaultDrawableRes) {
		this(context, list, defaultDrawableRes, null); 
	}

	public MoreAppsAdapter(Context context, ArrayList<MoreAppsModel> list, int defaultDrawableRes, Typeface typeface) {
		
		this.context = context;
		this.list = list;
		this.defaultDrawableRes = defaultDrawableRes; 
		this.typeface = typeface;
		this.options = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.showImageOnLoading(this.defaultDrawableRes)
		.showImageForEmptyUri(this.defaultDrawableRes)
		.showImageOnFail(this.defaultDrawableRes)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
		
	}

	@Override
	public int getCount() {
		try {
			return list.size();
		} catch (Exception e) {
		}
		return 0;
	}

	@Override
	public MoreAppsModel getItem(int position) {
		try {
			list.get(position);
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		
		final MoreAppsModel app = list.get(position);
		
		if(view == null){
			view = View.inflate(context, R.layout.view_more_apps, null);
			
			final ViewHolder holder = new ViewHolder();
			
			holder.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
			holder.tvDesc = (TextView) view.findViewById(R.id.tvDescription);
			holder.ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
			
			view.setTag(holder);
		}
		
		final ViewHolder holder = (ViewHolder) view.getTag();	
		
		holder.tvTitle.setTextColor(colorTitle);
		holder.tvTitle.setText(app.getAppName());
		holder.tvDesc.setTextColor(colorDescription);
		holder.tvDesc.setText(Html.fromHtml(app.getAppDescription()));
		if(typeface != null){
			holder.tvTitle.setTypeface(typeface);
			holder.tvDesc.setTypeface(typeface);
		}
		if(sizeTitle > 0){
			holder.tvTitle.setTextSize(sizeTitle);
		}
		if(sizeDesc > 0){
			holder.tvDesc.setTextSize(sizeDesc);
		}
		final String url = app.getAppImageUrl();
		Log.d("url",url);
		ImageLoader.getInstance()
		.displayImage(url, holder.ivIcon, options, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingStarted(String imageUri, View view) {
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
			}

			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			}
		});
//		Glide.with(context).load(url).placeholder(this.defaultDrawableRes).into(holder.ivIcon);
		view.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(app.getAppUrl())));
				
			}
		});
		
		return view;
	}

	/**
	 * Convenience method to update the backing array list of the adapter
	 * 
	 * @param list The new list to establish into this adapter
	 */
	public void updateList(ArrayList<MoreAppsModel> list) {
		this.list = list;
		notifyDataSetChanged();
	}

	public int getColorTitle() {
		return colorTitle;
	}

	public void setColorTitle(int colorTitle) {
		this.colorTitle = colorTitle;
	}

	public int getColorDescription() {
		return colorDescription;
	}

	public void setColorDescription(int colorDescription) {
		this.colorDescription = colorDescription;
	}

	public Typeface getTypeface() {
		return typeface;
	}

	/**
	 * Sets the default typeface and updates the adapter
	 * @param typeface
	 */
	public void setTypeface(Typeface typeface) {
		this.typeface = typeface;
		
		notifyDataSetChanged();
	}

	public float getSizeTitle() {
		return sizeTitle;
	}

	/**
	 * Sets the size of the title textview and updates the adapter
	 * @param sizeTitle
	 */
	public void setSizeTitle(float sizeTitle) {
		this.sizeTitle = sizeTitle;
		
		notifyDataSetChanged();
	}

	public float getSizeDesc() {
		return sizeDesc;
	}

	/**
	 * Sets the size of the desc textview and updates the adapter
	 * @param sizeDesc
	 */
	public void setSizeDesc(float sizeDesc) {
		this.sizeDesc = sizeDesc;
		
		notifyDataSetChanged();
	}

	
}
