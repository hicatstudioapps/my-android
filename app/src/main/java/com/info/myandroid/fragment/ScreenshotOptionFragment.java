package com.info.myandroid.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.R;
import com.info.myandroid.utils.AppConstants;
import com.info.myandroid.utils.AppUtil;

import java.util.Locale;

public class ScreenshotOptionFragment extends Fragment {
   private Activity activity;
   private ImageView imageViewScreenshotImage;
   private TextView textViewOption;

   public static Fragment getInstance(int var0) {
      ScreenshotOptionFragment var1 = new ScreenshotOptionFragment();
      Bundle var2 = new Bundle();
      var2.putInt("SCREENSHOT_FRAGMENT_POSITION", var0);
      var1.setArguments(var2);
      return var1;
   }

   private void initializeView() {
      View var1 = this.getView();
      this.imageViewScreenshotImage = (ImageView)var1.findViewById(R.id.imageViewScreenshotImage);
      this.textViewOption = (TextView)var1.findViewById(R.id.textViewOption);
      this.prepareScreenshotOptionView(this.getArguments().getInt("SCREENSHOT_FRAGMENT_POSITION"));
   }

   private void prepareScreenshotOptionView(int var1) {
      ++var1;
      String var2 = AppConstants.SCREENSHOT_OPTIONS_STACK[var1 - 1];
      int var3 = AppUtil.getImageResourceId("screenshot_option_" + var2);
      this.imageViewScreenshotImage.setImageResource(var3);
      if(AppUtil.isSupportedLanguage(Locale.getDefault())) {
         var2 = AppUtil.toLocaleBasedNumberConversion(var1);
      } else {
         var2 = String.valueOf(var1);
      }

      this.textViewOption.setText(var2);
   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.initializeView();
   }

   public void onAttach(Context var1) {
      super.onAttach(var1);
      this.activity = (HomeActivity)var1;
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.screenshot_option_screen_section, (ViewGroup)null);
   }
}
