package com.info.myandroid.fragment;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.MyAndroidApplication;
import com.info.myandroid.R;
import com.info.myandroid.utils.AppUtil;

import java.util.Properties;

@SuppressWarnings("ResourceType")
public class ScreenDensityFragment extends MasterFragment {
   private AdView adView;
   private AnimationRunnable animationRunnable = new AnimationRunnable((AnimationRunnable)null);
   private int deviceDensity = 0;
   private ImageView imageViewArrow;
   private LinearLayout linearLayoutHighDensity;
   private LinearLayout linearLayoutLowDensity;
   private LinearLayout linearLayoutMediumDensity;
   private LinearLayout linearLayoutXHighDensity;
   private LinearLayout linearLayoutXXHighDensity;
   private Animation.AnimationListener setDensityAnimationListener = new Animation.AnimationListener() {
      public void onAnimationEnd(Animation var1) {
         ScreenDensityFragment.this.setDensitySelection();
      }

      public void onAnimationRepeat(Animation var1) {
      }

      public void onAnimationStart(Animation var1) {
      }
   };
   private TextView textViewHighDensity;
   private TextView textViewHighDensityValue;
   private TextView textViewLowDensity;
   private TextView textViewLowDensityValue;
   private TextView textViewMediumDensity;
   private TextView textViewMediumDensityValue;
   private TextView textViewXHighDensity;
   private TextView textViewXHighDensityValue;
   private TextView textViewXXHighDensity;
   private TextView textViewXXHighDensityValue;

   private int getDeviceDensity() {
      DisplayMetrics var1 = new DisplayMetrics();
      int var2;
      if(this.deviceDensity == 0) {
         this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(var1);
         var2 = var1.densityDpi;
      } else {
         var2 = this.deviceDensity;
      }

      return var2;
   }

   public static ScreenDensityFragment getInstance() {
      return new ScreenDensityFragment();
   }

   private void initView() {
      View var1 = this.getView();
      this.imageViewArrow = (ImageView)var1.findViewById(R.id.imageViewArrow);
      this.linearLayoutLowDensity = (LinearLayout)var1.findViewById(R.id.llLowDensity);
      this.linearLayoutMediumDensity = (LinearLayout)var1.findViewById(R.id.llMediumDensity);
      this.linearLayoutHighDensity = (LinearLayout)var1.findViewById(R.id.llHighDensity);
      this.linearLayoutXHighDensity = (LinearLayout)var1.findViewById(R.id.llXHighDensity);
      this.linearLayoutXXHighDensity = (LinearLayout)var1.findViewById(R.id.llXXHighDensity);
      this.textViewLowDensityValue = (TextView)var1.findViewById(R.id.textViewLowDensityValue);
      this.textViewMediumDensityValue = (TextView)var1.findViewById(R.id.textViewMediumDensityValue);
      this.textViewHighDensityValue = (TextView)var1.findViewById(R.id.textViewHighDensityValue);
      this.textViewXHighDensityValue = (TextView)var1.findViewById(R.id.textViewXHighDensityValue);
      this.textViewXXHighDensityValue = (TextView)var1.findViewById(R.id.textViewXXHighDensityValue);
      this.textViewLowDensity = (TextView)var1.findViewById(R.id.textViewLowDensity);
      this.textViewMediumDensity = (TextView)var1.findViewById(R.id.textViewMediumDensity);
      this.textViewHighDensity = (TextView)var1.findViewById(R.id.textViewHighDensity);
      this.textViewXHighDensity = (TextView)var1.findViewById(R.id.textViewXHighDensity);
      this.textViewXXHighDensity = (TextView)var1.findViewById(R.id.textViewXXHighDensity);
      this.animationRunnable.duration = 1000;
      this.adView = null;
   }

   private void moveViewToPosition(View var1, int var2, int var3) {
      TranslateAnimation var4 = new TranslateAnimation(0.0F, 0.0F, 0.0F, (float)var2);
      var4.setDuration((long)var3);
      var4.setFillAfter(true);
      var4.setAnimationListener(this.setDensityAnimationListener);
      var1.startAnimation(var4);
   }

   private void onDensitySelectionChanges(LinearLayout var1, TextView var2, TextView var3,int id) {
      int var4 = this.getDeviceDensity();
//      ObjectAnimator anim = ObjectAnimator.of(var1, "alpha",0f , 1f);
//      anim.setDuration(1000);
//      anim.start();
//      var1.setBackgroundColor();
      var1.setBackgroundResource(R.drawable.rounded_corners_yellow_color_background);
      var2.setTypeface((Typeface) null, 1);
      var3.setVisibility(0);
      var3.setText("(" + AppUtil.toLocaleBasedNumberConversion(var4) + " " + this.getString(R.string._pixels_per_inch) + ")");
      var3.setTextColor(Color.WHITE);
      var2.setTextColor(Color.WHITE);
      View ind= var1.findViewById(id);
      Animation a= AnimationUtils.loadAnimation(MyAndroidApplication.getAppContext(),android.R.anim.fade_out);
      ind.setAnimation(a);
      ind.setVisibility(View.INVISIBLE);
//      if(var2 != this.textViewLowDensity) {
//         this.textViewLowDensity.setTextColor(Color.WHITE);
//      }
//
//      if(var2 != this.textViewMediumDensity) {
//         this.textViewMediumDensity.setTextColor(Color.WHITE);
//      }
//
//      if(var2 != this.textViewHighDensity) {
//         this.textViewHighDensity.setTextColor(Color.WHITE);
//      }
//
//      if(var2 != this.textViewXHighDensity) {
//            this.textViewXHighDensity.setTextColor(Color.WHITE);
//      }
//
//      if(var2 != this.textViewXXHighDensity) {
//         this.textViewXXHighDensity.setTextColor(Color.WHITE);
//      }

   }

   private void setDensitySelection() {
      int var1 = this.getDeviceDensity();
      if(var1 <= 120) {
         this.onDensitySelectionChanges(this.linearLayoutLowDensity, this.textViewLowDensity, this.textViewLowDensityValue,R.id.lInd);
      } else if(var1 <= 160) {
         this.onDensitySelectionChanges(this.linearLayoutMediumDensity, this.textViewMediumDensity, this.textViewMediumDensityValue,R.id.mInd);
      } else if(var1 <= 240) {
         this.onDensitySelectionChanges(this.linearLayoutHighDensity, this.textViewHighDensity, this.textViewHighDensityValue,R.id.hInd);
      } else if(var1 <= 320) {
         this.onDensitySelectionChanges(this.linearLayoutXHighDensity, this.textViewXHighDensity, this.textViewXHighDensityValue,R.id.xInd);
      } else {
         this.onDensitySelectionChanges(this.linearLayoutXXHighDensity, this.textViewXXHighDensity, this.textViewXXHighDensityValue,R.id.xxInd);
      }

   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.getActivity().setTitle(R.string.screen_density);
      this.setRetainInstance(true);
      this.initView();
//      this.linearLayoutXXHighDensity.post(this.animationRunnable);
      setDensitySelection();
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.device_density_screen, (ViewGroup)null);
   }

   public void onPause() {
      if(this.adView != null) {
         this.adView.pause();
      }
      super.onPause();
   }

   public void onResume() {
      super.onResume();
      if(this.adView != null) {
         this.adView.resume();
      }
   }

   private class AnimationRunnable implements Runnable {
      int duration;

      private AnimationRunnable() {
         this.duration = 1000;
      }

      // $FF: synthetic method
      AnimationRunnable(AnimationRunnable var2) {
         this();
      }

      public void run() {
         int var1 = ScreenDensityFragment.this.getDeviceDensity();
         if(var1 <= 120) {
            var1 = (ScreenDensityFragment.this.linearLayoutLowDensity.getTop() + ScreenDensityFragment.this.linearLayoutLowDensity.getBottom()) / 2;
         } else if(var1 <= 160) {
            var1 = (ScreenDensityFragment.this.linearLayoutMediumDensity.getTop() + ScreenDensityFragment.this.linearLayoutMediumDensity.getBottom()) / 2;
         } else if(var1 <= 240) {
            var1 = (ScreenDensityFragment.this.linearLayoutHighDensity.getTop() + ScreenDensityFragment.this.linearLayoutHighDensity.getBottom()) / 2;
         } else if(var1 <= 320) {
            var1 = (ScreenDensityFragment.this.linearLayoutXHighDensity.getTop() + ScreenDensityFragment.this.linearLayoutXHighDensity.getBottom()) / 2;
         } else {
            int top= ScreenDensityFragment.this.linearLayoutXXHighDensity.getTop();
            int bottom=ScreenDensityFragment.this.linearLayoutXXHighDensity.getBottom();
            var1 = (ScreenDensityFragment.this.linearLayoutXXHighDensity.getTop() + ScreenDensityFragment.this.linearLayoutXXHighDensity.getBottom()) / 2;
         }
         int var2 = ScreenDensityFragment.this.imageViewArrow.getHeight() / 2;
         ScreenDensityFragment.this.moveViewToPosition(ScreenDensityFragment.this.imageViewArrow, var1 - var2, this.duration);
      }
   }
   @Override
   public void onAttach(Context context) {
      super.onAttach(context);
//      ((HomeActivity)context).getToolbar().setBackgroundColor(getResources().getColor(R.color.screen_density));
   }
}
