package com.info.myandroid.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.R;

public class APILevelFragment extends MasterFragment {
   private String version_name="";

   private String getBuildVersion() {
      return Build.VERSION.RELEASE;
   }

   public static APILevelFragment getInstance() {
      return new APILevelFragment();
   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.getActivity().setTitle(R.string.os_version);
      View var3 = this.getView();
      String var2 = this.getBuildVersion();
      switch (Build.VERSION.SDK_INT){
         case 11:
            version_name+="HoneyComb ";
            break;
         case 12:
            version_name+="HoneyComb ";
            break;
         case 13:
            version_name+="HoneyComb ";
            break;
         case 14:
            version_name+="IceCreamSandwish ";
            break;
         case 15:
            version_name+="IceCreamSandwish ";
            break;
         case 16:
            version_name+="JellyBeans ";
            break;
         case 17:
            version_name+="JellyBeans ";
            break;
         case 18:
            version_name+="JellyBeans ";
            break;
         case 19:
            version_name+="KitKat ";
            break;
         case 20:
            version_name+="KitKat ";
            break;
         case 21:
            version_name+="Lollipop ";
            break;
         case 22:
            version_name+="Lollipop ";
            break;
         case 23:
            version_name+="MarshMallow ";
            break;

      }
      ((TextView)var3.findViewById(R.id.textViewAPILevelValue)).setText(version_name+" "+var2);
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.device_api_level_screen, (ViewGroup)null);
   }
   @Override
   public void onAttach(Context context) {
      super.onAttach(context);
//      ((HomeActivity)context).getToolbar().setBackgroundColor(getResources().getColor(R.color.version_color));
   }
}
