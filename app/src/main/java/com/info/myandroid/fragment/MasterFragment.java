package com.info.myandroid.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import com.info.myandroid.R;
import com.info.myandroid.utils.AppUtil;

public class MasterFragment extends Fragment {
   private AdView adView = null;

   private void loadAdvertisement() {
      View var2 = this.getView();
      AdRequest var1 = AppUtil.getAdRequest();
//      this.adView = (AdView)var2.findViewById(R.id.adSmartBanner);
//      if(this.adView != null) {
//         this.adView.loadAd(var1);
//      }

   }

   public void onActivityCreated(Bundle var1) {
      super.onCreate(var1);
      this.loadAdvertisement();
   }

   public void onDestroy() {
      if(this.adView != null) {
         this.adView.destroy();
      }

      super.onDestroy();
   }

   public void onPause() {
      if(this.adView != null) {
         this.adView.pause();
      }

      super.onPause();
   }

   public void onResume() {
      super.onResume();
      if(this.adView != null) {
         this.adView.resume();
      }

   }
}
