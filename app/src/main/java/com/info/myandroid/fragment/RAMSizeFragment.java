package com.info.myandroid.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Locale;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.R;
import com.info.myandroid.utils.AppUtil;

public class RAMSizeFragment extends MasterFragment {
   private String ramSizeUnit;

   private String getExactSize(float var1) {
      String var2;
      if(var1 < 1024.0F) {
         var2 = AppUtil.toLocaleBasedNumberConversion((int) var1);
         this.ramSizeUnit = this.getString(R.string.mb);
      } else {
         var2 = AppUtil.toLocaleBasedDecimalFormat(var1 / 1024.0F);
         this.ramSizeUnit = this.getString(R.string.gb);
      }

      return var2;
   }

   public static RAMSizeFragment getInstance() {
      return new RAMSizeFragment();
   }

   private String getRAMSize() {
      // $FF: Couldn't be decompiled
       try {
           RandomAccessFile randomAccessFile= new RandomAccessFile("/proc/meminfo", "r");
           String lineTotalRam= randomAccessFile.readLine();
           String temp=lineTotalRam.replaceAll("[\\D]", "");
           randomAccessFile.close();
          return getExactSize(Integer.parseInt(temp)/1024);
       } catch (FileNotFoundException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
       return "";
   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.getActivity().setTitle(R.string.title_ram);
      View var2 = this.getView();
      TextView var3 = (TextView)var2.findViewById(R.id.textViewRAMSizeValue);
      TextView var4 = (TextView)var2.findViewById(R.id.textViewRAMSizeUnit);
      if(AppUtil.getApplicationLocale().toString().split("_")[0].equals(Locale.ENGLISH.toString())) {
         var3.setText(this.getRAMSize() + " " + this.ramSizeUnit);
      } else {
         var3.setText(this.getRAMSize());
         var4.setText(this.ramSizeUnit);
      }
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.device_ram_size_screen, (ViewGroup)null);
   }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        ((HomeActivity)context).getToolbar().setBackgroundColor(getResources().getColor(R.color.ram_color));
    }
   public void onDestroy() {
      super.onDestroy();
   }
}
