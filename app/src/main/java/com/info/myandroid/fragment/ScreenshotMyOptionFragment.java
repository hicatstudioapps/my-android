package com.info.myandroid.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.R;
import com.info.myandroid.utils.AppUtil;
import com.info.myandroid.utils.PersistenceManager;

@SuppressWarnings("ResourceType")
public class ScreenshotMyOptionFragment extends MasterFragment {
   private AppCompatActivity activity;
   private Button buttonMoreOption;
   private RelativeLayout buttonSection;
   private ImageView imageViewScreenshotOption;
   private String screenshotOptionCode;
   private View viewDivider;

   public static ScreenshotMyOptionFragment getInstance() {
      ScreenshotMyOptionFragment var0 = new ScreenshotMyOptionFragment();
      var0.screenshotOptionCode = PersistenceManager.getDeviceScreenShotInfo();
      return var0;
   }

   private void handleShowMoreOption() {
      this.launchFragment(ScreenshotAllOptionsFragment.getInstance());
   }

   private void initializeView() {
      this.imageViewScreenshotOption = (ImageView)this.activity.findViewById(R.id.imageViewScreenshotOption);
      this.buttonSection = (RelativeLayout)this.activity.findViewById(R.id.buttonSection);
      this.buttonMoreOption = (Button)this.activity.findViewById(R.id.buttonMoreOption);
      ScreenshotMyOptionOnClickListener var1 = new ScreenshotMyOptionOnClickListener();
      this.buttonMoreOption.setOnClickListener(var1);
      this.viewDivider = this.activity.findViewById(R.id.viewDivider2);
      this.updateView();
   }

   private void launchFragment(Fragment var1) {
      FragmentTransaction var2 = this.getActivity().getSupportFragmentManager().beginTransaction();
      var2.replace(R.id.container, var1);
      var2.commit();
   }

   private void updateView() {
      byte var2 = 0;
      int var3 = AppUtil.getImageResourceId("screenshot_option_" + this.screenshotOptionCode);
      this.imageViewScreenshotOption.setImageResource(var3);
      if(PersistenceManager.isScreenshotOptionVerified()) {
         var2 = 8;
      } else {
         Bundle var1 = this.getArguments();
         if(var1 != null) {
            if(var1.getBoolean("SHOULD_SHOW_MORE_OPTION", true)) {
               var2 = 0;
            } else {
               var2 = 8;
            }
         }
      }

      this.buttonSection.setVisibility(var2);
      if(this.viewDivider != null) {
         this.viewDivider.setVisibility(var2);
      }

   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.getActivity().setTitle(R.string.screen_shot);
      this.setRetainInstance(true);
      this.initializeView();
   }

   public void onAttach(Context var1) {
      super.onAttach(var1);
      this.activity = (HomeActivity)var1;
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.screenshot_my_option_screen, (ViewGroup)null);
   }

   class ScreenshotMyOptionOnClickListener implements View.OnClickListener {
      public void onClick(View var1) {
         switch(var1.getId()) {
         case R.id.buttonMoreOption:
            ScreenshotMyOptionFragment.this.handleShowMoreOption();
         default:
         }
      }
   }
}
