package com.info.myandroid.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.info.myandroid.utils.AppConstants;


public class ScreenshotFragmentAdapter extends FragmentStatePagerAdapter {
   public ScreenshotFragmentAdapter(FragmentManager var1) {
      super(var1);
   }

   public int getCount() {
      return AppConstants.SCREENSHOT_OPTIONS_STACK.length;
   }

   public Fragment getItem(int var1) {
      return ScreenshotOptionFragment.getInstance(var1);
//      return null;
   }

   public void setPrimaryItem(ViewGroup var1, int var2, Object var3) {
      super.setPrimaryItem(var1, var2, var3);
   }
}
