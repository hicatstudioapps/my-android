package com.info.myandroid.fragment;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.AsyncTask.Status;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ViewAnimator;
import android.widget.ViewFlipper;
import android.widget.AdapterView.OnItemClickListener;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.R;

import com.info.myandroid.utils.CallDetailsData;
import com.info.myandroid.utils.CallTaskManager;
import com.info.myandroid.utils.CallTimeWindow;
import com.info.myandroid.utils.DeviceManager;
import com.info.myandroid.utils.KnownCallsDetailsAdapter;
import com.info.myandroid.utils.UnknownCallsDetailsAdapter;

import java.util.ArrayList;

@SuppressWarnings("ResourceType")
public class CallDetailsFragment extends MasterFragment implements OnItemClickListener, OnClickListener {
   // $FF: synthetic field
   private static int[] $SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow;
   private Activity activity;
   private CallTimeWindow callTimeWindow;
   private KnownCallsDetailsAdapter callsDetailsGridViewAdapter = null;
   private UnknownCallsDetailsAdapter callsDetailsListViewAdapter = null;
   boolean canDeviceMakePhoneCalls;
   private GridView gridViewCallDetails = null;
   private LinearLayout knownCallNoCallSection;
   private ArrayList knownContactCallList = null;
   private View linLayoutKnownCallsInProgressSection;
   private View linLayoutUnknownCallsInProgressSection;
   private LinearLayout linearLayoutLastWeekCallTab = null;
   private LinearLayout linearLayoutTodayCallTab = null;
   private LinearLayout linearLayoutYesterdayCallTab = null;
   private ListView listViewCallDetails = null;
   private View mainLayoutView;
   private ReadCallLogTask readCallLogTask;
   private RelativeLayout relLayoutKnownCalls = null;
   private RelativeLayout relLayoutUnknownCalls = null;
   private LinearLayout unKnownCallNoCallSection;
   private ArrayList unknownContactCallList = null;
   private View viewLastWeekTabSelect = null;
   private View viewTodayTabSelect = null;
   private View viewYesterdayTabSelect = null;

   // $FF: synthetic method
   static int[] $SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow() {
      int[] var0 = $SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow;
      if(var0 == null) {
         var0 = new int[CallTimeWindow.values().length];

         try {
            var0[CallTimeWindow.LAST_WEEK.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            var0[CallTimeWindow.TODAY.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            var0[CallTimeWindow.YESTERDAY.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         $SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow = var0;
      }

      return var0;
   }

   public CallDetailsFragment() {
      this.callTimeWindow = CallTimeWindow.TODAY;
      this.readCallLogTask = null;
      this.canDeviceMakePhoneCalls = false;
   }

   private void cleanKnonwCallsAdapter() {
      if(this.callsDetailsGridViewAdapter != null) {
         this.callsDetailsGridViewAdapter.cleanImageCache();
      }

   }

   public static CallDetailsFragment getInstance() {
      return new CallDetailsFragment();
   }

   private void handleAsyncTask(Activity var1) {
      if(this.readCallLogTask != null && this.readCallLogTask.getStatus() == Status.RUNNING) {
         this.showProgressBarSection();
      } else {
         this.readCallLogTask = new ReadCallLogTask((ReadCallLogTask)null);
         this.readCallLogTask.execute(new CallTimeWindow[]{this.callTimeWindow});
      }

   }

   private void hideMainContentSection() {
      ((LinearLayout)this.mainLayoutView.findViewById(R.id.linearLayoutCallDataRootContainer)).setVisibility(8);
   }

   private void hideTabsSection() {
      ((LinearLayout)this.mainLayoutView.findViewById(R.id.tabStructureTimePeriodSection)).setVisibility(8);
   }

   private void initCallDetailsContentView() {
      this.gridViewCallDetails = (GridView)this.mainLayoutView.findViewById(R.id.gridViewCallDetails);
      this.relLayoutKnownCalls = (RelativeLayout)this.mainLayoutView.findViewById(R.id.relLayoutKnownCalls);
      this.linLayoutKnownCallsInProgressSection = this.relLayoutKnownCalls.findViewById(R.id.linLayoutKnownCallsInProgressSection);
      this.knownCallNoCallSection = (LinearLayout)this.relLayoutKnownCalls.findViewById(R.id.knownCallNoCallSection);
      this.listViewCallDetails = (ListView)this.mainLayoutView.findViewById(R.id.listViewCallDetails);
      this.listViewCallDetails.setClickable(false);
      this.relLayoutUnknownCalls = (RelativeLayout)this.mainLayoutView.findViewById(R.id.relLayoutUnknownCalls);
      this.linLayoutUnknownCallsInProgressSection = this.relLayoutUnknownCalls.findViewById(R.id.linLayoutUnknownCallsInProgressSection);
      this.unKnownCallNoCallSection = (LinearLayout)this.relLayoutUnknownCalls.findViewById(R.id.unKnownCallNoCallSection);
   }

   private void initTabView() {
      this.linearLayoutTodayCallTab = (LinearLayout)this.mainLayoutView.findViewById(R.id.linearLayoutTodayCallTab);
      this.linearLayoutYesterdayCallTab = (LinearLayout)this.mainLayoutView.findViewById(R.id.linearLayoutYesterdayCallTab);
      this.linearLayoutLastWeekCallTab = (LinearLayout)this.mainLayoutView.findViewById(R.id.linearLayoutLastWeekCallTab);
      this.linearLayoutTodayCallTab.setOnClickListener(this);
      this.linearLayoutYesterdayCallTab.setOnClickListener(this);
      this.linearLayoutLastWeekCallTab.setOnClickListener(this);
      this.viewTodayTabSelect = this.mainLayoutView.findViewById(R.id.viewTodayTabSelect);
      this.viewYesterdayTabSelect = this.mainLayoutView.findViewById(R.id.viewYesterdayTabSelect);
      this.viewLastWeekTabSelect = this.mainLayoutView.findViewById(R.id.viewLastWeekTabSelect);
      this.viewTodayTabSelect.setVisibility(8);
      this.viewYesterdayTabSelect.setVisibility(8);
      this.viewLastWeekTabSelect.setVisibility(8);
      switch($SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow()[this.callTimeWindow.ordinal()]) {
      case 1:
         this.viewTodayTabSelect.setVisibility(0);
         break;
      case 2:
         this.viewYesterdayTabSelect.setVisibility(0);
         break;
      case 3:
         this.viewLastWeekTabSelect.setVisibility(0);
      }

   }

   private void initializeView() {
      this.initTabView();
      this.initCallDetailsContentView();
   }

   private void prepareViewForFeatureNotAvailable() {
      this.showFeatureNotAvailableSection();
      this.hideTabsSection();
      this.hideMainContentSection();
   }

   private void refreshCallDetailView(CallTimeWindow var1) {
      this.readCallLogTask = new ReadCallLogTask((ReadCallLogTask)null);
      this.readCallLogTask.execute(new CallTimeWindow[]{var1});
   }

   private void rotateView(View var1) {
      var1.setRotation(45);
      //AnimationFactory.flipTransition((ViewAnimator) var1.findViewById(2131492910), 1);
   }

   private void rotateView(ViewFlipper var1) {
     // AnimationFactory.flipTransition(var1, AnimationFactory.FlipDirection.LEFT_RIGHT);
   }

   private void setTitle() {
      this.activity.setTitle(R.string.call_details_title);
   }

   private void showFeatureNotAvailableSection() {
      ((RelativeLayout)this.mainLayoutView.findViewById(R.id.relLayCallFeatureNotAvailableSection)).setVisibility(0);
   }

   private void showKnownCalls() {
      this.linLayoutKnownCallsInProgressSection.setVisibility(8);
      this.knownCallNoCallSection.setVisibility(8);
      this.gridViewCallDetails.setVisibility(0);
   }

   private void showNoKnownCallsAvailable() {
      this.linLayoutKnownCallsInProgressSection.setVisibility(8);
      this.gridViewCallDetails.setVisibility(8);
      this.knownCallNoCallSection.setVisibility(0);
   }

   private void showNoUnKnownCallsAvailable() {
      this.linLayoutUnknownCallsInProgressSection.setVisibility(8);
      this.listViewCallDetails.setVisibility(8);
      this.unKnownCallNoCallSection.setVisibility(0);
   }

   private void showProgressBarSection() {
      this.gridViewCallDetails.setVisibility(8);
      this.knownCallNoCallSection.setVisibility(8);
//      (this.linLayoutKnownCallsInProgressSection.findViewById(R.id.progressBar)).sty(getResources().getDrawable(R.drawable.progress_call));
      this.linLayoutKnownCallsInProgressSection.setVisibility(0);
      this.listViewCallDetails.setVisibility(8);
      this.unKnownCallNoCallSection.setVisibility(8);
//      ((ProgressBar)this.linLayoutKnownCallsInProgressSection.findViewById(R.id.progressBar)).setProgressDrawable(getResources().getDrawable(R.drawable.progress_call));
      this.linLayoutUnknownCallsInProgressSection.setVisibility(0);
   }

   private void showUnKnownCalls() {
      this.linLayoutUnknownCallsInProgressSection.setVisibility(8);
      this.unKnownCallNoCallSection.setVisibility(8);
      this.listViewCallDetails.setVisibility(0);
   }

   private void updateCallDetailsContentView(CallDetailsData var1) {
      if(var1 != null) {
         this.knownContactCallList = var1.getKnownContactCallList();
         this.unknownContactCallList = var1.getUnknownContactCallList();
      }

      this.cleanKnonwCallsAdapter();
      if(this.knownContactCallList != null && this.knownContactCallList.size() > 0) {
         this.showKnownCalls();
         this.callsDetailsGridViewAdapter = new KnownCallsDetailsAdapter();
         this.callsDetailsGridViewAdapter.setCallsInList(this.knownContactCallList);
         this.callsDetailsGridViewAdapter.calculateHeightOfCells();
         this.gridViewCallDetails.setAdapter(this.callsDetailsGridViewAdapter);
         this.gridViewCallDetails.setOnItemClickListener(this);
      } else {
         this.showNoKnownCallsAvailable();
      }

      if(this.unknownContactCallList != null && this.unknownContactCallList.size() > 0) {
         this.showUnKnownCalls();
         this.callsDetailsListViewAdapter = new UnknownCallsDetailsAdapter();
         this.callsDetailsListViewAdapter.setCallsInList(this.unknownContactCallList);
         this.listViewCallDetails.setAdapter(this.callsDetailsListViewAdapter);
      } else {
         this.showNoUnKnownCallsAvailable();
      }

   }

   public ViewFlipper getFlippedChild(int var1) {
      int var3 = this.gridViewCallDetails.getCount();

      for(int var2 = 0; var2 < var3; ++var2) {
         ;
      }

      return (ViewFlipper)this.gridViewCallDetails.getChildAt(var1);
   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.activity = this.getActivity();
      this.mainLayoutView = this.getView();
      this.setTitle();
      this.canDeviceMakePhoneCalls = DeviceManager.canDeviceMakePhoneCalls();
      if(this.canDeviceMakePhoneCalls) {
         this.initializeView();
      } else {
         this.prepareViewForFeatureNotAvailable();
      }

   }

   public void onClick(View var1) {
      if(this.readCallLogTask == null || this.readCallLogTask.getStatus() != Status.RUNNING) {
         int var2 = var1.getId();
         this.viewTodayTabSelect.setVisibility(8);
         this.viewYesterdayTabSelect.setVisibility(8);
         this.viewLastWeekTabSelect.setVisibility(8);
         switch(var2) {
         case R.id.linearLayoutTodayCallTab:
            this.viewTodayTabSelect.setVisibility(0);
            this.callTimeWindow = CallTimeWindow.TODAY;
            break;
         case R.id.linearLayoutYesterdayCallTab:
            this.viewYesterdayTabSelect.setVisibility(0);
            this.callTimeWindow = CallTimeWindow.YESTERDAY;
            break;
         case R.id.linearLayoutLastWeekCallTab:
            this.viewLastWeekTabSelect.setVisibility(0);
            this.callTimeWindow = CallTimeWindow.LAST_WEEK;
         }

         this.refreshCallDetailView(this.callTimeWindow);
      }

   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.call_details_screen, (ViewGroup)null);
   }

   public void onDestroy() {
      super.onDestroy();
      this.cleanKnonwCallsAdapter();
   }

   @Override
   public void onAttach(Context context) {
      super.onAttach(context);
//      ((HomeActivity)context).getToolbar().setBackgroundColor(getResources().getColor(R.color.tab_bg));
   }

   public void onItemClick(AdapterView param1, View param2, int param3, long param4) {
      // $FF: Couldn't be decompiled
//      int j = callsDetailsGridViewAdapter.getCurrentFlipIndex();
//      callsDetailsGridViewAdapter.setPreviousFlipIndex(j);
      final ViewFlipper flipper= (ViewFlipper) param2.findViewById(R.id.view_flipper);
      final View animate_out= flipper.getChildAt(0);
      final View animate_in= flipper.getChildAt(1);
      if(animate_out.getVisibility() != View.VISIBLE )
         flip(animate_out,animate_in);
                 else
         flip(animate_in,animate_out);
//      flip(animate_in,animate_out);
//      animate_out.setVisibility(View.INVISIBLE);
//      animate_in.setVisibility(View.VISIBLE);
//      ValueAnimator valueAnimator= ObjectAnimator.ofFloat(animate_out, "rotationY", 0, 360);
//      valueAnimator.setDuration(3000);
//      valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//         @Override
//         public void onAnimationUpdate(ValueAnimator animation) {
//            if((float)animation.getAnimatedValue() > 180.0){
//               animate_in.setVisibility(View.VISIBLE);
//               animate_out.setVisibility(View.INVISIBLE);
//               animate_in.invalidate();
//               animate_out.invalidate();
//            }
//
//
//         }
//      });
//      valueAnimator.start();

     // rotateView(param2);

   }

   private void flip(final View in, final View out){
      final AnimatorSet animatorSet_In= (AnimatorSet) AnimatorInflater.loadAnimator(activity,R.anim.fade_out);
      animatorSet_In.addListener(new Animator.AnimatorListener() {
         @Override
         public void onAnimationStart(Animator animation) {
//               animate_in.setVisibility(View.VISIBLE);
//            animate_out.setVisibility(View.INVISIBLE);
            //in.setVisibility(View.VISIBLE);
         }

         @Override
         public void onAnimationEnd(Animator animation) {
            out.setVisibility(View.INVISIBLE);
         }

         @Override
         public void onAnimationCancel(Animator animation) {

         }

         @Override
         public void onAnimationRepeat(Animator animation) {

         }
      });
      animatorSet_In.setTarget(in);
      animatorSet_In.setDuration(500);
//      animatorSet_In.setStartDelay(450);

      AnimatorSet animatorSet_Out= (AnimatorSet) AnimatorInflater.loadAnimator(activity,R.anim.flip_in);
      animatorSet_Out.addListener(new Animator.AnimatorListener() {
         @Override
         public void onAnimationStart(Animator animation) {
//               animate_in.setVisibility(View.VISIBLE);
//            animate_out.setVisibility(View.INVISIBLE);
         }

         @Override
         public void onAnimationEnd(Animator animation) {
            animatorSet_In.start();
            in.setVisibility(View.VISIBLE);
         }

         @Override
         public void onAnimationCancel(Animator animation) {

         }

         @Override
         public void onAnimationRepeat(Animator animation) {

         }
      });
      animatorSet_Out.setTarget(out);
      animatorSet_Out.setDuration(500);
     // animatorSet_Out.playTogether(animatorSet_In);
      animatorSet_Out.start();

   }

   public void onStart() {
      super.onStart();
      if(this.canDeviceMakePhoneCalls) {
         this.handleAsyncTask(this.activity);
      }
   }

   private class ReadCallLogTask extends AsyncTask<CallTimeWindow,Void,CallDetailsData> {
      private ReadCallLogTask() {
      }

      // $FF: synthetic method
      ReadCallLogTask(ReadCallLogTask var2) {
         this();
      }

      protected CallDetailsData doInBackground(CallTimeWindow... var1) {
         Object var2 = null;

         CallDetailsData var4;
         try {
            var4 = CallTaskManager.analyzeCallLogs(var1[0]);
            Thread.sleep(2000L);
         } catch (Exception var3) {
            var4 = (CallDetailsData)var2;
         }
         return var4;
      }

      protected void onPostExecute(CallDetailsData var1) {
         super.onPostExecute(var1);
         CallDetailsFragment.this.updateCallDetailsContentView(var1);
      }

      protected void onPreExecute() {
         super.onPreExecute();
         CallDetailsFragment.this.showProgressBarSection();
      }
   }
}
