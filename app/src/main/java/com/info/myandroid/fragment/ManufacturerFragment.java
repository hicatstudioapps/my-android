package com.info.myandroid.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.R;
import com.info.myandroid.utils.AppUtil;

public class ManufacturerFragment extends MasterFragment {

   private String getDeviceManufacturerName() {
      return Build.MANUFACTURER;
   }

   private String getDeviceModelNumber() {
      return Build.MODEL;
   }

   public static ManufacturerFragment getInstance() {
      return new ManufacturerFragment();
   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.getActivity().setTitle(R.string.manufacturer_model);
      View var6 = this.getView();
      TextView var4 = (TextView)var6.findViewById(R.id.textViewManufacturerName);
      TextView var5 = (TextView)var6.findViewById(R.id.textViewModelNo);
      String var2 = this.getDeviceManufacturerName();
      String var7 = "";
      if(var2 != null) {
         var7 = AppUtil.convertFirstCharOfWordsToUpperCase(var2);
      }
      String var3 = this.getDeviceModelNumber();
      var2 = var3;
      if(var3 == null) {
         var2 = "";
      }
      var4.setText(var7);
      var5.setText(var2);
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.manufacturer_screen, (ViewGroup)null);
   }

   @Override
   public void onAttach(Context context) {
      super.onAttach(context);
//      ((HomeActivity)context).getToolbar().setBackgroundColor(getResources().getColor(R.color.fabricante_color));
   }

   public void onDestroy() {
      super.onDestroy();
   }
}
