package com.info.myandroid.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.R;


public class BatteryFragment extends MasterFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private HomeActivity mListener;
    private TextView temp;
    private TextView plug;
    private TextView health;
    private TextView status;
    private TextView tech;
    private ProgressBar progress;
    private TextView lvl;


    // TODO: Rename and change types and number of parameters
    public static BatteryFragment newInstance() {
        BatteryFragment fragment = new BatteryFragment();
        return fragment;
    }

    public BatteryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStop() {
        super.onStop();
        mListener.unregisterReceiver(battery_receiver);
    }

    @Override
    public void onActivityCreated(Bundle var1) {
        super.onActivityCreated(var1);
        View root= getView();
        this.getActivity().setTitle(R.string.battery);
        temp=(TextView)root.findViewById(R.id.temp);
        plug=(TextView)root.findViewById(R.id.plug);
        health=(TextView)root.findViewById(R.id.health);
        status=(TextView)root.findViewById(R.id.status);
        tech=(TextView)root.findViewById(R.id.tech);
        lvl=(TextView)root.findViewById(R.id.level);
        progress=(ProgressBar)root.findViewById(R.id.progressBar2);
        mListener.registerReceiver(battery_receiver,new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_battery, container, false);
    }

    

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            mListener = (HomeActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.registerReceiver(battery_receiver,new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }

    private int blevel;
    private int level;
    private BroadcastReceiver battery_receiver = new BroadcastReceiver() {
        public void onReceive(Context var1, Intent var2) {
            String var9 = var2.getStringExtra("technology");
            int var7 = var2.getIntExtra("plugged", -1);
            int var5 = var2.getIntExtra("scale", -1);
            int var4 = var2.getIntExtra("health", 0);
            int var8 = var2.getIntExtra("status", 0);
            int var3 = var2.getIntExtra("level", -1);
            BatteryFragment.this.blevel=var3;
            int var6 = var2.getIntExtra("temperature", 0) / 10;
            var2.getExtras();
            if(true) {
                if(var3 >= 0 && var5 > 0) {
                    BatteryFragment.this.level = var3 * 100 / var5;
                }
            progress.setMax(var5);
                progress.setProgress(level);
                lvl.setText(BatteryFragment.this.level+"%");
               // String var10 = getString(R.string.level)+" " + BatteryFragment.this.level + "%\n";
                tech.setText(var9);
//                var9 = var10 + getString(R.string.tech)+" " + var9 + "\n";
                //var9 = var9 + getString(R.string.plug) + BatteryFragment.this.getPlugTypeString(var7) + "\n";
                plug.setText(BatteryFragment.this.getPlugTypeString(var7));
                health.setText(BatteryFragment.this.getHealthString(var4));
//                var9 = var9 + getString(R.string.health)+ " " + BatteryFragment.this.getHealthString(var4) + "\n";
//                var9 = var9 + getString(R.string.status) + BatteryFragment.this.getStatusString(var8) + "\n";
                status.setText(BatteryFragment.this.getStatusString(var8));
                temp.setText(var6+" C");
                var9 = var9 + getString(R.string.temp)+" " + var6 + " C";
//                BatteryFragment.this.setBatteryLevelText(var9 );
            } else {
//                BatteryFragment.this.setBatteryLevelText("Battery not present!!!");
            }
        }
    };

    private String getHealthString(int var1) {
        String var2 = "Unknown";
        switch(var1) {
            case 2:
                var2 = "Good";
                break;
            case 3:
                var2 = "Over Heat";
                break;
            case 4:
                var2 = "Dead";
                break;
            case 5:
                var2 = "Over Voltage";
                break;
            case 6:
                var2 = "Failure";
        }

        return var2;
    }

    private String getPlugTypeString(int var1) {
        String var2 = "Unknown";
        switch(var1) {
            case 1:
                var2 = "AC";
                break;
            case 2:
                var2 = "USB";
        }

        return var2;
    }

    private String getStatusString(int var1) {
        String var2 = "Unknown";
        switch(var1) {
            case 2:
                var2 = "Charging";
                break;
            case 3:
                var2 = "Discharging";
                break;
            case 4:
                var2 = "Not Charging";
                break;
            case 5:
                var2 = "Full";
        }

        return var2;
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
   
}
