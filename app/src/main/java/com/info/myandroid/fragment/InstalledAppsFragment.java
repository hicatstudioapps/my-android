package com.info.myandroid.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.info.myandroid.HomeActivity;
import com.info.myandroid.MyAndroidApplication;
import com.info.myandroid.R;
import com.info.myandroid.utils.AppUtil;
import com.info.myandroid.utils.InstalledApplicationsAdapter;
import com.info.myandroid.utils.MyAppsSortFieldEnum;
import com.info.myandroid.utils.MyAppsTaskManager;
import com.info.myandroid.utils.SortDirectionEnum;

import java.util.ArrayList;
import java.util.List;

import badabing.lib.apprater.AppRater;

@SuppressWarnings("ResourceType")
public class InstalledAppsFragment extends MasterFragment {
   private static final String DATA_SCHEME_PACKAGE = "package";
   private static final String KEY_CURRENT_SORT_DIRECTION = "KEY_CURRENT_SORT_DIRECTION";
   private static final String KEY_CURRENT_SORT_FIELD = "KEY_CURRENT_SORT_FIELD";
   private BroadcastReceiver appUninstallBroadcastReceiver;
   private SortDirectionEnum currentSortDirection;
   private MyAppsSortFieldEnum currentSortField;
   private EditText editTextFilter;
   private InstalledApplicationsAdapter installedApplicationsAdapter;
   private InstalledPackagesAsyncTask installedPackagesAsyncTask;
   private boolean isFragmentStarted;
   private ListView listViewApps;
   private MenuItem menuItemSort;
   private MenuItem menuItemSortByAlphabeticallyAsc;
   private MenuItem menuItemSortByAlphabeticallyDesc;
   private MenuItem menuItemSortByInstallationDateAsc;
   private MenuItem menuItemSortByInstallationDateDesc;
   private ProgressBar progressBar;
   private boolean refreshApplicationListing;
   private RelativeLayout relativeLayoutSearchSection;
   private SearchAppsTextWatcher searchAppsTextWatcher;
   android.support.v7.widget.Toolbar toolbar;
   boolean sort_asc=true;
   boolean sort_date=true;
   private AdView adView;
   private HomeActivity activity;

   public InstalledAppsFragment() {
      this.currentSortField = MyAppsSortFieldEnum.APPLICATION_NAME;
      this.currentSortDirection = SortDirectionEnum.ASCENDING;
      this.refreshApplicationListing = false;
      this.isFragmentStarted = false;
      this.searchAppsTextWatcher = new SearchAppsTextWatcher((SearchAppsTextWatcher)null);
   }

   private void cancelAsyncTask() {
      try {
         if(this.installedPackagesAsyncTask != null) {
            this.installedPackagesAsyncTask.cancel(true);
         }
      } catch (Exception var2) {
         ;
      }

   }

   private void changeMenuSettings(Menu var1) {
      this.menuItemSort = var1.findItem(R.id.menu_item_sort);
      this.menuItemSort.setVisible(true);
      if(Build.VERSION.SDK_INT >= 11) {
         MenuItem var4 = var1.findItem(R.id.menu_item_share);
         MenuItem var3 = var1.findItem(R.id.menu_item_rate);
         var4.setShowAsAction(0);
         var3.setShowAsAction(0);
      }

   }

   private void cleanAdapterInstanceOnConfigruationChange() {
      if(this.installedApplicationsAdapter != null) {
         this.installedApplicationsAdapter.cleanAdpater();
      }

   }

   private void enableSortApplicationsMenu() {
      if(this.menuItemSort != null) {
         this.menuItemSort.setEnabled(true);
      }
   }

   public static InstalledAppsFragment getInstance() {
      return new InstalledAppsFragment();
   }

   private void handleSearchTextAvailable(List var1) {
      this.installedApplicationsAdapter = new InstalledApplicationsAdapter(getActivity().getLayoutInflater(),var1);
      this.listViewApps.setAdapter(installedApplicationsAdapter);
//      this.installedApplicationsAdapter.setCurrentApplicationList(var1);
//      this.installedApplicationsAdapter.notifyDataSetChanged();
//      String var2 = this.editTextFilter.getEditableText().toString();
//      this.installedApplicationsAdapter.setApplicationList(var1);
//      if(AppUtil.containsData(var2)) {
//         this.installedApplicationsAdapter.getFilter().filter(var2);
//      } else {
//         this.installedApplicationsAdapter.setCurrentApplicationList(var1);
//         this.installedApplicationsAdapter.notifyDataSetChanged();
//      }
   }

   private void hideAppListView() {
      this.listViewApps.setVisibility(4);
   }

   private void hideProgressBar() {
      this.progressBar.setVisibility(8);
   }

   private void hideSearchSection() {
      this.relativeLayoutSearchSection.setVisibility(4);
   }

   private void initalizeOnConfiguraitonChange() {
      AsyncTask.Status var1 = this.installedPackagesAsyncTask.getStatus();
      if(var1 != AsyncTask.Status.RUNNING && var1 != AsyncTask.Status.PENDING) {
         if(this.installedApplicationsAdapter != null) {
            this.hideProgressBar();
            this.showSearchSection();
            this.showAppListView();
            this.installedApplicationsAdapter.notifyDataSetChanged();
            this.setEmptyTextViewOnListView();
         }
      } else {
         this.showProgressBar();
      }

   }

   private void initalizeView() {
      if(this.installedPackagesAsyncTask != null) {
         this.initalizeOnConfiguraitonChange();
      } else {
         this.runAsynTaskForApplicationListing();
      }

   }

   private void onSearchedAppListObtained(List var1) {
      this.setTitleWithAppCount(var1.size());
      this.hideProgressBar();
      this.showSearchSection();
      this.showAppListView();
      this.handleSearchTextAvailable(var1);
      this.setEmptyTextViewOnListView();
      this.enableSortApplicationsMenu();
   }

   private void prepareSortSubMenu() {

//         if(this.currentSortDirection == SortDirectionEnum.ASCENDING) {
//            this.menuItemSortByAlphabeticallyDesc.setVisible(true);
//         }
//
//         if(this.currentSortDirection == SortDirectionEnum.ASCENDING) {
//            this.menuItemSortByInstallationDateDesc.setVisible(true);
//         } else {
//            this.menuItemSortByInstallationDateAsc.setVisible(true);
//         }
//      }

   }

   private void readDataFromSavedInstance(Bundle var1) {
      int var3 = var1.getInt("KEY_CURRENT_SORT_FIELD");
      int var2 = var1.getInt("KEY_CURRENT_SORT_DIRECTION");
      if(var3 == MyAppsSortFieldEnum.APPLICATION_NAME.getCode()) {
         this.currentSortField = MyAppsSortFieldEnum.APPLICATION_NAME;
      } else if(var3 == MyAppsSortFieldEnum.INSTALLATION_DATE.getCode()) {
         this.currentSortField = MyAppsSortFieldEnum.INSTALLATION_DATE;
      }

      if(var2 == SortDirectionEnum.ASCENDING.getCode()) {
         this.currentSortDirection = SortDirectionEnum.ASCENDING;
      } else if(var2 == SortDirectionEnum.DESCENDING.getCode()) {
         this.currentSortDirection = SortDirectionEnum.DESCENDING;
      }
   }

   private void refreshApplicationListing() {
      if(this.installedPackagesAsyncTask != null) {
         if(this.installedPackagesAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
            this.runAsynTaskForApplicationListing();
         }
      } else {
         this.runAsynTaskForApplicationListing();
      }

   }

   private void registerBroadcastForAppInstallUninstall() {
      this.appUninstallBroadcastReceiver = new AppUninstallBroadcastReceiver((AppUninstallBroadcastReceiver)null);
      Context var1 = MyAndroidApplication.getAppContext();
      IntentFilter var2 = new IntentFilter("android.intent.action.PACKAGE_REMOVED");
      var2.addDataScheme("package");
      var1.registerReceiver(this.appUninstallBroadcastReceiver, var2);
      var2 = new IntentFilter("android.intent.action.PACKAGE_ADDED");
      var2.addDataScheme("package");
      var1.registerReceiver(this.appUninstallBroadcastReceiver, var2);
   }

   private void removeTextWatcher() {
      if(this.editTextFilter != null && this.searchAppsTextWatcher != null) {
         this.editTextFilter.removeTextChangedListener(this.searchAppsTextWatcher);
      }

   }

   private void runAsynTaskForApplicationListing() {
      this.installedPackagesAsyncTask = new InstalledPackagesAsyncTask((InstalledPackagesAsyncTask)null);
      this.installedPackagesAsyncTask.execute(new Void[0]);
   }

   private void setEmptyTextViewOnListView() {
//      TextView var1 = (TextView)var3.findViewById(R.id.empty);
//      this.listViewApps.setEmptyView(var1);
   }

   private void setTextWatcher() {
      this.editTextFilter.addTextChangedListener(this.searchAppsTextWatcher);
   }

   private void setTitle() {
      String var1 = this.getString(R.string.search);
      activity.getToolbar().setTitle(var1);
   }

   private void setTitleWithAppCount(int var1) {
      String var2 = AppUtil.numberFormatting(var1);
      var2 = this.getString(R.string.search) + " (" + var2 + ")";
      activity.getToolbar().setTitle(var2);
   }

   private void showAppListView() {
      this.listViewApps.setVisibility(0);
   }

   private void showProgressBar() {
      this.progressBar.setVisibility(0);
   }

   private void showSearchSection() {
      this.relativeLayoutSearchSection.setVisibility(0);
   }

   private void sortAppsList() {
      MyAppsTaskManager.sortAppList(this.currentSortField, this.currentSortDirection, this.installedApplicationsAdapter.getCurrentApplicationList());
      this.installedApplicationsAdapter.setCurrentSortCriteria(this.currentSortField, this.currentSortDirection);
      this.installedApplicationsAdapter.notifyDataSetChanged();
   }

   private void unRegisterBroadcastForAppInstallUninstall() {
      if(this.appUninstallBroadcastReceiver != null) {
         MyAndroidApplication.getAppContext().unregisterReceiver(this.appUninstallBroadcastReceiver);
      }

   }

   @Override
   public void onAttach(Context context) {
      super.onAttach(context);
      this.activity=(HomeActivity)context;
//      this.activity.getToolbar().setBackgroundColor(getResources().getColor(R.color.app_color));

   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.setTitle();
      this.setRetainInstance(true);
      View var3 = this.getView();
      if(var1 != null) {
         this.readDataFromSavedInstance(var1);
      }
//      AdRequest var13;
//      try {
//         this.adView = (AdView)var3.findViewById(R.id.adView);
//         var13 = (new AdRequest.Builder()).build();
//         this.adView.loadAd(var13);
//         this.adView.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//               super.onAdLoaded();
//               adView.setVisibility(View.VISIBLE);
//            }
//         });
//      } catch (Exception var8) {
//         ;
//      }
////   toolbar = (android.support.v7.widget.Toolbar) var3.findViewById(R.id.toolbar);
////      toolbar.setBackgroundColor(getResources().getColor(R.color.app_color));
//////      toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.icon_app_search));
////      toolbar.setTitle(R.string.search);
//      setSupportActionBar(toolbar);
//      getSupportActionBar().setHomeButtonEnabled(true);
//      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//      setSupportActionBar(toolbar);
      this.relativeLayoutSearchSection = (RelativeLayout)var3.findViewById(R.id.relativeLayoutSearchSection);
      this.editTextFilter = (EditText)var3.findViewById(R.id.editTextFilter);
      ((ImageButton)var3.findViewById(R.id.imageButtonClearSearch)).setOnClickListener(new InstalledAppsOnClickListener((InstalledAppsOnClickListener) null));
      this.progressBar = (ProgressBar)var3.findViewById(R.id.progressBar);
      this.listViewApps = (ListView)var3.findViewById(R.id.listViewApps);
      if(this.installedApplicationsAdapter == null) {
            showProgressBar();
           runAsynTaskForApplicationListing();
//         this.installedApplicationsAdapter = new InstalledApplicationsAdapter(getActivity().getLayoutInflater(), (List)MyAppsTaskManager.getInstalledApplications(MyAppsSortFieldEnum.APPLICATION_NAME,SortDirectionEnum.DESCENDING));
//         this.installedApplicationsAdapter.setCurrentSortCriteria(this.currentSortField, this.currentSortDirection);
      } else {
         this.installedApplicationsAdapter.setLayoutInflater(getActivity().getLayoutInflater());
         List var4 = this.installedApplicationsAdapter.getOriginalApplicationList();
         int var2;
         if(var4 != null) {
            var2 = var4.size();
         } else {
            var2 = 0;
         }

         this.setTitleWithAppCount(var2);
      }

      //this.listViewApps.setAdapter(this.installedApplicationsAdapter);
      // this.setHasOptionsMenu(true);
      this.setTextWatcher();
      this.initalizeView();
   }


   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      setHasOptionsMenu(true);

      return inflater.inflate(R.layout.my_apps_layout,null);
   }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater var2) {
        //super.onCreateOptionsMenu(menu);
        var2.inflate(R.menu.menu_apps, menu);
        this.changeMenuSettings(menu);
        if(this.installedPackagesAsyncTask != null && this.installedPackagesAsyncTask.getStatus() == AsyncTask.Status.FINISHED) {
            this.enableSortApplicationsMenu();
        }
//
//        this.menuItemSortByInstallationDateAsc = menu.findItem(R.id.menu_item_sort_date_wise_asc);
//        this.menuItemSortByInstallationDateDesc = menu.findItem(R.id.menu_item_sort_date_wise_desc);
//        this.menuItemSortByAlphabeticallyAsc = menu.findItem(R.id.menu_item_sort_alphabetically_asc);
//        this.menuItemSortByAlphabeticallyDesc = menu.findItem(R.id.menu_item_sort_alphabetically_desc);
//        this.prepareSortSubMenu();
//        return true;
    }



   public void onDestroy() {
      super.onDestroy();
      this.cancelAsyncTask();
   }



//   public void onDetach() {
//      super.onDetach();
//      this.unRegisterBroadcastForAppInstallUninstall();
//      this.cleanAdapterInstanceOnConfigruationChange();
//   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2 = false;
      switch(var1.getItemId()) {
      case R.id.menu_item_sort_date:
         if(sort_date){
         this.currentSortField = MyAppsSortFieldEnum.INSTALLATION_DATE;
         this.currentSortDirection = SortDirectionEnum.ASCENDING;
         sort_date=false;}else
         {
            this.currentSortField = MyAppsSortFieldEnum.INSTALLATION_DATE;
            this.currentSortDirection = SortDirectionEnum.DESCENDING;
            sort_date=true;
         }
         var2 = true;
         break;
      case R.id.menu_item_sort:
         if(sort_asc){
            this.currentSortField = MyAppsSortFieldEnum.APPLICATION_NAME;
            this.currentSortDirection = SortDirectionEnum.ASCENDING;
            sort_asc=false;}else
         {
            this.currentSortField = MyAppsSortFieldEnum.APPLICATION_NAME;
            this.currentSortDirection = SortDirectionEnum.DESCENDING;
            sort_asc=true;
         }

         var2 = true;
             break;
         case R.id.menu_item_share:
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            String text = String.format(getString(R.string.share_text), getString(R.string.app_name), "https://play.google.com/store/apps/details?id="+ getActivity().getPackageName());
            sendIntent.putExtra(Intent.EXTRA_TEXT, text);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
            break;
         case R.id.menu_item_rate:
            AppRater.rateNow(getContext());
            break;
//         case android.R.id.home:
//            finish();
//            return true;

      }

      if(var2) {
         this.sortAppsList();
         this.prepareSortSubMenu();
      }

      return super.onOptionsItemSelected(var1);
   }

   public void onPause() {
      super.onPause();
   }

   public void onResume() {
      super.onResume();
   }

   public void onSaveInstanceState(Bundle var1) {
      super.onSaveInstanceState(var1);
      var1.putInt("KEY_CURRENT_SORT_FIELD", this.currentSortField.getCode());
      var1.putInt("KEY_CURRENT_SORT_DIRECTION", this.currentSortDirection.getCode());
   }

   public void onStart() {
      super.onStart();
      this.isFragmentStarted = true;
      if(this.refreshApplicationListing) {
         this.refreshApplicationListing = false;
         this.refreshApplicationListing();
      }

   }

   public void onStop() {
      super.onStop();
      this.isFragmentStarted = false;
   }

   private class AppUninstallBroadcastReceiver extends BroadcastReceiver {
      private AppUninstallBroadcastReceiver() {
      }

      // $FF: synthetic method
      AppUninstallBroadcastReceiver(AppUninstallBroadcastReceiver var2) {
         this();
      }

      public void onReceive(Context var1, Intent var2) {
         if(var2.getExtras() != null) {
            String var3 = var2.getAction();
            if((var3.equals("android.intent.action.PACKAGE_ADDED") || var3.equals("android.intent.action.PACKAGE_REMOVED")) && InstalledAppsFragment.this != null) {
               if(InstalledAppsFragment.this.isFragmentStarted) {
                  InstalledAppsFragment.this.refreshApplicationListing();
               } else {
                  InstalledAppsFragment.this.refreshApplicationListing = true;
               }
            }
         }

      }
   }

   private class InstalledAppsOnClickListener implements View.OnClickListener {
      private InstalledAppsOnClickListener() {
      }

      // $FF: synthetic method
      InstalledAppsOnClickListener(InstalledAppsOnClickListener var2) {
         this();
      }

      public void onClick(View var1) {
         switch(var1.getId()) {
         case R.id.imageButtonClearSearch:
            InstalledAppsFragment.this.editTextFilter.setText("");
         default:
         }
      }
   }

   private class InstalledPackagesAsyncTask extends AsyncTask<Void,Void,ArrayList> {
      private InstalledPackagesAsyncTask() {
      }

       @Override
       protected ArrayList doInBackground(Void[] params) {
           return MyAppsTaskManager.getInstalledApplications(InstalledAppsFragment.this.currentSortField, InstalledAppsFragment.this.currentSortDirection);
       }

       // $FF: synthetic method
      InstalledPackagesAsyncTask(InstalledPackagesAsyncTask var2) {
         this();
      }

      protected void onPostExecute(ArrayList var1) {
         if(!this.isCancelled()) {
            super.onPostExecute(var1);
            InstalledAppsFragment.this.onSearchedAppListObtained(var1);
            setTitleWithAppCount(var1.size());
//            toolbar.setTitle(getString(R.string.search)+"("+var1.size()+")");
         }
      }

      protected void onPreExecute() {
         super.onPreExecute();
         InstalledAppsFragment.this.showProgressBar();
         InstalledAppsFragment.this.hideSearchSection();
         InstalledAppsFragment.this.hideAppListView();
      }
   }

   private class SearchAppsTextWatcher implements TextWatcher {
      private SearchAppsTextWatcher() {
      }

      // $FF: synthetic method
      SearchAppsTextWatcher(SearchAppsTextWatcher var2) {
         this();
      }

      public void afterTextChanged(Editable var1) {
      }

      public void beforeTextChanged(CharSequence var1, int var2, int var3, int var4) {
      }

      public void onTextChanged(CharSequence var1, int var2, int var3, int var4) {
         if(var3 != var4 && AppUtil.isNotEmpty(InstalledAppsFragment.this.installedApplicationsAdapter.getOriginalApplicationList())) {
            InstalledAppsFragment.this.installedApplicationsAdapter.getFilter().filter(var1);
         }

      }
   }
}
