package com.info.myandroid.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.R;
import com.info.myandroid.utils.AppUIUtil;
import com.info.myandroid.utils.AppUtil;
import com.info.myandroid.utils.CameraDetail;
import com.info.myandroid.utils.CameraDirectoryNotAvailableException;
import com.info.myandroid.utils.CameraResolution;
import com.info.myandroid.utils.CameraTaskManager;
import com.info.myandroid.utils.DeviceCameras;
import com.info.myandroid.utils.PhotoRemaining;
import com.info.myandroid.utils.PhotoRemainingContainer;
import com.info.myandroid.utils.RotationWithTranslateAnimation;
import com.lacostra.utils.notification.Permission.Permission;

import java.util.ArrayList;

@SuppressWarnings("ResourceType")
public class CameraInfoFragment extends MasterFragment {
   private AsyncTaskForStoringDeviceCamerasInfo asyncTaskForStoringDeviceCamerasInfo;
   private boolean backStackAnimationDone = false;
   private CalculateRemainingPicturesAsyncTask calculateRemainingPicturesAsync;
   private DeviceCameras deviceCameras = null;
   private boolean frontStackAnimationDone = false;
   private boolean isFragmentDetached = false;
   private ProgressBar progressBarRemPicsCameraBack;
   private ProgressBar progressBarRemPicsCameraFront;
   private RelativeLayout relLayoutRemPhotosSectionBack;
   private RelativeLayout relLayoutRemPhotosSectionFront;
   private int remainingPicsBackCamera = -1;
   private int remainingPicsFrontCamera = -1;
   private View rootView;
   private Context context;

   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

   }



   private void cancelAsyncTasks() {
      try {
         AsyncTask.Status var1;
         if(this.asyncTaskForStoringDeviceCamerasInfo != null) {
            var1 = this.asyncTaskForStoringDeviceCamerasInfo.getStatus();
            if(var1 == AsyncTask.Status.PENDING || var1 == AsyncTask.Status.RUNNING) {
               this.asyncTaskForStoringDeviceCamerasInfo.cancel(false);
            }
         }

         if(this.calculateRemainingPicturesAsync != null) {
            var1 = this.calculateRemainingPicturesAsync.getStatus();
            if(var1 == AsyncTask.Status.PENDING || var1 == AsyncTask.Status.RUNNING) {
               this.calculateRemainingPicturesAsync.cancel(false);
            }
         }
      } catch (Exception var2) {
         ;
      }

   }

   public static CameraInfoFragment getInstance() {
      return new CameraInfoFragment();
   }

   private void hideCameraCapacityView(boolean var1) {
      RelativeLayout var2;
      if(var1) {
         var2 = this.relLayoutRemPhotosSectionFront;
      } else {
         var2 = this.relLayoutRemPhotosSectionBack;
      }

      ImageView var5 = (ImageView)var2.findViewById(R.id.imageViewStackCam1);
      ImageView var3 = (ImageView)var2.findViewById(R.id.imageViewStackCam2);
      ImageView var4 = (ImageView)var2.findViewById(R.id.imageViewStackCam3);
      var5.setVisibility(4);
      var3.setVisibility(4);
      var4.setVisibility(4);
      var5.clearAnimation();
      var3.clearAnimation();
      var4.clearAnimation();
      ((TextView)var2.findViewById(R.id.textViewRemPhotosLabel)).setVisibility(4);
      ((TextView)var2.findViewById(R.id.textViewRemPhotosCount)).setVisibility(4);
      ((TextView)var2.findViewById(R.id.textViewRemMsgNoPhotosCount)).setVisibility(8);
      this.hideProgressBarCameraResolutionSection(var2);
   }

   private void hideCameraNotAvailableView(RelativeLayout var1) {
      ((ImageView)var1.findViewById(R.id.imageViewCameraNotAvailable)).setVisibility(4);
      ((TextView)var1.findViewById(R.id.textViewCameraNotAvailable)).setVisibility(4);
   }

   private void hideCameraResolution(RelativeLayout var1) {
      ((TextView)var1.findViewById(R.id.textViewCameraResolution)).setVisibility(4);
      ((TextView)var1.findViewById(R.id.textViewCameraResDenominator)).setVisibility(4);
   }

   private void hideProgressBarCameraResolutionSection(RelativeLayout var1) {
      ProgressBar var2 = (ProgressBar)var1.findViewById(R.id.progressBarCalculatingRes);
      if(var2 != null) {
         var2.setVisibility(8);
      }

   }

   private void initializeView() {
      this.relLayoutRemPhotosSectionBack = (RelativeLayout)this.rootView.findViewById(R.id.relLayoutRemPhotosSectionBack);
      this.progressBarRemPicsCameraBack = (ProgressBar)this.relLayoutRemPhotosSectionBack.findViewById(R.id.progressBarRemPicsCamera);
      this.relLayoutRemPhotosSectionFront = (RelativeLayout)this.rootView.findViewById(R.id.relLayoutRemPhotosSectionFront);
      this.progressBarRemPicsCameraFront = (ProgressBar)this.relLayoutRemPhotosSectionFront.findViewById(R.id.progressBarRemPicsCamera);
   }

   private void onDeviceCamerasInformationAvailable() {
      int var1 = 2;
      if(this.deviceCameras != null) {
         var1 = this.deviceCameras.getFrontCameraStatus();
      }

      int var2 = 2;
      if(this.deviceCameras != null) {
         var2 = this.deviceCameras.getBackCameraStatus();
      }

      if(var1 == 1 || var2 == 1) {
//         this.calculateRemainingPicturesAsync = new CalculateRemainingPicturesAsyncTask((CalculateRemainingPicturesAsyncTask)null);
//         this.calculateRemainingPicturesAsync.execute(new Void[0]);
      }

      this.prepareFrontCameraView(var2);
      this.prepareBackCameraView(var1);
   }

   private void overrideCameraResolution(float var1, boolean var2) {
      int var5;
      if(var2) {
         var5 = R.id.relLayoutCameraResFront;
      } else {
         var5 = R.id.relLayoutCameraResBack;
      }

      RelativeLayout var4 = (RelativeLayout)this.rootView.findViewById(var5);
      TextView var3 = (TextView)var4.findViewById(R.id.textViewCameraResolution);
      var3.setVisibility(0);
      TextView var6 = (TextView)var4.findViewById(R.id.textViewCameraResDenominator);
      var6.setText(this.getString(R.string.camera_megapixel));
      var6.setVisibility(0);
      var3.setText(AppUtil.toLocaleBasedDecimalFormat(var1));
   }

   private void prepareAnimationPhotoStackForCamera(final boolean var1) {
      RelativeLayout var3;
      if(var1) {
         var3 = this.relLayoutRemPhotosSectionFront;
      } else {
         var3 = this.relLayoutRemPhotosSectionBack;
      }

      final ProgressBar var2;
      if(var1) {
         var2 = this.progressBarRemPicsCameraFront;
      } else {
         var2 = this.progressBarRemPicsCameraBack;
      }

      ImageView var4 = (ImageView)var3.findViewById(R.id.imageViewStackCam1);
      ImageView var5 = (ImageView)var3.findViewById(R.id.imageViewStackCam2);
      final ImageView var8 = (ImageView)var3.findViewById(R.id.imageViewStackCam3);
      var4.setVisibility(0);
      var5.setVisibility(0);
      var8.setVisibility(0);
      AnimationSet var7 = RotationWithTranslateAnimation.prepareAnimationSet(0);
      AnimationSet var9 = RotationWithTranslateAnimation.prepareAnimationSet(200);
      AnimationSet var6 = RotationWithTranslateAnimation.prepareAnimationSet(400);
      var6.setAnimationListener(new Animation.AnimationListener() {
         public void onAnimationEnd(Animation var1x) {
            var8.setVisibility(View.INVISIBLE);
//            if(!CameraInfoFragment.this.isFragmentDetached) {
//               if(CameraInfoFragment.this.calculateRemainingPicturesAsync != null) {
//                  AsyncTask.Status var2x = CameraInfoFragment.this.calculateRemainingPicturesAsync.getStatus();
//                  if(var2x != AsyncTask.Status.RUNNING && var2x != AsyncTask.Status.PENDING) {
//                     CameraInfoFragment.this.prepareCameraCapacityView(var1);
//                  } else {
//                     AppUIUtil.showProgressBar(var2);
//                  }
//               }
//
//               if(var1) {
//                  CameraInfoFragment.this.frontStackAnimationDone = true;
//               } else {
//                  CameraInfoFragment.this.backStackAnimationDone = true;
//               }
//            }
            resolutionData(var1);
         }

         public void onAnimationRepeat(Animation var1x) {
         }

         public void onAnimationStart(Animation var1x) {
         }
      });
      var4.startAnimation(var7);
      var5.startAnimation(var9);
      var8.startAnimation(var6);
   }

   private void prepareBackCameraView(int var1) {
      RelativeLayout var2 = (RelativeLayout)this.rootView.findViewById(R.id.relLayoutCameraResBack);
      this.hideProgressBarCameraResolutionSection(var2);
      if(var1 == 1) {
         this.prepareViewForCameraAvailable(false);
      } else {
         ((TextView)var2.findViewById(R.id.textViewCameraPosition)).setText(R.string.back_cam);
         this.prepareViewForCameraNotAvailable(var2, var1);
      }
   }

   public void resolutionData(boolean back){
      RelativeLayout var2;
      if(!back) {
         var2 = this.relLayoutRemPhotosSectionFront;
      } else {
         var2 = this.relLayoutRemPhotosSectionBack;
      }

      CameraDetail var5;
      if(!back) {
         var5 = this.deviceCameras.getBackCameraDetail();
      } else {
         var5 = this.deviceCameras.getFrontCameraDetail();
      }

      TextView var4 = (TextView)var2.findViewById(R.id.textViewRemPhotosLabel);
      TextView var3 = (TextView)var2.findViewById(R.id.textViewRemPhotosCount);
      TextView var6 = (TextView)var2.findViewById(R.id.textViewRemMsgNoPhotosCount);
      CameraResolution resolution=var5.getMaxCameraResolution();
      var3.setText(resolution.getResolutionWidth()+" x "+resolution.getResolutionHeight());
      var4.setVisibility(View.VISIBLE);
      var3.setVisibility(View.VISIBLE);
   }
   private void prepareCameraCapacityView(boolean var1) {
      this.showCameraCapacityView(var1);
      RelativeLayout var2;
      if(var1) {
         var2 = this.relLayoutRemPhotosSectionFront;
      } else {
         var2 = this.relLayoutRemPhotosSectionBack;
      }

      int var5;
      if(var1) {
         var5 = this.remainingPicsFrontCamera;
      } else {
         var5 = this.remainingPicsBackCamera;
      }

      TextView var4 = (TextView)var2.findViewById(R.id.textViewRemPhotosLabel);
      TextView var3 = (TextView)var2.findViewById(R.id.textViewRemPhotosCount);
      TextView var6 = (TextView)var2.findViewById(R.id.textViewRemMsgNoPhotosCount);

      if(var5 >= 0) {
         var4.setText(this.getString(R.string.photo_remaining));
         var3.setText(AppUtil.toLocaleBasedNumberConversion(var5));
      } else {
         this.hideCameraCapacityView(var1);
         var6.setText(R.string.cant_get_remaining_photo);
         var6.setVisibility(0);
      }

   }

   private void prepareFrontCameraView(int var1) {
      RelativeLayout var2 = (RelativeLayout)this.rootView.findViewById(R.id.relLayoutCameraResFront);
      this.hideProgressBarCameraResolutionSection(var2);
      if(var1 == 1) {
         this.prepareViewForCameraAvailable(true);
      } else {
         ((TextView)var2.findViewById(R.id.textViewCameraPosition)).setText(R.string.front_cam);
         this.prepareViewForCameraNotAvailable(var2, var1);
      }

   }

   private void prepareViewForCameraAvailable(boolean var1) {
      int var3;
      if(var1) {
         var3 = R.id.relLayoutCameraResFront;
      } else {
         var3 = R.id.relLayoutCameraResBack;
      }

      int var4;
      if(var1) {
         var4 = R.string.front_cam;
      } else {
         var4 = R.string.back_cam;
      }

      RelativeLayout var2 = (RelativeLayout)this.rootView.findViewById(var3);
      ((TextView)var2.findViewById(R.id.textViewCameraPosition)).setText(var4);
      this.showCameraResolution(var2, var1);
      this.hideCameraNotAvailableView(var2);
      this.prepareAnimationPhotoStackForCamera(var1);
   }

   private void prepareViewForCameraNotAvailable(RelativeLayout var1, int var2) {
      this.hideCameraResolution(var1);
      this.showCameraNotAvailableView(var1, var2);
   }

   private void runStoringDeviceCameraInfoAsyncTask() {
      this.asyncTaskForStoringDeviceCamerasInfo = new AsyncTaskForStoringDeviceCamerasInfo();
      this.asyncTaskForStoringDeviceCamerasInfo.execute();
   }

   private void showCameraCapacityView(boolean var1) {
      RelativeLayout var2;
      if(var1) {
         var2 = this.relLayoutRemPhotosSectionFront;
      } else {
         var2 = this.relLayoutRemPhotosSectionBack;
      }

      ((TextView)var2.findViewById(R.id.textViewRemPhotosLabel)).setVisibility(0);
      ((TextView)var2.findViewById(R.id.textViewRemPhotosCount)).setVisibility(0);
   }

   private void showCameraNotAvailableView(RelativeLayout var1, int var2) {
      ((ImageView)var1.findViewById(R.id.imageViewCameraNotAvailable)).setVisibility(0);
      TextView var3 = (TextView)var1.findViewById(R.id.textViewCameraNotAvailable);
      var3.setVisibility(0);
      if(var2 == 2) {
         var3.setText(this.getString(R.string.message_camera_not_available));
      } else {
         var3.setText(this.getString(R.string.message_camera_not_accessible));
      }

   }

   private void showCameraResolution(RelativeLayout var1, boolean var2) {
      TextView var3 = (TextView)var1.findViewById(R.id.textViewCameraResolution);
      var3.setVisibility(0);
      TextView var4 = (TextView)var1.findViewById(R.id.textViewCameraResDenominator);
      var4.setText(this.getString(R.string.camera_megapixel));
      var4.setVisibility(0);
      CameraDetail var5;
      if(var2) {
         var5 = this.deviceCameras.getFrontCameraDetail();
      } else {
         var5 = this.deviceCameras.getBackCameraDetail();
      }

      var3.setText(AppUtil.toLocaleBasedDecimalFormat(var5.getMaxCameraResolutionInMegaPixels()));
   }

   private void showProgressBarCameraResolutionSection(RelativeLayout var1) {
      ProgressBar var2 = (ProgressBar)var1.findViewById(R.id.progressBarCalculatingRes);
      if(var2 != null) {
         var2.setVisibility(0);
      }

   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.getActivity().setTitle(R.string.camera_info);
      this.rootView = this.getView();
      this.initializeView();
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.camera_info_screen, (ViewGroup)null);
   }

   public void onDetach() {
      super.onDetach();
      this.isFragmentDetached = true;
   }

   public void onStart() {
      super.onStart();

         this.runStoringDeviceCameraInfoAsyncTask();


   }

   @Override
   public void onAttach(Context context) {
      super.onAttach(context);
      this.context=context;
//      ((HomeActivity)context).getToolbar().setBackgroundColor(getResources().getColor(R.color.camera_color));
   }

   public void onStop() {
      super.onStop();
      this.cancelAsyncTasks();
      this.frontStackAnimationDone = false;
      this.backStackAnimationDone = false;
      RelativeLayout var1 = (RelativeLayout)this.rootView.findViewById(R.id.relLayoutCameraResBack);
      this.hideCameraResolution(var1);
      this.hideCameraNotAvailableView(var1);
      this.hideCameraCapacityView(false);
      var1 = (RelativeLayout)this.rootView.findViewById(R.id.relLayoutCameraResFront);
      this.hideCameraResolution(var1);
      this.hideCameraNotAvailableView(var1);
      this.hideCameraCapacityView(true);
   }

   @SuppressWarnings("ResourceType")
   private class AsyncTaskForStoringDeviceCamerasInfo extends AsyncTask<Void,Void,DeviceCameras> {
      private AsyncTaskForStoringDeviceCamerasInfo() {
      }

       @Override
       protected DeviceCameras doInBackground(Void[] params) {
           DeviceCameras d= CameraTaskManager.getDeviceCamerasInfo();
//          try {
//            // Thread.sleep(10000L);
//          } catch (InterruptedException e) {
//             e.printStackTrace();
//          }
          return d;
       }


      protected void onPostExecute(DeviceCameras var1) {
         CameraInfoFragment.this.deviceCameras = var1;
         CameraInfoFragment.this.onDeviceCamerasInformationAvailable();
      }

      protected void onPreExecute() {
         super.onPreExecute();
         RelativeLayout var1 = (RelativeLayout)CameraInfoFragment.this.rootView.findViewById(R.id.relLayoutCameraResFront);
         CameraInfoFragment.this.showProgressBarCameraResolutionSection(var1);
         var1 = (RelativeLayout)CameraInfoFragment.this.rootView.findViewById(R.id.relLayoutCameraResBack);
         CameraInfoFragment.this.showProgressBarCameraResolutionSection(var1);
      }
   }

   private class CalculateRemainingPicturesAsyncTask extends AsyncTask<Void,Void,PhotoRemainingContainer> {
      private CalculateRemainingPicturesAsyncTask() {
      }


       @Override
       protected PhotoRemainingContainer doInBackground(Void... params) {

           PhotoRemainingContainer var2;
           try {
               var2 = CameraTaskManager.getRemainingPhotos(CameraInfoFragment.this.deviceCameras);
           } catch (CameraDirectoryNotAvailableException var3) {
               return null;
           }

           return var2;
       }

       // $FF: synthetic method
      CalculateRemainingPicturesAsyncTask(CalculateRemainingPicturesAsyncTask var2) {
         this();
      }

      private void onPhotosRemainingObtained(PhotoRemainingContainer var1) {
         if(var1 != null) {
            PhotoRemaining var3 = var1.getBackCameraPhotoRemaining();
            float var2;
            if(var3 != null) {
               CameraInfoFragment.this.remainingPicsBackCamera = var3.getCountPhotoRemaining();
               if(var3.isOverrideDeviceCameraResolution()) {
                  var2 = var3.getOverridenCameraPixels();
                  CameraInfoFragment.this.overrideCameraResolution(var2, false);
               }
            }

            PhotoRemaining var4 = var1.getFrontCameraPhotoRemaining();
            if(var4 != null) {
               CameraInfoFragment.this.remainingPicsFrontCamera = var4.getCountPhotoRemaining();
               if(var4.isOverrideDeviceCameraResolution()) {
                  var2 = var4.getOverridenCameraPixels();
                  CameraInfoFragment.this.overrideCameraResolution(var2, true);
               }
            }
         }

         AppUIUtil.hideProgressBar(CameraInfoFragment.this.progressBarRemPicsCameraBack);
         if(CameraInfoFragment.this.backStackAnimationDone) {
            CameraInfoFragment.this.prepareCameraCapacityView(false);
         }

         AppUIUtil.hideProgressBar(CameraInfoFragment.this.progressBarRemPicsCameraFront);
         if(CameraInfoFragment.this.frontStackAnimationDone) {
            CameraInfoFragment.this.prepareCameraCapacityView(true);
         }

      }


      protected void onPostExecute(PhotoRemainingContainer var1) {
         this.onPhotosRemainingObtained(var1);
      }

      protected void onPreExecute() {
         super.onPreExecute();
      }
   }
}
