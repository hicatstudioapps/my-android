package com.info.myandroid.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.info.myandroid.R;
import com.info.myandroid.utils.AppUtil;
import com.info.myandroid.utils.DeviceManager;
import com.info.myandroid.utils.PersistenceManager;

import java.io.File;

@SuppressWarnings("ResourceType")
public class MemoryFragment extends MasterFragment {

    private Activity activity;
    private boolean areTwoMemoryCardsAvailable;
    private long deviceAvailableInternalMemory;
    private long deviceAvailableSDCardSecondaryMemory;
    private long deviceAvialableExternalMemory;
    private long deviceTotalExternalMemory;
    private long deviceTotalInternalMemory;
    private long deviceTotalSDCardSecondaryMemory;
    private long deviceUsedSDCardSecondaryMemory;
    private Handler handler;
    private ImageView imageViewPhoneMemory;
    private ImageView imageViewSDCard;
    private ImageView imageViewSDCardLogo;
    private ImageView imageViewSDCardLogoSecondary;
    private ImageView imageViewSDCardSecondary;
    private boolean isExternalMemoryAvailable;
    private LinearLayout linearLayoutExternalStorage;
    private LinearLayout linearLayoutPhoneMedia;
    private TextView textViewPhoneMemoryAvailable;
    private TextView textViewPhoneMemoryAvailablePercentage;
    private TextView textViewPhoneMemoryTitle;
    private TextView textViewPhoneMemoryTotal;
    private TextView textViewPhoneMemoryUsed;
    private TextView textViewPhoneMemoryUsedPercentage;
    private TextView textViewSDCardAvailable;
    private TextView textViewSDCardAvailablePercentage;
    private TextView textViewSDCardAvailablePercentageSecondary;
    private TextView textViewSDCardAvailableSecondary;
    private TextView textViewSDCardTitle;
    private TextView textViewSDCardTitleSecondary;
    private TextView textViewSDCardTotal;
    private TextView textViewSDCardTotalSecondary;
    private TextView textViewSDCardUsed;
    private TextView textViewSDCardUsedPercentage;
    private TextView textViewSDCardUsedPercentageSecondary;
    private TextView textViewSDCardUsedSecondary;
    private long totalExternalUsed;
    private double totalExternalUsedPercentage;
    private long totalInternalUsed;
    private double totalInternalUsedPercentage;
    private double totalSDCardSecondaryUsedPercentage;


    private void adjustViewForExternalMemoryNotAvailable()
    {
        linearLayoutPhoneMedia.setVisibility(8);
        imageViewPhoneMemory.setImageResource(R.drawable.phone_memory);
        textViewPhoneMemoryTitle.setText(R.string.phone);
    }

    private void calculateExternalMemory()
    {
        Object obj = Environment.getExternalStorageDirectory();
        if (obj != null)
        {
            obj = ((File) (obj)).getPath();
            deviceTotalExternalMemory = PersistenceManager.getTotalMemorySize(((String) (obj)));
            deviceAvialableExternalMemory = PersistenceManager.getAvailableMemorySize(((String) (obj)));
            totalExternalUsed = deviceTotalExternalMemory - deviceAvialableExternalMemory;
        }
    }

    private void calculateExternalSecondaryMemory()
    {
        String s = getSdCardSecondaryPath();
        if (s != null)
        {
            deviceTotalSDCardSecondaryMemory = PersistenceManager.getTotalMemorySize(s);
            deviceAvailableSDCardSecondaryMemory = PersistenceManager.getAvailableMemorySize(s);
            deviceUsedSDCardSecondaryMemory = deviceTotalSDCardSecondaryMemory - deviceAvailableSDCardSecondaryMemory;
        }
    }

    private void calculateInternalMemory()
    {
        Object obj = Environment.getDataDirectory();
        if (obj != null)
        {
            obj = ((File) (obj)).getPath();
            deviceTotalInternalMemory = PersistenceManager.getTotalMemorySize(((String) (obj)));
            deviceAvailableInternalMemory = PersistenceManager.getAvailableMemorySize(((String) (obj)));
            totalInternalUsed = deviceTotalInternalMemory - deviceAvailableInternalMemory;
        }
    }

    private void displayExternalMemoryDataOnView()
    {
        if (DeviceManager.isPrimaryExternalStorageRemovable())
        {
            textViewSDCardTitle.setText(R.string.sd_card_external);
            imageViewSDCardLogo.setVisibility(0);
        }
        totalExternalUsedPercentage = (double)totalExternalUsed / (double)deviceTotalExternalMemory;
        int i = (int)(totalExternalUsedPercentage * 100D);
        String s = (new StringBuilder(String.valueOf(AppUtil.toLocaleBasedNumberConversion(i)))).append("%").toString();
        textViewSDCardUsedPercentage.setText(s);
        s = AppUtil.seperateValueBySpace(Formatter.formatShortFileSize(activity, totalExternalUsed));
        textViewSDCardUsed.setText(s);
        s = (new StringBuilder(String.valueOf(AppUtil.toLocaleBasedNumberConversion(100 - i)))).append("%").toString();
        textViewSDCardAvailablePercentage.setText(s);
        s = AppUtil.seperateValueBySpace(Formatter.formatShortFileSize(activity, deviceAvialableExternalMemory));
        textViewSDCardAvailable.setText(s);
        s = (new StringBuilder(String.valueOf(getString(R.string.total_)))).append(AppUtil.seperateValueBySpace(Formatter.formatShortFileSize(activity, deviceTotalExternalMemory))).toString();
        textViewSDCardTotal.setText(s);
    }

    private void displayExternalSecondaryMemoryDataOnView()
    {
        totalSDCardSecondaryUsedPercentage = (double)deviceUsedSDCardSecondaryMemory / (double)deviceTotalSDCardSecondaryMemory;
        int i = (int)(totalSDCardSecondaryUsedPercentage * 100D);
        if (DeviceManager.isPrimaryExternalStorageRemovable())
        {
            textViewSDCardTitleSecondary.setText(R.string.phone_memory_media);
            imageViewSDCardLogoSecondary.setVisibility(8);
        }
        String s = (new StringBuilder(String.valueOf(i))).append("%").toString();
        textViewSDCardUsedPercentageSecondary.setText(s);
        s = (new StringBuilder(String.valueOf(100 - i))).append("%").toString();
        textViewSDCardAvailablePercentageSecondary.setText(s);
        s = AppUtil.seperateValueBySpace(Formatter.formatShortFileSize(activity, deviceUsedSDCardSecondaryMemory));
        textViewSDCardUsedSecondary.setText(s);
        s = AppUtil.seperateValueBySpace(Formatter.formatShortFileSize(activity, deviceAvailableSDCardSecondaryMemory));
        textViewSDCardAvailableSecondary.setText(s);
        s = (new StringBuilder(String.valueOf(getString(R.string.total_)))).append(AppUtil.seperateValueBySpace(Formatter.formatShortFileSize(activity, deviceTotalSDCardSecondaryMemory))).toString();
        textViewSDCardTotalSecondary.setText(s);
    }

    private void displayInernalMemoryDataOnView()
    {
        totalInternalUsedPercentage = (double)totalInternalUsed / (double)deviceTotalInternalMemory;
        int i = (int)(totalInternalUsedPercentage * 100D);
        String s = (new StringBuilder(String.valueOf(AppUtil.toLocaleBasedNumberConversion(i)))).append("%").toString();
        textViewPhoneMemoryUsedPercentage.setText(s);
        s = AppUtil.seperateValueBySpace(Formatter.formatShortFileSize(activity, totalInternalUsed));
        textViewPhoneMemoryUsed.setText(s);
        s = (new StringBuilder(String.valueOf(AppUtil.toLocaleBasedNumberConversion(100 - i)))).append("%").toString();
        textViewPhoneMemoryAvailablePercentage.setText(s);
        s = AppUtil.seperateValueBySpace(Formatter.formatShortFileSize(activity, deviceAvailableInternalMemory));
        textViewPhoneMemoryAvailable.setText(s);
        s = (new StringBuilder(String.valueOf(getString(R.string.total_)))).append(AppUtil.seperateValueBySpace(Formatter.formatShortFileSize(activity, deviceTotalInternalMemory))).toString();
        textViewPhoneMemoryTotal.setText(s);
    }

    public static MemoryFragment getInstance()
    {
        return new MemoryFragment();
    }

    private void handleExternalSecondaryMemory()
    {
        calculateExternalSecondaryMemory();
        totalSDCardSecondaryUsedPercentage = 0.0D;
        if (areTwoMemoryCardsAvailable)
        {
            imageViewPhoneMemory.setImageResource(R.drawable.same_memory);
            imageViewSDCard.setImageResource(R.drawable.same_memory);
            imageViewSDCardSecondary.setImageResource(R.drawable.same_memory);
            if (deviceTotalSDCardSecondaryMemory == 0L)
            {
                textViewSDCardTotalSecondary.setText(R.string.no_sd_card);
                areTwoMemoryCardsAvailable = false;
                return;
            } else
            {
                displayExternalSecondaryMemoryDataOnView();
                return;
            }
        } else
        {
            hideExternalSecondaryMemoryView();
            return;
        }
    }

    private void handleInternalMemory()
    {
        calculateInternalMemory();
        displayInernalMemoryDataOnView();
    }

    private void hideExternalSecondaryMemoryView()
    {
        linearLayoutExternalStorage.setVisibility(8);
    }

    private void initViews()
    {
        View view = getView();
        imageViewSDCardLogo = (ImageView)view.findViewById(R.id.imageViewSDCardLogo);
        imageViewSDCardLogoSecondary = (ImageView)view.findViewById(R.id.imageViewSDCardLogoSecondary);
        imageViewPhoneMemory = (ImageView)view.findViewById(R.id.imageViewPhoneMemory);
        imageViewSDCard = (ImageView)view.findViewById(R.id.imageViewSDCard);
        imageViewSDCardSecondary = (ImageView)view.findViewById(R.id.imageViewSDCardSecondary);
        linearLayoutPhoneMedia = (LinearLayout)view.findViewById(R.id.llphoneMedia);
        linearLayoutExternalStorage = (LinearLayout)view.findViewById(R.id.llExternalStorage);
        textViewPhoneMemoryTitle = (TextView)view.findViewById(R.id.textViewPhoneMemoryTitle);
        textViewPhoneMemoryUsed = (TextView)view.findViewById(R.id.textViewPhoneMemoryUsedInvalue);
        textViewPhoneMemoryAvailable = (TextView)view.findViewById(R.id.textViewPhoneMemoryAvailableInvalue);
        textViewPhoneMemoryUsedPercentage = (TextView)view.findViewById(R.id.textViewPhoneMemoryUsedInPercentage);
        textViewPhoneMemoryAvailablePercentage = (TextView)view.findViewById(R.id.textViewPhoneMemoryAvailableInPercentage);
        textViewPhoneMemoryTotal = (TextView)view.findViewById(R.id.textViewPhoneMemoryTotal);
        textViewSDCardTitle = (TextView)view.findViewById(R.id.textViewSDCardTitle);
        textViewSDCardUsed = (TextView)view.findViewById(R.id.textViewSDCardUsedInValue);
        textViewSDCardAvailable = (TextView)view.findViewById(R.id.textViewSDCardAvailableInValue);
        textViewSDCardUsedPercentage = (TextView)view.findViewById(R.id.textViewSDCardUsedInPercentage);
        textViewSDCardAvailablePercentage = (TextView)view.findViewById(R.id.textViewSDCardAvailableInPercentage);
        textViewSDCardTotal = (TextView)view.findViewById(R.id.textViewSDCardTotal);
        textViewSDCardTitleSecondary = (TextView)view.findViewById(R.id.textViewSDCardTitleSecondary);
        textViewSDCardUsedSecondary = (TextView)view.findViewById(R.id.textViewSDCardUsedInValueSecondary);
        textViewSDCardAvailableSecondary = (TextView)view.findViewById(R.id.textViewSDCardAvailableInValueSecondary);
        textViewSDCardUsedPercentageSecondary = (TextView)view.findViewById(R.id.textViewSDCardUsedInPercentageSecondary);
        textViewSDCardAvailablePercentageSecondary = (TextView)view.findViewById(R.id.textViewSDCardAvailableInPercentageSecondary);
        textViewSDCardTotalSecondary = (TextView)view.findViewById(R.id.textViewSDCardTotalSecondary);
    }

    private void runAsyncTask()
    {
//        (new ShowFilledMemoryTask(imageViewPhoneMemory, totalInternalUsedPercentage)).execute(new Void[0]);
//        if (isExternalMemoryAvailable)
//        {
//            (new ShowFilledMemoryTask(imageViewSDCard, totalExternalUsedPercentage)).execute(new Void[0]);
//        }
//        if (areTwoMemoryCardsAvailable)
//        {
//            (new ShowFilledMemoryTask(imageViewSDCardSecondary, totalSDCardSecondaryUsedPercentage)).execute(new Void[0]);
//        }
    }

    private void handleExternalMemory()
    {
        isExternalMemoryAvailable = DeviceManager.isExternalMemoryAvailable();
        calculateExternalMemory();
        if (deviceTotalExternalMemory == 0L || deviceTotalExternalMemory == deviceTotalInternalMemory)
        {
            isExternalMemoryAvailable = false;
        }
        if (isExternalMemoryAvailable)
        {
            displayExternalMemoryDataOnView();
            return;
        } else
        {
            adjustViewForExternalMemoryNotAvailable();
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        handler = new Handler();
        activity = getActivity();
        activity.setTitle(R.string.memory);
        initViews();
        handleInternalMemory();
        handleExternalMemory();
        handleExternalSecondaryMemory();
        runAsyncTask();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        return layoutinflater.inflate(R.layout.memory_screen, null);
    }

    private String getSdCardSecondaryPath()
    {
//        File afile[] = (new File("/mnt")).listFiles();
//        if (afile == null) return null;
//        int j = afile.length;
//        int i = 0;
//        _L5:
//        if (i < j) goto _L3; else goto _L2
//        _L2:
//        return null;
//        _L3:
//        Object obj = afile[i];
//        if (!((File) (obj)).isDirectory())
//        {
//            break MISSING_BLOCK_LABEL_139;
//        }
//        obj = (new StringBuilder()).append(((File) (obj)).getAbsolutePath()).toString();
//        boolean flag;
//        boolean flag1;
//        boolean flag2;
//        if (((String) (obj)).indexOf("ext") > 0)
//        {
//            flag = true;
//        } else
//        {
//            flag = false;
//        }
//        if (((String) (obj)).indexOf("sd") > 0)
//        {
//            flag1 = true;
//        } else
//        {
//            flag1 = false;
//        }
//        flag2 = AppUtil.areTwoExternalMemoryCardsAvailable(((String) (obj)));
//        if (!flag && !flag1 || !flag2)
//        {
//            break MISSING_BLOCK_LABEL_139;
//        }
//        areTwoMemoryCardsAvailable = true;
//        obj = (new File(((String) (obj)))).getPath();
//        return ((String) (obj));
//        Exception exception;
//        exception;
//        i++;
//        if (true) goto _L5; else goto _L4
//        _L4:
        return "/storage/ext_sd";
    }

    private class ShowFilledMemoryTask extends AsyncTask
    {

        private ImageView imageView;
        private Bitmap memoryBitmap;
        final MemoryFragment this$0;
        private double widthPercentage;

        protected  Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected Void doInBackground(Void avoid[])
        {
//            int j;
//            int i1;
//            Paint paint;
//            int k;
//            int l;
//
//                memoryBitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap().copy(android.graphics.Bitmap.Config.ARGB_8888, true);
//                Canvas canvas = new Canvas(memoryBitmap);
//                paint = new Paint();
//                paint.setStyle(android.graphics.Paint.Style.FILL);
//                paint.setColor(-17408);
//                int i = memoryBitmap.getWidth();
//                l = memoryBitmap.getHeight();
//                i1 = (int)((double)i * widthPercentage);
//
//            j = 0;
//            if (j < i1) {
//                Thread.sleep(5L);
//                k = 0;
//            } else return null;
//            _L5:
//            Thread.sleep(5L);
//            k = 0;
//            _L3:
//            if (k < l)
//            {
//                break MISSING_BLOCK_LABEL_132;
//            }
//            handler.post(new Runnable() {
//
//                public void run()
//                {
//                    imageView.setImageBitmap(memoryBitmap);
//                }
//
//
//
//            });
//            j++;
//            continue; /* Loop/switch isn't completed */
//            avoid.drawPoint(j, k, paint);
//            k++;
//            if (true) goto _L3; else goto _L2
//            _L2:
//            _L1:
//            if (j < i1) goto _L5; else goto _L4
//            _L4:
            return null;
        }



        public ShowFilledMemoryTask(ImageView imageview, double d)
        {
            this$0 = MemoryFragment.this;
            imageView = imageview;
            widthPercentage = d;
        }
    }
}