package com.info.myandroid.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

import com.info.myandroid.R;
import com.info.myandroid.utils.AppUtil;
import com.info.myandroid.utils.DeviceManager;
import com.info.myandroid.utils.DeviceScreenSize;

@SuppressWarnings("ResourceType")
public class ScreenSizeFragment extends MasterFragment {
   private static final int DEVICE_IMAGE_REDUCTION_FACTOR = 2;
   private Animation animationFadeIn;
   private int deviceHeight;
   private int deviceWidth;
   private Runnable imageDimensionsRunnable = new GetImageDimensions((GetImageDimensions)null);
   private int imageHeight;
   private ImageView imageViewScreen;
   private int imageViewTop;
   private int imageViewleft;
   private int imageWidth;
   private TextView mTextViewApprox;
   private Runnable setHeightRunnable = new SetScreenHeight((SetScreenHeight)null);
   private Runnable setWidthRunnable = new SetScreenWidth((SetScreenWidth)null);
   private Animation.AnimationListener showDimensionAnimationListener = new Animation.AnimationListener() {
      public void onAnimationEnd(Animation var1) {
         int var2 = ScreenSizeFragment.this.getResources().getColor(17170443);
         ScreenSizeFragment.this.textViewDeviceHeight.setBackgroundColor(var2);
         ScreenSizeFragment.this.textViewDeviceWidth.setBackgroundColor(var2);
         ScreenSizeFragment.this.mTextViewApprox.setVisibility(0);
         ScreenSizeFragment.this.textViewScreenDimension.setText(AppUtil.toLocaleBasedNumberConversion(ScreenSizeFragment.this.deviceHeight) + " x " + AppUtil.toLocaleBasedNumberConversion(ScreenSizeFragment.this.deviceWidth) + " " + ScreenSizeFragment.this.getString(R.string._pixels));
         ScreenSizeFragment.this.textViewScreenSizeInInches.setText(ScreenSizeFragment.this.getScreenSizesInInches() + " " + ScreenSizeFragment.this.getString(R.string._inches));
         ScreenSizeFragment.this.textViewScreenSizeInInches.startAnimation(ScreenSizeFragment.this.animationFadeIn);
         ScreenSizeFragment.this.textViewScreenDimension.startAnimation(ScreenSizeFragment.this.animationFadeIn);
         ScreenSizeFragment.this.mTextViewApprox.startAnimation(ScreenSizeFragment.this.animationFadeIn);
      }

      public void onAnimationRepeat(Animation var1) {
      }

      public void onAnimationStart(Animation var1) {
      }
   };
   private TextView textViewDeviceHeight;
   private TextView textViewDeviceWidth;
   private TextView textViewScreenDimension;
   private TextView textViewScreenSizeInInches;

   public static ScreenSizeFragment getInstance() {
      return new ScreenSizeFragment();
   }

   private void initViews() {
      View var1 = this.getView();
      this.imageViewScreen = (ImageView)var1.findViewById(R.id.imageViewScreen);
      this.prepareDeviceImageSize(this.imageViewScreen);
      this.imageViewScreen.post(this.imageDimensionsRunnable);
      this.textViewDeviceWidth = (TextView)var1.findViewById(R.id.imageViewScreenTextWidth);
      this.textViewDeviceWidth.post(this.setWidthRunnable);
          this.textViewDeviceHeight = (TextView)var1.findViewById(R.id.imageViewScreenTextHeight);
      this.textViewDeviceHeight.post(this.setHeightRunnable);
      this.textViewScreenDimension = (TextView)var1.findViewById(R.id.textViewScreenDimension);
      this.textViewScreenSizeInInches = (TextView)var1.findViewById(R.id.textViewScreenSizeInInches);
      this.mTextViewApprox = (TextView)var1.findViewById(R.id.textSizeApprox);
   }

   private void moveViewToPosition(View var1, int var2, int var3, int var4, int var5) {
      TranslateAnimation var6 = new TranslateAnimation((float)var2, (float)var3, (float)var4, (float)var5);
      var6.setDuration(1000L);
      var6.setFillAfter(true);
      var6.setAnimationListener(this.showDimensionAnimationListener);
      var1.startAnimation(var6);
   }

   private void prepareDeviceImageSize(ImageView var1) {
      LayoutParams var2 = (LayoutParams)var1.getLayoutParams();
      if(this.deviceWidth < this.deviceHeight) {
         var2.width = this.deviceWidth / 2;
      } else {
         var2.height = this.deviceHeight / 2;
      }

      var1.setLayoutParams(var2);
   }

   public String getScreenSizesInInches() {
      DisplayMetrics var1 = new DisplayMetrics();
      this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(var1);
      int var2;
      int var3;
      if(this.getResources().getConfiguration().orientation == 2) {
         var2 = (int)var1.xdpi;
         var3 = (int)var1.ydpi;
      } else {
         var3 = (int)var1.xdpi;
         var2 = (int)var1.ydpi;
      }

      return AppUtil.toLocaleBasedDecimalFormat(Math.sqrt(Math.pow((double)this.deviceWidth / (double)var3, 2.0D) + Math.pow((double)this.deviceHeight / (double)var2, 2.0D)));
   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      DeviceScreenSize var2 = DeviceManager.getDevicePhysicalScreenSize();
      this.deviceHeight = var2.getScreenHeightInPixels();
      this.deviceWidth = var2.getScreenWidthInPixels();
      FragmentActivity var3 = this.getActivity();
      var3.setTitle(R.string.screen_size);
      this.initViews();
      this.textViewDeviceHeight.setText(AppUtil.toLocaleBasedNumberConversion(this.deviceHeight));
      this.textViewDeviceWidth.setText(AppUtil.toLocaleBasedNumberConversion(this.deviceWidth));
      this.animationFadeIn = AnimationUtils.loadAnimation(var3, R.anim.fade_in);
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.device_size_screen, (ViewGroup)null);
   }

   private class GetImageDimensions implements Runnable {
      private GetImageDimensions() {
      }

      // $FF: synthetic method
      GetImageDimensions(GetImageDimensions var2) {
         this();
      }

      public void run() {
         ScreenSizeFragment.this.imageWidth = ScreenSizeFragment.this.imageViewScreen.getWidth();
         ScreenSizeFragment.this.imageHeight = ScreenSizeFragment.this.imageViewScreen.getHeight();
         ScreenSizeFragment.this.imageViewleft = ScreenSizeFragment.this.imageViewScreen.getLeft();
         ScreenSizeFragment.this.imageViewTop = ScreenSizeFragment.this.imageViewScreen.getTop();
      }
   }

   private class SetScreenHeight implements Runnable {
      private SetScreenHeight() {
      }

      // $FF: synthetic method
      SetScreenHeight(SetScreenHeight var2) {
         this();
      }

      public void run() {
         LayoutParams var1 = (LayoutParams)ScreenSizeFragment.this.textViewDeviceHeight.getLayoutParams();
         int var6 = ScreenSizeFragment.this.imageViewleft;
         int var2 = ScreenSizeFragment.this.imageWidth * 1 / 4;
         int var3 = ScreenSizeFragment.this.imageViewTop;
         int var4 = ScreenSizeFragment.this.imageHeight / 2;
         int var7 = ScreenSizeFragment.this.textViewDeviceHeight.getWidth();
         int var5 = ScreenSizeFragment.this.textViewDeviceHeight.getHeight();
         var1.setMargins(var6 + var2 - var7 / 2, 0, 0, 0);
         ScreenSizeFragment.this.textViewDeviceHeight.setLayoutParams(var1);
         ScreenSizeFragment.this.moveViewToPosition(ScreenSizeFragment.this.textViewDeviceHeight, 0, 0, 0, var3 + var4 - var5 / 2);
      }
   }

   private class SetScreenWidth implements Runnable {
      private SetScreenWidth() {
      }

      // $FF: synthetic method
      SetScreenWidth(SetScreenWidth var2) {
         this();
      }

      public void run() {
         LayoutParams var1 = (LayoutParams)ScreenSizeFragment.this.textViewDeviceWidth.getLayoutParams();
         int var2 = ScreenSizeFragment.this.imageViewleft;
         int var6 = ScreenSizeFragment.this.imageWidth / 2;
         int var5 = ScreenSizeFragment.this.imageViewTop;
         int var4 = ScreenSizeFragment.this.imageHeight * 3 / 4;
         int var3 = ScreenSizeFragment.this.textViewDeviceWidth.getHeight();
         int var7 = ScreenSizeFragment.this.textViewDeviceWidth.getWidth();
         var1.setMargins(0, var5 + var4 - var3 / 2, 0, 0);
         ScreenSizeFragment.this.textViewDeviceWidth.setLayoutParams(var1);
         ScreenSizeFragment.this.moveViewToPosition(ScreenSizeFragment.this.textViewDeviceWidth, 0, var2 + var6 - var7 / 2, 0, 0);
      }
   }
}
