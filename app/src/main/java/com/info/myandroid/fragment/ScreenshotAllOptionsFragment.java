package com.info.myandroid.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Scroller;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.R;
import com.info.myandroid.utils.AppConstants;
import com.info.myandroid.utils.AppUIUtil;
import com.info.myandroid.utils.DeviceInfo;
import com.info.myandroid.utils.DeviceManager;
import com.info.myandroid.utils.DeviceScreenshot;
import com.info.myandroid.utils.PersistenceManager;
import com.info.myandroid.utils.Screenshot;

import java.lang.reflect.Field;

@SuppressWarnings("ResourceType")
public class ScreenshotAllOptionsFragment extends MasterFragment {
   private Activity activity;
   private PagerAdapter adapter;
   private AlertDialog alertDialogNoMoreOption;
   private ButtonClickListerner buttonClickListener;
   private Button buttonNo;
   private Button buttonYes;
   private ImageView imageViewNext;
   private ImageView imageViewPrevious;
   private boolean isbuttonPressed;
   private RelativeLayout relativeLayoutScreenshotImage;
   private Resources resources;
   private ScreenshotPageChangeListener screenshotPageChangeListener;
   private CustomScroller scroller;
   private ViewPager viewPager;

   public static ScreenshotAllOptionsFragment getInstance() {
      return new ScreenshotAllOptionsFragment();
   }

   private void handleMoreOption(int var1) {
      this.isbuttonPressed = true;
      this.viewPager.setCurrentItem(this.viewPager.getCurrentItem() + var1, true);
   }

   private void handleNo() {
      if(this.viewPager.getCurrentItem() != AppConstants.SCREENSHOT_OPTIONS_STACK.length - 1) {
         this.isbuttonPressed = true;
         this.viewPager.setCurrentItem(this.viewPager.getCurrentItem() + 1, true);
      } else {
         this.alertDialogNoMoreOption = this.showNoMoreOptionsDialog();
      }

   }

   private void handleYes() {
      AppUIUtil.showToastNotification(this.activity, this.activity.getString(R.string.toast_message_screenshot_option_great), (String) null);
//      int var3 = this.viewPager.getCurrentItem();
//      String var1 = AppConstants.SCREENSHOT_OPTIONS_STACK[var3 + 1 - 1];
//      PersistenceManager.persistDeviceScreenshotOption(var1);
//      DeviceScreenshot var4 = this.prepareDeviceScreenshot(var1);
//      Intent var2 = new Intent(this.activity, DeviceInfoIntentService.class);
//      var2.putExtra("INTENT_KEY_DEVICE_SCREENSHOT", var4);
//      var2.putExtra("INTENT_SERVICE_TASK", "SAVE");
//      this.activity.startService(var2);
     // this.launchScreenshotMyOptionFragment();
   }

   private void initializeView() {
      View var1 = this.getView();
      this.buttonClickListener = new ButtonClickListerner((ButtonClickListerner)null);
      this.screenshotPageChangeListener = new ScreenshotPageChangeListener((ScreenshotPageChangeListener)null);
      this.adapter = new ScreenshotFragmentAdapter(this.getFragmentManager());
      this.viewPager = (ViewPager)var1.findViewById(R.id.pager);
      this.viewPager.setAdapter(this.adapter);
      this.viewPager.setOnPageChangeListener(this.screenshotPageChangeListener);
      this.useCustomScroller();
      this.imageViewNext = (ImageView)this.activity.findViewById(R.id.imageViewNext);
      this.imageViewPrevious = (ImageView)this.activity.findViewById(R.id.imageViewPrevious);
      this.imageViewNext.setOnClickListener(this.buttonClickListener);
      this.imageViewPrevious.setOnClickListener(this.buttonClickListener);
      this.setNextPreviousVisibility();
      this.buttonYes = (Button)var1.findViewById(R.id.buttonYes);
      this.buttonNo = (Button)var1.findViewById(R.id.buttonNo);
      this.relativeLayoutScreenshotImage = (RelativeLayout)var1.findViewById(R.id.relativeLayoutScreenshotImage);
      this.resources = this.getActivity().getResources();
      Drawable var3 = this.resources.getDrawable(R.drawable.screenshot_option_home_vol_down);
      ViewGroup.LayoutParams var2 = this.relativeLayoutScreenshotImage.getLayoutParams();
      var2.height = var3.getIntrinsicHeight();
      this.relativeLayoutScreenshotImage.setLayoutParams(var2);
      this.buttonYes.setOnClickListener(this.buttonClickListener);
      this.buttonNo.setOnClickListener(this.buttonClickListener);
   }

   private void launchScreenshotMyOptionFragment() {
      ScreenshotMyOptionFragment var1 = ScreenshotMyOptionFragment.getInstance();
      Bundle var2 = new Bundle();
      var2.putBoolean("SHOULD_SHOW_MORE_OPTION", false);
      var1.setArguments(var2);
      FragmentTransaction var3 = this.getActivity().getSupportFragmentManager().beginTransaction();
      var3.replace(R.id.container, var1);
      var3.commit();
   }

   private DeviceScreenshot prepareDeviceScreenshot(String var1) {
      DeviceInfo var3 = DeviceManager.getDeviceInfo();
      DeviceScreenshot var2 = new DeviceScreenshot();
      new Screenshot();
      var2.setDeviceInfo(var3);
      Screenshot var4 = new Screenshot();
      var4.setScreenshotOptionCode(var1);
      var2.setScreenshot(var4);
      return var2;
   }

   private void setNextPreviousVisibility() {
      if(this.viewPager.getCurrentItem() == 0) {
         this.imageViewPrevious.setVisibility(4);
         this.imageViewNext.setVisibility(0);
      } else if(this.viewPager.getCurrentItem() == 3) {
         this.imageViewNext.setVisibility(4);
         this.imageViewPrevious.setVisibility(0);
      } else {
         this.imageViewNext.setVisibility(0);
         this.imageViewPrevious.setVisibility(0);
      }

   }

   private AlertDialog showNoMoreOptionsDialog() {
      String var4 = this.getString(R.string.dialog_title_screenshot_no_more_suggestion);
      String var5 = this.getString(R.string.dialog_message_screenshot_try_again);
      String var1 = this.getString(R.string.button_text_retry_now);
      String var3 = this.getString(17039360);
      final AlertDialog var2 = (new AlertDialog.Builder(this.activity)).create();
      var2.setTitle(var4);
      var2.setMessage(var5);
      var2.setButton(-1, var1, new DialogInterface.OnClickListener() {
         public void onClick(DialogInterface var1, int var2x) {
            var2.dismiss();
            ScreenshotAllOptionsFragment.this.viewPager.setCurrentItem(0);
         }
      });
      var2.setButton(-2, var3, new DialogInterface.OnClickListener() {
         public void onClick(DialogInterface var1, int var2x) {
            var2.dismiss();
         }
      });
      var2.setCancelable(false);
      var2.show();
      return var2;
   }

   private void useCustomScroller() {
      try {
         Field var1 = ViewPager.class.getDeclaredField("mScroller");
         var1.setAccessible(true);
         this.scroller = new CustomScroller(this.activity);
         var1.set(this.viewPager, this.scroller);
      } catch (Exception var2) {
         var2.printStackTrace();
      }

   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.getActivity().setTitle(R.string.screen_shot);
      this.setRetainInstance(true);
      this.initializeView();
   }

   public void onAttach(Context var1) {
      super.onAttach(var1);
      this.activity = (HomeActivity)var1;
//      ((HomeActivity)var1).getToolbar().setBackgroundColor(getResources().getColor(R.color.screen_color));

   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.screenshot_all_options_screen, (ViewGroup)null);
   }

   private class ButtonClickListerner implements View.OnClickListener {
      private ButtonClickListerner() {
      }

      // $FF: synthetic method
      ButtonClickListerner(ButtonClickListerner var2) {
         this();
      }

      public void onClick(View var1) {
         switch(var1.getId()) {
         case R.id.imageViewNext:
            ScreenshotAllOptionsFragment.this.handleMoreOption(1);
            break;
         case R.id.imageViewPrevious:
            ScreenshotAllOptionsFragment.this.handleMoreOption(-1);
            break;
//         case 2131493062:
//         case 2131493063:
//         default:
//            break;
         case R.id.buttonYes:
            ScreenshotAllOptionsFragment.this.handleYes();
            break;
         case R.id.buttonNo:
            ScreenshotAllOptionsFragment.this.handleNo();
         }

      }
   }

   private class CustomScroller extends Scroller {
      public CustomScroller(Context var2) {
         super(var2);
      }

      public void startScroll(int var1, int var2, int var3, int var4) {
         super.startScroll(var1, var2, var3, var4);
      }

      public void startScroll(int var1, int var2, int var3, int var4, int var5) {
         if(ScreenshotAllOptionsFragment.this.isbuttonPressed) {
            super.startScroll(var1, var2, var3, var4, 2000);
         } else {
            super.startScroll(var1, var2, var3, var4, var5);
         }

         ScreenshotAllOptionsFragment.this.isbuttonPressed = false;
      }
   }

   private class ScreenshotPageChangeListener implements ViewPager.OnPageChangeListener {
      private ScreenshotPageChangeListener() {
      }

      // $FF: synthetic method
      ScreenshotPageChangeListener(ScreenshotPageChangeListener var2) {
         this();
      }

      public void onPageScrollStateChanged(int var1) {
      }

      public void onPageScrolled(int var1, float var2, int var3) {
      }

      public void onPageSelected(int var1) {
         ScreenshotAllOptionsFragment.this.setNextPreviousVisibility();
      }
   }
}
