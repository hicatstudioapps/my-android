package com.info.myandroid.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.MyAndroidApplication;
import com.info.myandroid.R;
import com.info.myandroid.utils.AppUtil;

@SuppressWarnings("ResourceType")
public class InternetStatusFragment extends MasterFragment {
   private AppCompatActivity activity;
   private ImageView imageViewInternetStatus;
   private TextView textViewConnectedTo;
   private TextView textViewMobileNetworkText;
   private TextView textViewWiFiName;
   private TextView textViewWiFiText;
   private TextView result;

   private void checkMobilTypeConnection(NetworkInfo var1) {
      switch(var1.getSubtype()) {
      case 0:
         this.internetConnectionMobileNetworkUnknown(R.string.mobile_network_type_unknown);
         break;
      case 1:
      case 2:
      case 4:
      case 7:
      case 11:
         this.internetConnectionMobile(R.string._2g);
         break;
      case 3:
      case 5:
      case 6:
      case 8:
      case 9:
      case 10:
      case 12:
      case 14:
      case 15:
         this.internetConnectionMobile(R.string._3g);
         break;
      case 13:
             this.internetConnectionMobile(R.string._4g);
         break;
      default:
         this.noInternetConnection();
      }

   }

   public static InternetStatusFragment getInstance() {
      return new InternetStatusFragment();
   }

   @Override
   public void onAttach(Context context) {
      super.onAttach(context);
//      ((HomeActivity)context).getToolbar().setBackgroundColor(getResources().getColor(R.color.internet));
   }
   private void internetConnectionMobile(int var1) {
      this.textViewMobileNetworkText.setText(this.getString(var1));
      this.result.setText(this.getString(var1));
//      this.imageViewInternetStatus.setVisibility(8);
//      this.textViewWiFiName.setVisibility(8);
//      this.textViewWiFiText.setVisibility(8);
   }

   private void internetConnectionMobileNetworkUnknown(int var1) {
      float var2 = this.activity.getResources().getDimension(R.dimen.text_size_mobile_network_unknown);
      this.textViewMobileNetworkText.setTextSize(var2);
      this.textViewMobileNetworkText.setText(this.getString(var1));
      this.result.setText(this.getString(var1));
//      this.imageViewInternetStatus.setVisibility(8);
//      this.textViewWiFiName.setVisibility(8);
//      this.textViewWiFiText.setVisibility(8);
   }

   private void internetConnectionWiFi(Context var1) {
      String var2 = AppUtil.removePrefixSuffix(((WifiManager) var1.getSystemService("wifi")).getConnectionInfo().getSSID(), "\"");
//      this.textViewMobileNetworkText.setVisibility(8);
//      this.textViewWiFiName.setText(var2);
//      this.imageViewInternetStatus.setImageResource(R.drawable.internet_wifi);
      this.result.setText("WI-FI");
   }

   private void noInternetConnection() {
      //this.imageViewInternetStatus.setImageResource(R.drawable.not_available);
      this.result.setText(R.string.no_internet);
      //this.textViewWiFiName.setText(R.string.no_internet);
//      this.textViewMobileNetworkText.setVisibility(8);
//      this.textViewConnectedTo.setVisibility(8);
//      this.textViewWiFiText.setVisibility(8);
   }

   private void resetVisibility() {
      this.textViewConnectedTo.setVisibility(0);
      this.textViewWiFiName.setVisibility(0);
      this.textViewWiFiText.setVisibility(0);
   }

   public void getNetworkClass(Context var1) {
      NetworkInfo var2 = ((ConnectivityManager)var1.getSystemService("connectivity")).getActiveNetworkInfo();
      if(var2 != null) {
         if(var2.getType() == 1) {
            this.internetConnectionWiFi(var1);
         } else {
            this.checkMobilTypeConnection(var2);
         }
      } else {
         this.noInternetConnection();
      }

   }

   public void onActivityCreated(Bundle var1) {
      super.onActivityCreated(var1);
      this.getActivity().setTitle(R.string.internet_status);
      View var2 = this.getView();
      this.imageViewInternetStatus = (ImageView)var2.findViewById(R.id.imageViewInternetStatus);
      this.textViewWiFiName = (TextView)var2.findViewById(R.id.textViewWiFiName);
      this.textViewMobileNetworkText = (TextView)var2.findViewById(R.id.textViewMobileNetworkText);
      this.textViewWiFiText = (TextView)var2.findViewById(R.id.textViewWiFiText);
      this.textViewConnectedTo = (TextView)var2.findViewById(R.id.textViewConnectedTo);
      this.result=((TextView)var2.findViewById(R.id.textView20));
   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
   }

   public View onCreateView(LayoutInflater var1, ViewGroup var2, Bundle var3) {
      return var1.inflate(R.layout.internet_status_screen, (ViewGroup)null);
   }

   public void onDestroy() {
      super.onDestroy();
   }

   public void onResume() {
      super.onResume();
      this.resetVisibility();
      this.getNetworkClass(MyAndroidApplication.getAppContext());
   }
}
