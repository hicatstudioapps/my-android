package com.info.myandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import badabing.lib.apprater.AppRater;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ScreenCaptureActivity extends AppCompatActivity {

    int [] tut_images= new int[]{R.drawable.screen1_03,R.drawable.screen2_03,R.drawable.screen3_03,R.drawable.screnn4_03};
    @InjectView(R.id.prev)
    View  prev;
    @InjectView(R.id.next)
    View next;
    @InjectView(R.id.imageView5)
    ImageView imageView;
    int position=0;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_capture);
        AdRequest var13;
        try {
            this.adView = (AdView)this.findViewById(R.id.adView);
            var13 = (new AdRequest.Builder()).build();
            this.adView.loadAd(var13);
            this.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    adView.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception var8) {
            ;
        }
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.screen_color));
        toolbar.setTitle(R.string.screen_capture);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setSupportActionBar(toolbar);
        Glide.with(this).load(tut_images[0]).into(imageView);
    }

    @OnClick({R.id.next,R.id.prev})
    public void click(View view){
        switch (view.getId()){
            case R.id.prev:
                Glide.with(this).load(tut_images[--position]).into(imageView);
                next.setVisibility(View.VISIBLE);
                if(position<=0){
                    position=0;
                    prev.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.next:
                Glide.with(this).load(tut_images[++position]).into(imageView);
                prev.setVisibility(View.VISIBLE);
                if(position>=2){
                    position=2;
                    next.setVisibility(View.INVISIBLE);
                }
                break;

        }
        if(position<=0)
            position=0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu var1) {
        this.getMenuInflater().inflate(R.menu.menu_home_screen, var1);
        return true;

    }
    public boolean onOptionsItemSelected(MenuItem var1) {
        switch (var1.getItemId()) {
            case android.R.id.home:
                finish();
                break;

//      case 16908332:
//         this.toggleDrawer();
//         break;
//      case 2131493078:
//         this.toggleDrawer();
//         break;
            case R.id.menu_item_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                String text = String.format(getString(R.string.share_text), getString(R.string.app_name), "https://play.google.com/store/apps/details?id="+ getPackageName());
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.menu_item_rate:
                AppRater.rateNow(this);
                break;
        }

        return super.onOptionsItemSelected(var1);
    }
}
