package com.info.myandroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.info.myandroid.utils.CallContactDetailsData;
import com.info.myandroid.utils.CallDetailsInfo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by La Costra on 01/09/2015.
 */
public class RVFakeRecordAdapter extends RecyclerView.Adapter<RVFakeRecordAdapter.ViewHolder> {

    ArrayList<CallContactDetailsData> items;
    DisplayImageOptions options;

    Context context;
    public RVFakeRecordAdapter(ArrayList items,Context context) {
        this.items = items;
        this.options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        this.context=context;
    }
    public void delteItem(int adapterPosition) {

    items.remove(adapterPosition);

        notifyItemRemoved(adapterPosition);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.call_details_known_detail_summary_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(items.get(position).getName());
        holder.number.setText(items.get(position).getNumber());
        SimpleDateFormat format= new SimpleDateFormat("HH:mm");
        long time=((CallDetailsInfo) items.get(position).getCallsMultipleDatas().get(0)).getCallDate();
        String f= format.format(new Date(time));
        int min=(int)time/60/60;
        int seg=(int)time/60%60;
        String m="";
        if(min+"".length()<2)
            m="0"+min;
        else
        m=min+"";
        String s="";
        if(seg+"".length()<2)
            s="0"+seg;
        else
        s=seg+"";
        holder.time.setText(f);
        switch (((CallDetailsInfo)items.get(position).getCallsMultipleDatas().get(0)).getCallType()){
            case 1:
                holder.call_type.setImageResource(R.drawable.in);
                break;
            case 2:
                holder.call_type.setImageResource(R.drawable.out);
                break;
            default:
                holder.call_type.setImageResource(R.drawable.miss);
        }
//        File image=new File(items.get(position).imagePath.toString());
//        Uri img;
//        if(!items.get(position).imagePath.toString().contains("content:"))
//            img= Uri.fromFile(image);
//        else
//            img=items.get(position).imagePath;
//        ImageLoader.getInstance()
//                .displayImage("content://com.android.contacts/data/264", holder.cimage, options, new SimpleImageLoadingListener() {
//                    @Override
//                    public void onLoadingStarted(String imageUri, View view) {
//
//                    }
//
//                    @Override
//                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                    }
//
//                    @Override
//                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                    }
//                });
//        if(!items.get(position).imagePath.toString().isEmpty())
//        holder.cimage.setImageURI(items.get(position).imagePath);
//        else
//            holder.cimage.setImageResource(R.drawable.avatar);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView name;
        public TextView number;
        public TextView time;
        public ImageView call_type;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
//            name= (TextView)itemView.findViewById(R.id.name);
//            number= (TextView)itemView.findViewById(R.id.number);
//            time= (TextView) itemView.findViewById(R.id.textView18);
//            call_type=(ImageView)itemView.findViewById(R.id.imageView6);

        }

        @Override
        public void onClick(View v) {
//            switch (v.getId()){
////                case R.id.imageView3:
////                    delteItem(getAdapterPosition());
////                    break;
//                case R.id.imageView9:
//                    delteItem(getAdapterPosition());
//                    break;
//                default:
//                    Intent intent= new Intent(context,Profilecustom.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    Bundle bundle= new Bundle();
//                    //nombre
//                    bundle.putBoolean("list", true);
//                    String [] array= context.getResources().getStringArray(R.array.time_options);
//                    MyApplication.putProfile(context, items.get(getAdapterPosition()).cname
//                            , items.get(getAdapterPosition()).cnum,
//                            items.get(getAdapterPosition()).imagePath.toString()
//                            , array[Integer.parseInt(items.get(getAdapterPosition()).time)], items.get(getAdapterPosition()).rec, "y", items.get(getAdapterPosition()).id,items.get(getAdapterPosition()).delay,
//                            items.get(getAdapterPosition()).ringtone);
//                    ((Listviewimage)context).finish();
//            }
        }
    }
}

