package com.info.myandroid;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.info.myandroid.fragment.CallDetailsFragment;
import com.info.myandroid.fragment.InstalledAppsFragment;
import com.info.myandroid.utils.AppUtil;
import com.info.myandroid.utils.BaseActivity;
import com.info.myandroid.utils.DeviceSpecsItem;
import com.info.myandroid.utils.MemoryActivity;
import com.lacostra.utils.notification.Permission.Permission;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Locale;

import badabing.lib.ServerUtilities;
import badabing.lib.apprater.AppRater;
import badabing.lib.handler.ExceptionHandler;
import butterknife.ButterKnife;
import butterknife.InjectView;

import static android.Manifest.permission.CAMERA;

import static android.Manifest.permission.READ_CONTACTS;

import static android.Manifest.permission.READ_CALL_LOG;

public class MenuActivity extends BaseActivity {
   private AdView adView;
   @InjectView(R.id.toolbar)
   Toolbar toolbar ;
   @InjectView(R.id.android_version)
   TextView android_version;
   @InjectView(R.id.ram)
   TextView ram;
   @InjectView(R.id.model)
   TextView model;
   private String ramSizeUnit;

   private final int CAMERA_R=1;
   private int CALL=2;
   private int CONTACT=3;


   @Override
   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
      //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
      switch (requestCode){
         case 4:
            if(Permission.hasPermission(this, READ_CONTACTS) && Permission.hasPermission(this,READ_CALL_LOG)){
               Intent var7 = new Intent(this, HomeActivity.class);
               var7.putExtra("KEY_POSITION", 4);
               startActivity(var7);}
            else{
               // showAPILevelFragment();
               Toast.makeText(this,"You must accept the requested permission in order to access these feature!",Toast.LENGTH_LONG).show();
            }
            break;
         case 5:
            if(Permission.hasPermission(this, CAMERA)){
               Intent var7 = new Intent(this, HomeActivity.class);
            var7.putExtra("KEY_POSITION", requestCode);
            startActivity(var7);}
            else{
               // showAPILevelFragment();
               Toast.makeText(this,"You must accept the requested permission in order to access these feature!",Toast.LENGTH_LONG).show();
            }
            break;
      }
   }

   public void requestCamera(){
      ArrayList<String> perm= new ArrayList<>();
      perm.add(CAMERA);
      askPermision(perm,5);
   }

   public void requestCall(){
      ArrayList<String> perm= new ArrayList<>();
      perm.add(READ_CONTACTS);
      perm.add(READ_CALL_LOG);
      askPermision(perm, 4);
   }
   @SuppressLint({"NewApi"})
   private void changeTitleBarAppearance() {
//      int var3 = VERSION.SDK_INT;
//      Drawable var1 = this.getResources().getDrawable(R.drawable.blue_color_action_bar);
//      if(var3 >= 11) {
//         ActionBar var2 = this.getActionBar();
//         if(var2 != null) {
//            var2.setBackgroundDrawable(var1);
//         }
//      } else {
//         TextView var4 = (TextView)this.getWindow().findViewById(16908310);
//         if(var4 != null) {
//            ViewParent var5 = var4.getParent();
//            if(var5 != null && var5 instanceof View) {
//               ((View)var5).setBackgroundResource(2130837513);
//            }
//         }
//      }

   }

   private static DeviceSpecsItem prepareMenuObject(String var0, int var1, int var2) {
      DeviceSpecsItem var3 = new DeviceSpecsItem();
      var3.setDisplayText(var0);
      var3.setDrawableId(var1);
      var3.setItemType(var2);
      return var3;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem var1) {
      switch (var1.getItemId()){
      case R.id.menu_item_share:
      Intent sendIntent = new Intent();
      sendIntent.setAction(Intent.ACTION_SEND);
      String text = String.format(getString(R.string.share_text), getString(R.string.app_name), "https://play.google.com/store/apps/details?id="+ getPackageName());
      sendIntent.putExtra(Intent.EXTRA_TEXT, text);
      sendIntent.setType("text/plain");
      startActivity(sendIntent);
      break;
      case R.id.menu_item_rate:
      AppRater.rateNow(MenuActivity.this);
      break;
      }
      return true;
   }

   public void onBluetooth(){
      startActivityForResult(new Intent(
              BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
   }
   private ArrayList prepareMenuScreenList() {
      ArrayList var1 = new ArrayList();
      var1.add(prepareMenuObject(this.getString(R.string.section_tittle_1), R.drawable.aplicaciones, 0));
      var1.add(prepareMenuObject(this.getString(R.string.wifi_text), R.drawable.wiffi, 20));
      var1.add(prepareMenuObject(this.getString(R.string.bletoo), R.drawable.blu, 30));
//      var1.add(prepareMenuObject(this.getString(R.string.data), R.drawable.data, 40));
      var1.add(prepareMenuObject(this.getString(R.string.section_tittle_2), R.drawable.aplicaciones, 50));


      var1.add(prepareMenuObject(this.getString(R.string.search), R.drawable.aplicaciones, 1));
      var1.add(prepareMenuObject(this.getString(R.string.memory), R.drawable.sd, 2));
      var1.add(prepareMenuObject(this.getString(R.string.batery), R.drawable.bateria, 14));
     // var1.add(prepareMenuObject(this.getString(R.string.screen_shot), R.drawable.icon_screenshot, 3));
      var1.add(prepareMenuObject(this.getString(R.string.call_details_title), R.drawable.llamadas ,4));
      var1.add(prepareMenuObject(this.getString(R.string.camera_info), R.drawable.camara, 5));
     // var1.add(prepareMenuObject(this.getString(R.string.internet_status), R.drawable.icon_network, 6));
      var1.add(prepareMenuObject(this.getString(R.string.screen_shot), R.drawable.screenshot, 7));
      var1.add(prepareMenuObject(this.getString(R.string.internet_status), R.drawable.internet, 8));
      var1.add(prepareMenuObject(this.getString(R.string.screnn_size), R.drawable.tamano, 9));
      var1.add(prepareMenuObject(this.getString(R.string.screen_density), R.drawable.densidad, 10));
      var1.add(prepareMenuObject(this.getString(R.string.ram), R.drawable.ram, 11));
      var1.add(prepareMenuObject(this.getString(R.string.os_version), R.drawable.version, 12));
      var1.add(prepareMenuObject(this.getString(R.string.manufacturer_title), R.drawable.fabricante, 13));
      //var1.add(prepareMenuObject(this.getString(R.string.screen_density), R.drawable.icon_density, 8));
     // var1.add(prepareMenuObject(this.getString(R.string.ram), R.drawable.icon_ram, 9));
      //var1.add(prepareMenuObject(this.getString(R.string.os_version), R.drawable.icon_android_version, 10));
      //var1.add(prepareMenuObject(this.getString(R.string.manufacturer_title), R.drawable.icon_manufacturer_model, 11));
      return var1;
   }

   public void loadAdvertisement() {
      AdRequest var1 = AppUtil.getAdRequest();
      this.adView = (AdView)this.findViewById(R.id.adView);
      if(this.adView != null) {
         this.adView.loadAd(var1);
      }

   }
   MenuScreenListAdapter var3;
   RecyclerView var2;
   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.menu_screen);
      ServerUtilities.registerWithGCM(this);
      ExceptionHandler.register(
              this,  //contexto de la Activity
              "Error Mi android 2", //asunto del mensaje
              "david.plasencia@nauta.cu");
      ButterKnife.inject(this);
      toolbar.setNavigationIcon(R.mipmap.ic_launcher);
      setSupportActionBar(toolbar);
      this.loadAdvertisement();
//      this.changeTitleBarAppearance();
      var3 = new MenuScreenListAdapter(this, this.prepareMenuScreenList());
      var2 = (RecyclerView) this.findViewById(R.id.listViewMenu);
      var2.setLayoutManager(new LinearLayoutManager(MenuActivity.this));
      var2.setAdapter(var3);
     // var2.setOnItemClickListener(new MenuActivity.OnItemClick((MenuActivity.OnItemClick) null));
      String version_name="Android ";
      switch (Build.VERSION.SDK_INT){
         case 11:
            version_name+="HoneyComb ";
            break;
         case 12:
            version_name+="HoneyComb ";
            break;
         case 13:
            version_name+="HoneyComb ";
            break;
         case 14:
            version_name+="IceCreamSandwish ";
            break;
         case 15:
            version_name+="IceCreamSandwish ";
            break;
         case 16:
            version_name+="JellyBeans ";
            break;
         case 17:
            version_name+="JellyBeans ";
            break;
         case 18:
            version_name+="JellyBeans ";
            break;
         case 19:
            version_name+="KitKat ";
            break;
         case 20:
            version_name+="KitKat ";
            break;
         case 21:
            version_name+="Lollipop ";
            break;
         case 22:
            version_name+="Lollipop ";
            break;
         case 23:
            version_name+="MarshMallow ";
            break;

      }
      android_version.setText(version_name+ Build.VERSION.RELEASE);
      if(AppUtil.getApplicationLocale().toString().split("_")[0].equals(Locale.ENGLISH.toString())) {
         ram.setText(this.getRAMSize() + " " + this.ramSizeUnit);
      } else {
         ram.setText("RAM: " + this.getRAMSize() + this.ramSizeUnit);
         //ram.setText(this.ramSizeUnit);
      }
      model.setText("MODEL: "+Build.MANUFACTURER+" "+Build.MODEL);
      this.adView = (AdView)this.findViewById(R.id.adView);
      AdRequest var13 = (new AdRequest.Builder()).build();
      this.adView.loadAd(var13);
      this.adView.setAdListener(new AdListener() {
         @Override
         public void onAdLoaded() {
            super.onAdLoaded();
            adView.setVisibility(View.VISIBLE);
         }
      });
   }

   private String getExactSize(float var1) {
      String var2;
      if(var1 < 1024.0F) {
         var2 = AppUtil.toLocaleBasedNumberConversion((int) var1);
         this.ramSizeUnit = this.getString(R.string.mb);
      } else {
         var2 = AppUtil.toLocaleBasedDecimalFormat(var1 / 1024.0F);
         this.ramSizeUnit = this.getString(R.string.gb);
      }

      return var2;
   }

   private String getRAMSize() {
      // $FF: Couldn't be decompiled
      try {
         RandomAccessFile randomAccessFile= new RandomAccessFile("/proc/meminfo", "r");
         String lineTotalRam= randomAccessFile.readLine();
         String temp=lineTotalRam.replaceAll("[\\D]", "");
         randomAccessFile.close();
         return getExactSize(Integer.parseInt(temp)/1024);
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
      return "";
   }

   public void onDestroy() {
      if(this.adView != null) {
         this.adView.destroy();
      }

      super.onDestroy();
   }

   public void onPause() {
      if(this.adView != null) {
         this.adView.pause();
      }

      super.onPause();
   }

   @Override
   public boolean onCreateOptionsMenu(Menu var1) {
      this.getMenuInflater().inflate(R.menu.menu_home_screen, var1);
      return true;

   }

   public void onResume() {
      super.onResume();
      var3.notifyDataSetChanged();
   }

   @Override
   public void onBackPressed() {
      super.onBackPressed();
      MyAndroidApplication.showInterstitial();
   }

   protected void onSaveInstanceState(Bundle var1) {
      var1.putBoolean("KEY_IS_ANIMATION_DONE", true);
      super.onSaveInstanceState(var1);
   }

   private class OnItemClick implements OnItemClickListener {
      private OnItemClick() {
      }

      // $FF: synthetic method
      OnItemClick(MenuActivity.OnItemClick var2) {
         this();
      }

      public void onItemClick(AdapterView var1, View var2, int var3, long var4) {
         DeviceSpecsItem var6 = (DeviceSpecsItem)var2.getTag();
//         switch (var6.getItemType()){
//            case 7:
//               startActivity(new Intent(MenuActivity.this,ScreenActivity.class));
//               break;
//            case 2:
//               startActivity(new Intent(MenuActivity.this,MemoryActivity.class));
//               break;
//            case 5:
//               startActivity(new Intent(MenuActivity.this,CameraActivity.class));
//               break;
//            case 1:
//               startActivity(new Intent(MenuActivity.this, InstalledAppsFragment.class));
//               break;
//            case 4:
//               startActivity(new Intent(MenuActivity.this, CallDetailsFragment.class));
//               break;
//         }
         Intent var7 = new Intent(MenuActivity.this, HomeActivity.class);
         var7.putExtra("KEY_POSITION", var6.getItemType());
         MenuActivity.this.startActivity(var7);
//         Toast.makeText(MyAndroidApplication.getAppContext(), var6.getItemType(), Toast.LENGTH_SHORT).show();
      }
   }

   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      if(requestCode == 0 && resultCode == RESULT_OK)
         Toast.makeText(MyAndroidApplication.getAppContext(), MyAndroidApplication.getAppContext().getString(R.string.blu_on), Toast.LENGTH_SHORT).show();
      else
         Toast.makeText(MyAndroidApplication.getAppContext(),MyAndroidApplication.getAppContext().getString(R.string.blu_off),Toast.LENGTH_SHORT).show();
      var3.notifyDataSetChanged();
   }
}
