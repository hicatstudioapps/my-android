package com.info.myandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;

public class SplashActivity extends Activity {
   private Handler handler;
   private ImageView imageViewSplash;
   private Runnable launchActivityRunnable = new SplashActivity.ActivityRunnable((SplashActivity.ActivityRunnable)null);

   private void setSplashImageDimensions() {
      WindowManager var1 = (WindowManager)this.getSystemService(WINDOW_SERVICE);
      DisplayMetrics var2 = new DisplayMetrics();
      var1.getDefaultDisplay().getMetrics(var2);
      int var4 = var2.heightPixels;
      int var3 = var2.widthPixels;
      if(var3 < var4) {
         var3 = (int)((double)var3 * 0.7D);
      } else {
         var3 = (int)((double)var4 * 0.7D);
      }

      LayoutParams var5 = this.imageViewSplash.getLayoutParams();
      var5.width = var3;
      var5.height = var3;
      this.imageViewSplash.setLayoutParams(var5);
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.splash_screen);
//      this.imageViewSplash = (ImageView)this.findViewById(R.id.imageViewSplash);
//      this.setSplashImageDimensions();
      this.handler = new Handler();
   }

   protected void onPause() {
      super.onPause();
      this.handler.removeCallbacks(this.launchActivityRunnable);
   }

   protected void onResume() {
      super.onResume();
      this.handler.postDelayed(this.launchActivityRunnable, 3000L);
   }

   private class ActivityRunnable implements Runnable {
      private ActivityRunnable() {
      }

      // $FF: synthetic method
      ActivityRunnable(SplashActivity.ActivityRunnable var2) {
         this();
      }

      public void run() {
         Intent var1 = new Intent(SplashActivity.this, MenuActivity.class);
        // var1.putExtra("KEY_POSITION", 5);
        /// startActivity(var1);
         SplashActivity.this.startActivity(var1);
         MyAndroidApplication.showInterstitial();
         SplashActivity.this.finish();
      }
   }
}
