package com.info.myandroid;

import android.*;
import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.info.myandroid.utils.CameraDirectoryNotAvailableException;
import com.info.myandroid.utils.CameraTaskManager;
import com.info.myandroid.utils.DeviceCameras;
import com.info.myandroid.utils.PhotoRemaining;
import com.info.myandroid.utils.PhotoRemainingContainer;
import com.lacostra.utils.notification.Permission.Permission;
import com.lacostra.utils.notification.Permission.PermissionActivity;

import java.util.ArrayList;

import badabing.lib.apprater.AppRater;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class CameraActivity extends PermissionActivity {

    private DeviceCameras deviceCameras;
    @InjectView(R.id.b_pixel)
    TextView bMP;
    private AdView adView;
    @InjectView(R.id.b_picture)
    TextView p_picture;
    @InjectView(R.id.f_picture)
    TextView f_picture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        AdRequest var13;
        try {
            this.adView = (AdView)this.findViewById(R.id.adView);
            var13 = (new AdRequest.Builder()).build();
            this.adView.loadAd(var13);
            this.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    adView.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception var8) {
            ;
        }
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.storage_color));
        toolbar.setTitle(R.string.camera_info);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu var1) {
        this.getMenuInflater().inflate(R.menu.menu_home_screen, var1);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem var1) {
        switch (var1.getItemId()) {
            case android.R.id.home:
                finish();
                break;

//      case 16908332:
//         this.toggleDrawer();
//         break;
//      case 2131493078:
//         this.toggleDrawer();
//         break;
            case R.id.menu_item_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                String text = String.format(getString(R.string.share_text), getString(R.string.app_name), "https://play.google.com/store/apps/details?id="+ getPackageName());
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.menu_item_rate:
                AppRater.rateNow(this);
                break;
        }

        return super.onOptionsItemSelected(var1);
    }

    public static void shareMessage() {
        // $FF: Couldn't be decompiled
    }
    public void rateApplication() {
        try {
            this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + this.getPackageName())));
        } catch (Exception var2) {
            Log.e("MyAndroid", "Error occured while launching play store for rating app", var2);
        }
    }
    private class AsyncTaskForStoringDeviceCamerasInfo extends AsyncTask<Void,Void,DeviceCameras> {
        private AsyncTaskForStoringDeviceCamerasInfo() {
        }

        @Override
        protected DeviceCameras doInBackground(Void[] params) {
            return CameraTaskManager.getDeviceCamerasInfo();
        }


        protected void onPostExecute(DeviceCameras var1) {
            deviceCameras = var1;
            onDeviceCamerasInformationAvailable();
            new CalculateRemainingPicturesAsyncTask().execute();
        }

        protected void onPreExecute() {
            super.onPreExecute();
//            RelativeLayout var1 = (RelativeLayout)CameraActivity.this.rootView.findViewById(R.id.relLayoutCameraResFront);
//            CameraActivity.this.showProgressBarCameraResolutionSection(var1);
//            var1 = (RelativeLayout)CameraActivity.this.rootView.findViewById(R.id.relativeLayoutAppListItemContainer);
//         CameraActivity.this.showProgressBarCameraResolutionSection(var1);
        }
    }

    private void onDeviceCamerasInformationAvailable() {
        if(deviceCameras.getBackCameraStatus()==1){
            bMP.setText((int)deviceCameras.getBackCameraDetail().getMaxCameraResolutionInMegaPixels()+"");
            p_picture.setText((int)deviceCameras.getBackCameraDetail().getMaxCameraResolution().getResolutionWidth()+"x"+(int)deviceCameras.getBackCameraDetail().getSecondMaxCameraResolution().getResolutionHeight());
        }
        if(deviceCameras.getFrontCameraStatus()==1){
            bMP.setText((int)deviceCameras.getFrontCameraDetail().getMaxCameraResolutionInMegaPixels()+"");
            f_picture.setText((int)deviceCameras.getFrontCameraDetail().getMaxCameraResolution().getResolutionWidth()+"x"+(int)deviceCameras.getFrontCameraDetail().getSecondMaxCameraResolution().getResolutionHeight());
        }
    }

    private class CalculateRemainingPicturesAsyncTask extends AsyncTask<Void,Void,PhotoRemainingContainer> {
        private CalculateRemainingPicturesAsyncTask() {
        }


        @Override
        protected PhotoRemainingContainer doInBackground(Void... params) {

            PhotoRemainingContainer var2;
            try {
                var2 = CameraTaskManager.getRemainingPhotos(CameraActivity.this.deviceCameras);
            } catch (CameraDirectoryNotAvailableException var3) {
                return null;
            }

            return var2;
        }


        private void onPhotosRemainingObtained(PhotoRemainingContainer var1) {
            if(var1 != null) {
                PhotoRemaining var3 = var1.getBackCameraPhotoRemaining();
                Toast.makeText(CameraActivity.this, var3.getCountPhotoRemaining()+"", Toast.LENGTH_SHORT).show();
//                float var2;
//                if(var3 != null) {
//                    CameraActivity.this.remainingPicsBackCamera = var3.getCountPhotoRemaining();
//                    if(var3.isOverrideDeviceCameraResolution()) {
//                        var2 = var3.getOverridenCameraPixels();
//                        CameraActivity.this.overrideCameraResolution(var2, false);
//                    }
//                }

                PhotoRemaining var4 = var1.getFrontCameraPhotoRemaining();
                Toast.makeText(CameraActivity.this, var4.getCountPhotoRemaining()+"", Toast.LENGTH_SHORT).show();
            }

//            AppUIUtil.hideProgressBar(CameraActivity.this.progressBarRemPicsCameraBack);
//            if(CameraActivity.this.backStackAnimationDone) {
//                CameraActivity.this.prepareCameraCapacityView(false);
//            }
//
//            AppUIUtil.hideProgressBar(CameraActivity.this.progressBarRemPicsCameraFront);
//            if(CameraActivity.this.frontStackAnimationDone) {
//                CameraActivity.this.prepareCameraCapacityView(true);
//            }

        }



        protected void onPostExecute(PhotoRemainingContainer var1) {
            this.onPhotosRemainingObtained(var1);
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }
    }
}
