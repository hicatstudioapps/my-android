package com.info.myandroid;

import android.*;
import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.info.myandroid.fragment.APILevelFragment;
import com.info.myandroid.fragment.BatteryFragment;
import com.info.myandroid.fragment.CallDetailsFragment;
import com.info.myandroid.fragment.CameraInfoFragment;
import com.info.myandroid.fragment.InstalledAppsFragment;
import com.info.myandroid.fragment.InternetStatusFragment;
import com.info.myandroid.fragment.ManufacturerFragment;
import com.info.myandroid.fragment.MemoryFragment;
import com.info.myandroid.fragment.RAMSizeFragment;
import com.info.myandroid.fragment.ScreenDensityFragment;
import com.info.myandroid.fragment.ScreenSizeFragment;
import com.info.myandroid.fragment.ScreenshotAllOptionsFragment;
import com.info.myandroid.fragment.ScreenshotMyOptionFragment;
import com.info.myandroid.utils.AppUtil;
import com.info.myandroid.utils.MemoryActivity;
import com.info.myandroid.utils.PersistenceManager;
import com.lacostra.utils.notification.Permission.Permission;
import com.lacostra.utils.notification.Permission.PermissionActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;

import badabing.lib.apprater.AppRater;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_CALL_LOG;
import static android.Manifest.permission.READ_CONTACTS;

public class HomeActivity extends PermissionActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private AdView adView;
    private final int CAMERA_R=1;
    private int CALL=2;
    private int CONTACT=3;

    private int getDeviceWidth() {
        DisplayMetrics var1 = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(var1);
        return var1.widthPixels;
    }

    private void launchFragment(Fragment var1) {
        FragmentTransaction var2 = this.getSupportFragmentManager().beginTransaction();
        var2.replace(R.id.container, var1);
        var2.commit();
    }

    private DeviceSpecsItem prepareMenuObject(String var1, int var2, int var3) {
        DeviceSpecsItem var4 = new DeviceSpecsItem();
        var4.setDisplayText(var1);
        var4.setDrawableId(var2);
        var4.setItemType(var3);
        return var4;
    }

    public void rateApplication() {
        try {
            this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + this.getPackageName())));
        } catch (Exception var2) {
            Log.e("MyAndroid", "Error occured while launching play store for rating app", var2);
        }
    }


    public static void shareMessage() {
        // $FF: Couldn't be decompiled
    }

    private void showCallDetailsFragment() {
//        Toast.makeText(this, "Call", Toast.LENGTH_SHORT).show();
         this.launchFragment(CallDetailsFragment.getInstance());
    }

    private void showCameraInfoFragment() {
//        Toast.makeText(this, "Camera", Toast.LENGTH_SHORT).show();
            CameraInfoFragment camera=CameraInfoFragment.getInstance();
           this.launchFragment(camera);
    }

    private void showDensityFragment() {
//        Toast.makeText(this, "Density", Toast.LENGTH_SHORT).show();
        this.launchFragment(ScreenDensityFragment.getInstance());
    }


    private void showInstalledAppsFragment() {
//        Toast.makeText(this, "apps", Toast.LENGTH_SHORT).show();
        this.launchFragment(InstalledAppsFragment.getInstance());
    }

    private void showInternetStatusFragment() {
//        Toast.makeText(this, "Internet", Toast.LENGTH_SHORT).show();
         this.launchFragment(InternetStatusFragment.getInstance());
    }

    private void showManufacturerFragment() {
//        Toast.makeText(this, "Fabricante", Toast.LENGTH_SHORT).show();
        this.launchFragment(ManufacturerFragment.getInstance());
    }

    private void showMemoryFragment() {
//        Toast.makeText(this, "Memoria", Toast.LENGTH_SHORT).show();
         this.launchFragment(new MemoryActivity());
    }

    private void showRAMSizeFragment() {

//        Toast.makeText(this, "ram", Toast.LENGTH_SHORT).show();
        this.launchFragment(RAMSizeFragment.getInstance());
    }

    private void showScreenSizeFragment() {
//        Toast.makeText(this, "ScreenSize", Toast.LENGTH_SHORT).show();
          this.launchFragment(ScreenSizeFragment.getInstance());
    }

     private void showScreenshotFragment() {
//        Toast.makeText(this, "Screen_shoot", Toast.LENGTH_SHORT).show();
        Object var1;
      if(AppUtil.containsData(PersistenceManager.getDeviceScreenShotInfo())) {
         var1 = ScreenshotMyOptionFragment.getInstance();
      } else {
         var1 = ScreenshotAllOptionsFragment.getInstance();
      }
        this.launchFragment((Fragment)var1);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    private Toolbar toolbar;

    protected void onCreate(Bundle var1) {
        super.onCreate(var1);
        this.setContentView(R.layout.home_screen);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.drawerLayout = (DrawerLayout) this.findViewById(R.id.drawer_layout);
        android.support.v7.app.ActionBarDrawerToggle toggle = new android.support.v7.app.ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.setDrawerListener(toggle);
        if(Build.VERSION.SDK_INT > 14)
        drawerLayout.setFitsSystemWindows(false);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        Bundle var4 = this.getIntent().getExtras();
        int option= (int) var4.get("KEY_POSITION");
        if (var4 == null) {
            this.showAPILevelFragment();
        }else{
            switch (option){
                case 1:
                    showInstalledAppsFragment();
                    break;
                case 2:
                    showMemoryFragment();
                    break;
                case 4:
                    showCallDetailsFragment();
                    break;
                case 5:

                    showCameraInfoFragment();

                    break;
                case 7:
                    showScreenshotFragment();
                    break;
                case 8:
                    showInternetStatusFragment();
                    break;
                case 9:
                    showScreenSizeFragment();
                    break;
                case 10:
                    showDensityFragment();
                    break;
                case 11:
                    showRAMSizeFragment();
                    break;
                case 12:
                    showAPILevelFragment();
                    break;
                case 13:
                    showManufacturerFragment();
                    break;
                case 14:
                    this.launchFragment(BatteryFragment.newInstance());
                    break;
            }
        }

        Log.i("MyAndroid", "HomeActivity.onCreate() - Ended");
        AppUtil.updateConfigurtionWithLocale();
        AdRequest var13;

        this.adView = (AdView)this.findViewById(R.id.adView);
        var13 = (new AdRequest.Builder()).build();
        this.adView.loadAd(var13);
        this.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                adView.setVisibility(View.VISIBLE);
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu var1) {
            this.getMenuInflater().inflate(R.menu.menu_home_screen, var1);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem var1) {
        switch (var1.getItemId()) {
      case R.id.menu_item_share:
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            String text = String.format(getString(R.string.share_text), getString(R.string.app_name), "https://play.google.com/store/apps/details?id="+ getPackageName());
            sendIntent.putExtra(Intent.EXTRA_TEXT, text);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
            break;
            case R.id.menu_item_rate:
                AppRater.rateNow(HomeActivity.this);
                break;
        }

        return super.onOptionsItemSelected(var1);
    }


    public void showAPILevelFragment() {
//        Toast.makeText(this, "Api", Toast.LENGTH_SHORT).show();
      this.launchFragment(APILevelFragment.getInstance());
    }

    public void showDrawer(View var1) {
        // this.drawerLayout.openDrawer(this.drawerList);
    }
    public void requestCamera(){
        ArrayList<String> perm= new ArrayList<>();
        perm.add(CAMERA);
        askPermision(perm, 5);
    }

    public void requestCall(){
        ArrayList<String> perm= new ArrayList<>();
        perm.add(READ_CONTACTS);
        perm.add(READ_CALL_LOG);
        askPermision(perm, 4);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 4:
                if(!Permission.hasPermission(this, READ_CONTACTS) && !Permission.hasPermission(this,READ_CALL_LOG)){
                  // showCallDetailsFragment();}

                    // showAPILevelFragment();
                    Toast.makeText(this,"You must accept the requested permission in order to access these feature!",Toast.LENGTH_LONG).show();
                }
                break;
            case 5:
                if(!Permission.hasPermission(this, CAMERA)){

                    // showAPILevelFragment();
                    Toast.makeText(this,"You must accept the requested permission in order to access these feature!",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.search:
                showInstalledAppsFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.memory:
                showMemoryFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.screenShot:
                showScreenshotFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.call:
                if(Permission.hasPermission(this, READ_CONTACTS) && Permission.hasPermission(this,READ_CALL_LOG)){
                    showCallDetailsFragment();}
                else
                {
                    requestCall();
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return false;
                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.camera:
                if(Permission.hasPermission(this,CAMERA))
                showCameraInfoFragment();
                else{
                    requestCamera();
                    drawerLayout.closeDrawer(GravityCompat.START);
                    return false;}
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.internet:
                showInternetStatusFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.screen_size:
                showScreenSizeFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.screen_density:
                showDensityFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.ram:
                showRAMSizeFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.os_version:
                showAPILevelFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.manofacturer:
                showManufacturerFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.bateria:
                this.launchFragment(BatteryFragment.newInstance());
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
        }
        return true;
    }

}
