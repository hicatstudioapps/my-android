package com.info.myandroid.utils;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.os.StatFs;
import android.preference.PreferenceManager;

import com.info.myandroid.MyAndroidApplication;

public class PersistenceManager {

   public static long getAvailableMemorySize(String var0) {
      long var2;
      if(var0 == null) {
         var2 = 0L;
      } else {
         int var1;
         try {
            StatFs var5 = new StatFs(var0);
            var2 = (long)var5.getBlockSize();
            var1 = var5.getAvailableBlocks();
         } catch (Exception var4) {
            var2 = -1L;
            return var2;
         }

         var2 = (long)var1 * var2;
      }

      return var2;
   }

   public static long getAverageSizeCameraPhoto(String var0, boolean var1) {
      if(var1) {
         var0 = "KEY_PHOTO_CATEGORY_FRONT_CAM_AVG_FILE_SIZE" + var0;
      } else {
         var0 = "KEY_PHOTO_CATEGORY_BACK_CAM_AVG_FILE_SIZE" + var0;
      }

      return PreferenceManager.getDefaultSharedPreferences(MyAndroidApplication.getAppContext()).getLong(var0, 0L);
   }

   private static CameraDetail getDeviceCameraDetailFromCache(boolean param0, SharedPreferences param1) {
      // $FF: Couldn't be decompiled
      return null;
   }

   public static DeviceCameras getDeviceCamerasInfoFromPersistentCache() {
      // $FF: Couldn't be decompiled
      return null;
   }

   public static String getDeviceScreenShotInfo() {
      String var0 = null;

      String var1;
      try {
         var1 = PreferenceManager.getDefaultSharedPreferences(MyAndroidApplication.getAppContext()).getString(getScreenshotOptionKey(), (String)null);
      } catch (Exception var2) {
         return var0;
      }

      var0 = var1;
      return var0;
   }

   private static String getScreenshotOptionKey() {
      return "KEY_DEVICE_SCREEN_SHOT_OPTION_" + VERSION.SDK_INT;
   }

   private static String getScreenshotOptionVerifiedKey() {
      return "KEY_IS_DEVICE_SCREEN_SHOT_VERIFIED_" + VERSION.SDK_INT;
   }

   public static long getTotalMemorySize(String var0) {
      long var2;
      if(var0 == null) {
         var2 = 0L;
      } else {
         int var1;
         try {
            StatFs var5 = new StatFs(var0);
            var2 = (long)var5.getBlockSize();
            var1 = var5.getBlockCount();
         } catch (Exception var4) {
            var2 = -1L;
            return var2;
         }

         var2 = (long)var1 * var2;
      }

      return var2;
   }

   private static CameraDetail initializeCameraDetail(boolean var0, CameraDetail var1) {
      String var2;
      if(var0) {
         var2 = "CAMERA_POSITION_FRONT";
      } else {
         var2 = "CAMERA_POSITION_BACK";
      }

      var1.setCameraPosition(var2);
      return var1;
   }

   public static boolean isCameraInfoAvailableFromPersistentCache() {
      boolean var4 = false;

      int var1;
      int var2;
      boolean var3;
      try {
         SharedPreferences var0 = PreferenceManager.getDefaultSharedPreferences(MyAndroidApplication.getAppContext());
         var2 = var0.getInt("KEY_FRONT_CAMERA_STATUS", -1);
         var1 = var0.getInt("KEY_BACK_CAMERA_STATUS", -1);
      } catch (Exception var5) {
         var3 = var4;
         return var3;
      }

      if(var2 != 1) {
         var3 = var4;
         if(var2 != 2) {
            return var3;
         }
      }

      if(var1 != 1) {
         var3 = var4;
         if(var1 != 2) {
            return var3;
         }
      }

      var3 = true;
      return var3;
   }

   public static boolean isScreenshotOptionVerified() {
      return PreferenceManager.getDefaultSharedPreferences(MyAndroidApplication.getAppContext()).getBoolean(getScreenshotOptionVerifiedKey(), false);
   }

   public static void persistAverageSizeCameraPhoto(String param0, boolean param1, long param2) {
      // $FF: Couldn't be decompiled
   }

   public static void persistDeviceCameraInfo(DeviceCameras param0) {
      // $FF: Couldn't be decompiled
   }

   private static void persistDeviceCameraInfo(boolean param0, CameraDetail param1, Editor param2) {
      // $FF: Couldn't be decompiled
   }

   public static void persistDeviceScreenshotOption(String var0) {
      try {
         if(AppUtil.containsData(var0)) {
            Editor var1 = PreferenceManager.getDefaultSharedPreferences(MyAndroidApplication.getAppContext()).edit();
            var1.putString(getScreenshotOptionKey(), var0);
            var1.commit();
         }
      } catch (Exception var2) {
         ;
      }

   }

   public static void persistDeviceScreenshotOption(String var0, boolean var1) {
      try {
         if(AppUtil.containsData(var0)) {
            Editor var2 = PreferenceManager.getDefaultSharedPreferences(MyAndroidApplication.getAppContext()).edit();
            var2.putString(getScreenshotOptionKey(), var0);
            var2.putBoolean(getScreenshotOptionVerifiedKey(), var1);
            var2.commit();
         }
      } catch (Exception var3) {
         ;
      }

   }

//   private static ArrayList prepareCameraResolutions(boolean param0, SharedPreferences param1) {
//      // $FF: Couldn't be decompiled
//   }
}
