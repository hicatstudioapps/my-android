package com.info.myandroid.utils;


import java.io.Serializable;

public class Screenshot extends BaseModel implements Serializable {
   private static final long serialVersionUID = 1L;
   private String screenshotOptionCode = "";

   public String getScreenshotOptionCode() {
      return this.screenshotOptionCode;
   }

   public void resetScreenshotOption() {
      this.screenshotOptionCode = "";
   }

   public void setScreenshotOptionCode(String var1) {
      this.screenshotOptionCode = var1;
   }
}
