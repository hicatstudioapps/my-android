package com.info.myandroid.utils;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.info.myandroid.MyAndroidApplication;

import java.util.ArrayList;

public class InstalledApplicationManager {

   public static ArrayList getInstalledApplications() {
      PackageManager var1 = MyAndroidApplication.getAppContext().getPackageManager();
      Intent var2 = new Intent("android.intent.action.MAIN", (Uri)null);
      var2.addCategory("android.intent.category.LAUNCHER");
      ArrayList var0 = null;

      ArrayList var4;
      try {
         var4 = (ArrayList)var1.queryIntentActivities(var2, 0);
      } catch (Exception var3) {
         return var0;
      }

      var0 = var4;
      return var0;
   }
}
