package com.info.myandroid.utils;


import java.io.Serializable;

public class DeviceInfo extends BaseModel implements Serializable {
   private static final long serialVersionUID = 1L;
   int androidApiVersion = 0;
   private String deviceManufacturerName = null;
   private String deviceModel = null;

   public int getAndroidApiVersion() {
      return this.androidApiVersion;
   }

   public String getDeviceManufacturerName() {
      String var1;
      if(this.deviceManufacturerName != null) {
         var1 = this.deviceManufacturerName;
      } else {
         var1 = "";
      }

      this.deviceManufacturerName = var1;
      return this.deviceManufacturerName.trim();
   }

   public String getDeviceModel() {
      String var1;
      if(this.deviceModel != null) {
         var1 = this.deviceModel;
      } else {
         var1 = "";
      }

      this.deviceModel = var1;
      return this.deviceModel.trim();
   }

   public void setAndroidApiVersion(int var1) {
      this.androidApiVersion = var1;
   }

   public void setDeviceManufacturerName(String var1) {
      this.deviceManufacturerName = var1;
   }

   public void setDeviceModel(String var1) {
      this.deviceModel = var1;
   }
}
