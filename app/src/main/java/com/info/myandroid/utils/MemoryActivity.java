package com.info.myandroid.utils;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.info.myandroid.HomeActivity;
import com.info.myandroid.R;
import com.info.myandroid.fragment.MasterFragment;

import java.io.File;

import badabing.lib.apprater.AppRater;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class MemoryActivity extends MasterFragment {
    @InjectView(R.id.totoal_internal)
    TextView totalInternal;
    @InjectView(R.id.used_internal)
    TextView usedIternalPercent;
    @InjectView(R.id.avalible_internal)
    TextView avalibleInnternalPrecent;
    @InjectView(R.id.textView10)
    TextView used;
    @InjectView(R.id.textView11)
    TextView avalible;
    @InjectView(R.id.progressBar3)
    ProgressBar internalProgres;
    int internalTotal=0;
    int internalProgress=0;

    @InjectView(R.id.totoal_external)
    TextView totalExternal;
    @InjectView(R.id.used_external)
    TextView usedExternalPercent;
    @InjectView(R.id.avalible_external)
    TextView avalibleExternalPrecent;
    @InjectView(R.id.textView13)
    TextView usedE;
    @InjectView(R.id.textView14)
    TextView avalibleE;
    @InjectView(R.id.progressBar4)
    ProgressBar externalProgres;
    @InjectView(R.id.extView)
    View exteView;
    private HomeActivity activity;
    private int externalProgress;
    private int externalTotal;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_memory,null);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle var1) {
        super.onActivityCreated(var1);
        activity = (HomeActivity)getActivity();
        activity.setTitle(R.string.memory);
//        activity.getToolbar().setBackgroundColor(getResources().getColor(R.color.storage_color));
        calculateExternalMemory();
        if(isExternalAvalible()){
            calculateSDCard();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new InternalFill().execute();
                if(isExternalAvalible())
                new ExternalFill().execute();
            }
        }, 1500L);
    }




    private void calculateSDCard() {
        String path= System.getenv("SECONDARY_STORAGE");
        long deviceTotalExternalMemory = PersistenceManager.getTotalMemorySize(((String) (path)));
        long deviceAvialableExternalMemory = PersistenceManager.getAvailableMemorySize(((String) (path)));
        long totalExternalUsed = deviceTotalExternalMemory - deviceAvialableExternalMemory;
        int total= (int) (deviceTotalExternalMemory/1024/1024/1024);
        int avalible= (int) (deviceAvialableExternalMemory/1024/1024/1024);
        int used= (int) (totalExternalUsed/1024/1024/1024);
        externalProgres.setMax(total);
        externalProgres.setProgress(0);
        internalProgress=used;
        internalTotal=total;
        //externalProgres.setProgress(used);
        totalExternal.setText(total+ " GB");
        usedExternalPercent.setText(used*100/total+" %");
        avalibleExternalPrecent.setText(avalible*100/total+" %");
        usedE.setText(used+" GB");
        avalibleE.setText(avalible+" GB");
        exteView.setVisibility(View.VISIBLE);
    }

    private void calculateExternalMemory()
    {
        Object obj = Environment.getExternalStorageDirectory();
        if (obj != null)
        {
            obj = ((File) (obj)).getPath();
            long deviceTotalExternalMemory = PersistenceManager.getTotalMemorySize(((String) (obj)));
            long deviceAvialableExternalMemory = PersistenceManager.getAvailableMemorySize(((String) (obj)));
            long totalExternalUsed = deviceTotalExternalMemory - deviceAvialableExternalMemory;
            totalInternal.setText(deviceTotalExternalMemory/1024/1024/1024+"GB");
            int usedP= (int) (totalExternalUsed*100/deviceTotalExternalMemory);
            int avalP=(int)100-usedP;
            int usedT= (int) (totalExternalUsed/1024/1024/1024);
            int avalT= (int) (deviceAvialableExternalMemory/1024/1024/1024);
            usedIternalPercent.setText(usedP + " %");
            avalibleInnternalPrecent.setText(avalP + " %");
            used.setText(usedT + "GB");
            avalible.setText(avalT + "GB");
            internalProgres.setMax((int)(deviceTotalExternalMemory/1024/1024/1024));
            internalProgres.setProgress(0);
            //internalProgres.setProgress(usedT);
            externalProgress=usedT;
            externalTotal=(int)deviceTotalExternalMemory/1024/1024/1024;
//            Log.d("internal total",deviceTotalExternalMemory/1024/1024+"");
//            Log.d("internal avalible",deviceAvialableExternalMemory/1024/1024+"");
//            Log.d("internal used",totalExternalUsed/1024/1024+"");
//            Log.d("sec storage ", System.getenv("SECONDARY_STORAGE"));
        }
    }
////    @Override
////    public boolean onCreateOptionsMenu(Menu var1) {
////        this.getMenuInflater().inflate(R.menu.menu_home_screen, var1);
////        return true;
////
////    }
//    public boolean onOptionsItemSelected(MenuItem var1) {
//        switch (var1.getItemId()) {
//            case android.R.id.home:
//                finish();
//                break;
//
////      case 16908332:
////         this.toggleDrawer();
////         break;
////      case 2131493078:
////         this.toggleDrawer();
////         break;
//            case R.id.menu_item_share:
//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                String text = String.format(getString(R.string.share_text), getString(R.string.app_name), "https://play.google.com/store/apps/details?id="+ getPackageName());
//                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
//                sendIntent.setType("text/plain");
//                startActivity(sendIntent);
//                break;
//            case R.id.menu_item_rate:
//                AppRater.rateNow(this);
//                break;
//        }
//       return super.onOptionsItemSelected(var1);
//    }
//    public void rateApplication() {
//        try {
//            this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + this.getPackageName())));
//        } catch (Exception var2) {
//            Log.e("MyAndroid", "Error occured while launching play store for rating app", var2);
//        }
//    }

    public boolean isExternalAvalible(){
        try{
            String path= System.getenv("SECONDARY_STORAGE");
            File f= new File(path);
            if(f.exists() && f.canWrite())
                return true;
            else return false;
        }catch (Exception ex){
            return false;
        }

    }
    public static void shareMessage() {
        // $FF: Couldn't be decompiled
    }

    class InternalFill extends AsyncTask<Void,Integer,Void>{

        @Override
        protected Void doInBackground(Void... params) {
            for (int i=0;i<externalProgress;i++){
                final int finalP=i;
                publishProgress(i);
                try {
                    Thread.sleep(10L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            internalProgres.setProgress(values[0].intValue());
        }
    }
    class ExternalFill extends AsyncTask<Void,Integer,Void>{

        @Override
        protected Void doInBackground(Void... params) {
            for (int i=0;i<internalProgress;i++){
                final int finalP=i;
                publishProgress(i);
                try {
                    Thread.sleep(10L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            externalProgres.setProgress(values[0].intValue());
        }
    }
}
