package com.info.myandroid.utils;

public class DeviceCameras {

   private CameraDetail backCameraDetail = null;
   private int backCameraStatus = 2;
   private CameraDetail frontCameraDetail = null;
   private int frontCameraStatus = 2;

   public CameraDetail getBackCameraDetail() {
      return this.backCameraDetail;
   }

   public int getBackCameraStatus() {
      return this.backCameraStatus;
   }

   public CameraDetail getFrontCameraDetail() {
      return this.frontCameraDetail;
   }

   public int getFrontCameraStatus() {
      return this.frontCameraStatus;
   }

   public boolean isCameraInfoAvailable() {
      boolean var2 = false;
      boolean var1 = var2;
      if(this.frontCameraDetail != null) {
         var1 = var2;
         if(this.frontCameraDetail.isCameraDetailAvailable()) {
            var1 = var2;
            if(this.backCameraDetail != null) {
               var1 = var2;
               if(this.backCameraDetail.isCameraDetailAvailable()) {
                  var1 = true;
               }
            }
         }
      }

      return var1;
   }

   public void setBackCameraDetail(CameraDetail var1) {
      this.backCameraDetail = var1;
   }

   public void setBackCameraStatus(int var1) {
      this.backCameraStatus = var1;
   }

   public void setFrontCameraDetail(CameraDetail var1) {
      this.frontCameraDetail = var1;
   }

   public void setFrontCameraStatus(int var1) {
      this.frontCameraStatus = var1;
   }

   public String toString() {
      return "########## DeviceCameras Begin ########## \n" + this.frontCameraDetail + "\n" + this.backCameraDetail + "\n" + "########## DeviceCameras End ########## \n";
   }
}
