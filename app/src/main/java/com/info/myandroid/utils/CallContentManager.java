package com.info.myandroid.utils;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;

import com.info.myandroid.MyAndroidApplication;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CallContentManager {

    public static Bitmap getContactPhotoImage(String param0) {
        Bitmap bm = null;
        Cursor cursor = ((ContentResolver) (MyAndroidApplication.getAppContext().getContentResolver())).query(Uri.withAppendedPath(android.provider.ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(param0)), new String[]{"_id"}, null, null, null);
        if (cursor.moveToFirst()) {
            long l = cursor.getLong(cursor.getColumnIndex("_id"));
            Uri contact = ContentUris.withAppendedId(android.provider.ContactsContract.Contacts.CONTENT_URI, l);
            // Uri photoUri = Uri.withAppendedPath(contact, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
//         Cursor cursor1 = MyAndroidApplication.getAppContext().getContentResolver().query(photoUri,
//                 new String[]{ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
//         if (cursor1.moveToFirst()) {
//            byte[] data = cursor.getBlob(0);
//            return data;
//         }

            if (!AppUtil.isCurrentVersionICSAndAbove()) {
                InputStream stream = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    stream = android.provider.ContactsContract.Contacts.openContactPhotoInputStream(((ContentResolver) (MyAndroidApplication.getAppContext().getContentResolver())), ((Uri) (contact)), true);
                    //stream.
                }
                //BufferedInputStream bufferedInputStream= new BufferedInputStream(stream);
                if (stream != null) {
                    bm = BitmapFactory.decodeStream(stream);
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    cursor.close();
                }
            } else {
                try {
                    InputStream stream = android.provider.ContactsContract.Contacts.openContactPhotoInputStream(((ContentResolver) (MyAndroidApplication.getAppContext().getContentResolver())), ((Uri) (contact)));
                    //BufferedInputStream bufferedInputStream= new BufferedInputStream(stream);
                    if (stream != null) {
                        bm = BitmapFactory.decodeStream(stream);
                        stream.close();
                        cursor.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bm;
    }
}
