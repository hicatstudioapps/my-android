package com.info.myandroid.utils;

public enum CallTimeWindow {

   LAST_WEEK(3),
   TODAY(1),
   YESTERDAY(2);

   private final int code;

   private CallTimeWindow(int var3) {
      this.code = var3;
   }

   public int getCode() {
      return this.code;
   }
}
