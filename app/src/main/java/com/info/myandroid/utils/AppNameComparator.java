package com.info.myandroid.utils;

import java.text.Collator;
import java.util.Comparator;

public class AppNameComparator implements Comparator<InstalledApplicationInfo> {
   private SortDirectionEnum sortDirectionEnum;
   private String textCurrentObject;
   private String textOtherObject;

   public AppNameComparator(SortDirectionEnum var1) {
      this.sortDirectionEnum = var1;
   }

   public int compare(InstalledApplicationInfo var1, InstalledApplicationInfo var2) {
      byte var4 = 0;
      if(this.sortDirectionEnum == SortDirectionEnum.ASCENDING) {
         this.textCurrentObject = var1.getApplicationName();
         this.textOtherObject = var2.getApplicationName();
      } else {
         this.textCurrentObject = var2.getApplicationName();
         this.textOtherObject = var1.getApplicationName();
      }

      int var3 = var4;
      if(this.textCurrentObject != null) {
         var3 = var4;
         if(this.textOtherObject != null) {
            var3 = Collator.getInstance(AppUtil.getApplicationLocale()).compare(this.textCurrentObject.trim(), this.textOtherObject.trim());
         }
      }

      return var3;
   }
}
