package com.info.myandroid.utils;


import java.io.Serializable;

public class DeviceScreenshot extends BaseModel implements Serializable {
   private static final long serialVersionUID = 1L;
   private DeviceInfo deviceInfo = null;
   private Screenshot screenshot = null;

   public DeviceInfo getDeviceInfo() {
      return this.deviceInfo;
   }

   public Screenshot getScreenshot() {
      return this.screenshot;
   }

   public void setDeviceInfo(DeviceInfo var1) {
      this.deviceInfo = var1;
   }

   public void setScreenshot(Screenshot var1) {
      this.screenshot = var1;
   }
}
