package com.info.myandroid.utils;

import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.HashMap;

import com.info.myandroid.MyAndroidApplication;

public class CameraContentManager {

   public static ArrayList getAggregatedPhotoInfo(String param0) {
      // $FF: Couldn't be decompiled
      ArrayList<PhotoCategoryInfo> result=new ArrayList<>();
      HashMap<PhotoCategoryInfo,PhotoCategoryInfo> hashMap= new HashMap<>();
      String obj2 = (new StringBuilder(String.valueOf("_data"))).append(" LIKE ? ").toString();
      String obj3 = (new StringBuilder(String.valueOf("datetaken"))).append(" DESC ").toString();
      String s = (new StringBuilder(String.valueOf(param0))).append("/%").toString();
      Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
      Cursor cursor = MyAndroidApplication.getAppContext().getContentResolver().query(uri, null, "_data LIKE ?", new String[]{"%DCIM%"}, null);
       if(cursor.moveToFirst()){
          while (!cursor.isLast()){
              String data=cursor.getString(cursor.getColumnIndex("_data"));
             // if(cursor.getString(cursor.getColumnIndex("_data")).contains(param0)){
                int size=cursor.getColumnIndex("_size");
                int dateTaken=cursor.getColumnIndex("datetaken");
                int with=cursor.getColumnIndex("width");
                int height=cursor.getColumnIndex("height");
                PhotoCategoryInfo info= new PhotoCategoryInfo(cursor.getInt(with),cursor.getInt(height));
                if(hashMap.get(info)!=null){
                    PhotoCategoryInfo temp=hashMap.get(info);
                    temp.increasePhotoCount();
                    temp.setLastPhotoTakenOn(dateTaken);
                    temp.increaseTotalSize(size);
                    hashMap.put(info,temp);
                }else{
                 hashMap.put(info, info);
                }//}
              cursor.moveToNext();
          }
           cursor.close();
           for (PhotoCategoryInfo temp : hashMap.keySet()) {
               result.add(temp);
           }
       }
      return result;
   }
}
