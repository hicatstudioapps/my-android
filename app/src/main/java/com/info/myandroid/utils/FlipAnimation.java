package com.info.myandroid.utils;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class FlipAnimation extends Animation {
   public static final float SCALE_DEFAULT = 0.75F;
   private Camera mCamera;
   private final float mCenterX;
   private final float mCenterY;
   private final float mFromDegrees;
   private final float mToDegrees;
   private float scale;
   private final ScaleUpDownEnum scaleType;

   public FlipAnimation(float var1, float var2, float var3, float var4, float var5, ScaleUpDownEnum var6) {
      label15: {
         this.mFromDegrees = var1;
         this.mToDegrees = var2;
         this.mCenterX = var3;
         this.mCenterY = var4;
         if(var5 > 0.0F) {
            var1 = var5;
            if(var5 < 1.0F) {
               break label15;
            }
         }

         var1 = 0.75F;
      }

      this.scale = var1;
      ScaleUpDownEnum var7 = var6;
      if(var6 == null) {
         var7 = ScaleUpDownEnum.SCALE_CYCLE;
      }

      this.scaleType = var7;
   }

   protected void applyTransformation(float var1, Transformation var2) {
      float var3 = this.mFromDegrees;
      float var6 = this.mToDegrees;
      float var4 = this.mCenterX;
      float var5 = this.mCenterY;
      Camera var7 = this.mCamera;
      Matrix var8 = var2.getMatrix();
      var7.save();
      var7.rotateY(var3 + (var6 - var3) * var1);
      var7.getMatrix(var8);
      var7.restore();
      var8.preTranslate(-var4, -var5);
      var8.postTranslate(var4, var5);
      var8.preScale(this.scaleType.code, this.scaleType.code, var4, var5);
   }

   public void initialize(int var1, int var2, int var3, int var4) {
      super.initialize(var1, var2, var3, var4);
      this.mCamera = new Camera();
   }

   public static enum ScaleUpDownEnum {
      // $FF: synthetic field
      SCALE_CYCLE(3) ,
      SCALE_DOWN(2),
      SCALE_NONE(4),
      SCALE_UP(1);

       private final int code;

       private ScaleUpDownEnum(int var3) {
           this.code = var3;
       }

       public int getScale() {
           return this.code;
       }

      // $FF: synthetic method
//      static int[] $SWITCH_TABLE$com$innovationm$myandroid$anim$FlipAnimation$ScaleUpDownEnum() {
//         int[] var0 = $SWITCH_TABLE$com$innovationm$myandroid$anim$FlipAnimation$ScaleUpDownEnum;
//         if(var0 == null) {
//            var0 = new int[values().length];
//
//            try {
//               var0[SCALE_CYCLE.ordinal()] = 3;
//            } catch (NoSuchFieldError var5) {
//               ;
//            }
//
//            try {
//               var0[SCALE_DOWN.ordinal()] = 2;
//            } catch (NoSuchFieldError var4) {
//               ;
//            }
//
//            try {
//               var0[SCALE_NONE.ordinal()] = 4;
//            } catch (NoSuchFieldError var3) {
//               ;
//            }
//
//            try {
//               var0[SCALE_UP.ordinal()] = 1;
//            } catch (NoSuchFieldError var2) {
//               ;
//            }
//
//            $SWITCH_TABLE$com$innovationm$myandroid$anim$FlipAnimation$ScaleUpDownEnum = var0;
//         }
//
//         return var0;
//      }

//      public float getScale(float var1, float var2) {
//         float var3 = 1.0F;
//         switch($SWITCH_TABLE$com$innovationm$myandroid$anim$FlipAnimation$ScaleUpDownEnum()[this.ordinal()]) {
//         case 1:
//            var1 += (1.0F - var1) * var2;
//            break;
//         case 2:
//            var1 = 1.0F - (1.0F - var1) * var2;
//            break;
//         case 3:
//            boolean var4;
//            if((double)var2 > 0.5D) {
//               var4 = true;
//            } else {
//               var4 = false;
//            }
//
//            if(var4) {
//               var1 += (1.0F - var1) * (var2 - 0.5F) * 2.0F;
//            } else {
//               var1 = 1.0F - (1.0F - var1) * var2 * 2.0F;
//            }
//            break;
//         default:
//            var1 = var3;
//         }
//
//         return var1;
//      }
   }
}
