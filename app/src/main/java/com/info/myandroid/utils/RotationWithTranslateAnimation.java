package com.info.myandroid.utils;

import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;

public class RotationWithTranslateAnimation {
   public static AnimationSet prepareAnimationSet(int var0) {
      AnimationSet var1 = new AnimationSet(true);
      var1.setFillAfter(true);
      RotateAnimation var2 = new RotateAnimation(360.0F, 0.0F, 1, 0.5F, 1, 0.5F);
      var2.setStartOffset((long)(var0 + 0));
      var2.setDuration(800L);
      var1.addAnimation(var2);
      TranslateAnimation var3 = new TranslateAnimation(1000.0F, 0.0F, 0.0F, 0.0F);
      var3.setStartOffset((long)(var0 + 0));
      var3.setDuration(800L);
      var1.addAnimation(var3);
      return var1;
   }
}
