package com.info.myandroid.utils;

public class DeviceScreenSize {
   private int screenHeightInPixels = 0;
   private int screenWidthInPixels = 0;

   public int getScreenHeightInPixels() {
      return this.screenHeightInPixels;
   }

   public int getScreenWidthInPixels() {
      return this.screenWidthInPixels;
   }

   public void setScreenHeightInPixels(int var1) {
      this.screenHeightInPixels = var1;
   }

   public void setScreenWidthInPixels(int var1) {
      this.screenWidthInPixels = var1;
   }
}
