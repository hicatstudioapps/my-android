package com.info.myandroid.utils;

import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Locale;

import com.info.myandroid.MyAndroidApplication;

@SuppressWarnings("ResourceType")
public class DeviceManager {

   public static boolean canDeviceMakePhoneCalls() {
      TelephonyManager var0 = (TelephonyManager) MyAndroidApplication.getAppContext().getSystemService("phone");
      boolean var1 = true;
      if(var0 != null) {
         if(var0.getPhoneType() == 0) {
            var1 = false;
         }
      } else {
         var1 = false;
      }

      return var1;
   }

   public static String getCameraPhotoLocation() throws CameraDirectoryNotAvailableException {
      File var1 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
      String var0 = null;
      if(var1 != null) {
         var0 = var1.getAbsolutePath();
      }

      if(var0 != null && !var0.trim().equals("")) {
         return var0;
      } else {
         throw new CameraDirectoryNotAvailableException();
      }
   }

   public static String getCurrentLanguageCodeOnDevice() {
      Locale var1 = MyAndroidApplication.getAppContext().getResources().getConfiguration().locale;
      String var0 = null;
      if(var1 != null) {
         String var2 = var1.getLanguage();
         var0 = var2;
         if(var2 == null) {
            var0 = "";
         }
      }

      return var0;
   }

   public static DeviceScreenSize getDeviceAvailableScreenSize() {
      DeviceScreenSize var2 = new DeviceScreenSize();
      WindowManager var0 = (WindowManager)MyAndroidApplication.getAppContext().getSystemService("window");
      DisplayMetrics var1 = new DisplayMetrics();
      var0.getDefaultDisplay().getMetrics(var1);
      int var3 = var1.heightPixels;
      int var4 = var1.widthPixels;
      var2.setScreenHeightInPixels(var3);
      var2.setScreenWidthInPixels(var4);
      return var2;
   }

   public static DeviceInfo getDeviceInfo() {
      new DeviceInfo();
      DeviceInfo var2 = new DeviceInfo();
      String var1 = Build.MODEL;
      String var0 = Build.MANUFACTURER;
      int var3 = VERSION.SDK_INT;
      var2.setDeviceManufacturerName(var0);
      var2.setDeviceModel(var1);
      var2.setAndroidApiVersion(var3);
      return var2;
   }

   public static DeviceScreenSize getDevicePhysicalScreenSize() {
      DeviceScreenSize var1 = new DeviceScreenSize();
      DisplayMetrics var0 = new DisplayMetrics();
      Display var2 = ((WindowManager)MyAndroidApplication.getAppContext().getSystemService("window")).getDefaultDisplay();
      int var4;
      int var5;
      if(VERSION.SDK_INT >= 17) {
         var2.getRealMetrics(var0);
         var5 = var0.widthPixels;
         var4 = var0.heightPixels;
      } else {
         var2.getMetrics(var0);
         try {
            Method var3 = Display.class.getMethod("getRawHeight", new Class[0]);
            var5 = ((Integer)Display.class.getMethod("getRawWidth", new Class[0]).invoke(var2, new Object[0])).intValue();
            var4 = ((Integer)var3.invoke(var2, new Object[0])).intValue();
         } catch (Exception var6) {
            var5 = var0.widthPixels;
            var4 = var0.heightPixels;
         }
      }
      var1.setScreenWidthInPixels(var5);
      var1.setScreenHeightInPixels(var4);
      return var1;
   }

   public static boolean isExternalMemoryAvailable() {
      // $FF: Couldn't be decompiled
     return true;
   }

   public static boolean isPrimaryExternalStorageRemovable() {
      boolean var1;
      try {
         var1 = Environment.isExternalStorageRemovable();
      } catch (Exception var2) {
         var1 = false;
      }

      return var1;
   }
}
