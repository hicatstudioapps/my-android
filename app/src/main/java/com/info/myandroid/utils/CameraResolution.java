package com.info.myandroid.utils;

public class CameraResolution implements Comparable<CameraResolution> {
   private int resolutionHeight = 0;
   private int resolutionWidth = 0;

   public CameraResolution(int resolutionHeight, int resolutionWidth) {
      this.resolutionHeight = resolutionHeight;
      this.resolutionWidth = resolutionWidth;
   }

   public int compareTo(CameraResolution var1) {
      return this.resolutionHeight * this.resolutionWidth - var1.resolutionHeight * var1.resolutionWidth;
   }

   public int getResolutionHeight() {
      return this.resolutionHeight;
   }

   public float getResolutionInMegaPixels() {
      return (float)this.resolutionWidth * (float)this.resolutionHeight / 1000000.0F;
   }

   public float getResolutionInPixels() {
      return (float)this.resolutionWidth * (float)this.resolutionHeight;
   }

   public int getResolutionWidth() {
      return this.resolutionWidth;
   }

   public void setResolutionHeight(int var1) {
      this.resolutionHeight = var1;
   }

   public void setResolutionWidth(int var1) {
      this.resolutionWidth = var1;
   }

   public String toString() {
      return "CameraResolution: Width x Height = " + this.resolutionWidth + " x " + this.resolutionHeight;
   }
}
