package com.info.myandroid.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.widget.ImageView;
import android.widget.TextView;

import com.info.myandroid.MyAndroidApplication;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.WeakHashMap;

@SuppressWarnings("ResourceType")
public class ImageLoader {
   private Drawable defaultImage = null;
   private Map imageViewsMap = Collections.synchronizedMap(new WeakHashMap());
   private LruCache memoryCache = null;
   private Map queueMap = Collections.synchronizedMap(new WeakHashMap());

   public ImageLoader(Context var1, Drawable var2) {
      this.init(var1, var2);
   }

   // $FF: synthetic method
   static Map access$2(ImageLoader var0) {
      return var0.queueMap;
   }

   // $FF: synthetic method
   static LruCache access$3(ImageLoader var0) {
      return var0.memoryCache;
   }

   private boolean imageViewReused(PhotoToLoad var1) {
      String var2 = (String)this.imageViewsMap.get(var1.imageView);
      boolean var3;
      if(var2 != null && var2.equals(var1.cellUniqueKey)) {
         var3 = false;
      } else {
         var3 = true;
      }

      return var3;
   }

   private void init(Context var1, Drawable var2) {
      this.memoryCache = new LruCache(1048576 * ((ActivityManager)var1.getSystemService("activity")).getMemoryClass() / 8);
      this.defaultImage = var2;
   }

   private void queuePhoto(String numero, ImageView imagen, String nombre, TextView var4) {
      Integer var5 = (Integer)this.queueMap.get(numero);
      if(var5 == null || var5.intValue() == 0) {
         this.queueMap.put(numero, Integer.valueOf(0));
         (new LoadBitmapTask()).execute(new Object[]{numero, imagen, nombre, var4});
         this.queueMap.put(numero, Integer.valueOf(1));
      }

   }

   public void cleanImageCache() {
      try {
         if(this.memoryCache != null) {
            this.memoryCache.evictAll();
         }
      } catch (Exception var2) {
         ;
      }

      if(this.imageViewsMap != null) {
         this.imageViewsMap.clear();
      }

      if(this.queueMap != null) {
         this.queueMap.clear();
      }

   }

   public void displayImage(String number, ImageView imageView, String name, TextView var4) {
      Object var6 = null;
      this.imageViewsMap.put(imageView, number);
      Bitmap var5 = (Bitmap)var6;
      if(number != null) {
         var5 = (Bitmap)var6;
         if(number.length() > 0) {
            var5 = (Bitmap)this.memoryCache.get(number);
         }
      }

      if(var5 != null) {
         imageView.setImageBitmap(var5);
         var4.setText("");
         var4.setVisibility(4);
      } else {
         imageView.setImageDrawable(this.defaultImage);
         var4.setText(name);
         var4.setVisibility(0);
         if(number != null && number.length() > 0) {
            this.queuePhoto(number, imageView, name, var4);
         }
      }

   }

   class LoadBitmapTask extends AsyncTask<Object,Void,TransitionDrawable> {
      private PhotoToLoad photoToLoad = null;

      protected TransitionDrawable doInBackground(Object... var1) {
         Resources var3 = MyAndroidApplication.getAppContext().getResources();
         Object var2 = null;
         this.photoToLoad = ImageLoader.this.new PhotoToLoad((String)var1[0], (ImageView)var1[1], (String)var1[2], (TextView)var1[3]);
         TransitionDrawable var5;
         if(ImageLoader.this.imageViewReused(this.photoToLoad)) {
            var5 = null;
         } else {
            Bitmap var4 = CallContentManager.getContactPhotoImage(this.photoToLoad.cellUniqueKey);
            if(var4 == null) {
               var5 = null;
            } else {
               var5 = (TransitionDrawable)var2;
               if(var4 != null) {
                  var5 = new TransitionDrawable(new Drawable[]{ImageLoader.this.defaultImage, new BitmapDrawable(var3, var4)});
                  var5.setCrossFadeEnabled(true);
               }
            }
         }

         return var5;
      }

      protected void onPostExecute(TransitionDrawable transitiondrawable) {
         // $FF: Couldn't be decompiled
         if (imageViewReused(photoToLoad))
         {
            queueMap.remove(photoToLoad.cellUniqueKey);
            return;
         }
         if (transitiondrawable != null)
         {
            try
            {
               photoToLoad.imageView.setImageDrawable(transitiondrawable);
               photoToLoad.imageView.setVisibility(0);
               photoToLoad.textViewName.setVisibility(4);
               transitiondrawable.startTransition(500);
//               transitiondrawable = (BitmapDrawable)transitiondrawable.getDrawable(1);
               memoryCache.put(photoToLoad.cellUniqueKey, ((BitmapDrawable)transitiondrawable.getDrawable(1)).getBitmap());
               queueMap.put(photoToLoad.cellUniqueKey, Integer.valueOf(2));
               return;
            }
            // Misplaced declaration of an exception variable
            catch (Exception ex)
            {
               queueMap.remove(photoToLoad.cellUniqueKey);
            }
            return;
         }
         photoToLoad.imageView.setImageDrawable(defaultImage);
         photoToLoad.textViewName.setText(photoToLoad.contactName);
         photoToLoad.textViewName.setVisibility(0);
         photoToLoad.textViewName.bringToFront();
         queueMap.put(photoToLoad.cellUniqueKey, Integer.valueOf(3));
         return;
      }
   }

   private class PhotoToLoad {
      public String cellUniqueKey = null;
      public String contactName = null;
      public ImageView imageView = null;
      public TextView textViewName = null;

      public PhotoToLoad(String var2, ImageView var3, String var4, TextView var5) {
         this.cellUniqueKey = var2;
         this.imageView = var3;
         this.contactName = var4;
         this.textViewName = var5;
      }
   }
}
