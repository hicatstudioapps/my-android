package com.info.myandroid.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.AdRequest;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.info.myandroid.MyAndroidApplication;
import com.info.myandroid.R;

public class AppUtil {
   public static boolean areTwoExternalMemoryCardsAvailable(String var0) {

      return false;
   }

   public static void closeCursor(Cursor var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (Exception var1) {
            ;
         }
      }

   }

   public static void closeInputStream(InputStream var0) {
      if(var0 != null) {
         try {
            var0.close();
         } catch (Exception var1) {
            ;
         }
      }

   }

   public static boolean compareArrays(String[] var0, String[] var1) {
      byte var3 = 5;
      boolean var4 = false;
      if(var0 != null && var1 != null) {
         if(var0.length == var1.length) {
            var4 = true;
         }

         int var2 = var0.length;
         if(var2 > 5) {
            var2 = var3;
         }

         for(int var5 = 0; var5 < var2; ++var5) {
            if(!var0[var5].equals(var1[var5])) {
               var4 = false;
               break;
            }
         }
      } else {
         var4 = false;
      }

      return var4;
   }

   public static boolean containsData(String var0) {
      boolean var1 = false;
      if(var0 == null || var0.trim().equals("")) {
         var1 = true;
      }

      boolean var2;
      if(var1) {
         var2 = false;
      } else {
         var2 = true;
      }

      return var2;
   }

   public static String convertFirstCharOfWordsToUpperCase(String param0) {
      // $FF: Couldn't be decompiled
      String temp=param0.charAt(0)+"";
      return param0.replace(param0.charAt(0),temp.toUpperCase().charAt(0));
   }

   public static AdRequest getAdRequest() {
      return (new AdRequest.Builder()).build();
   }

   public static Locale getApplicationLocale() {
      Locale var1 = Locale.getDefault();
      Locale var0 = var1;
      if(!isSupportedLanguage(var1)) {
         var0 = Locale.ENGLISH;
      }
      return var0;
   }

   public static String getCallDuration(long var0) {
      Context var5 = MyAndroidApplication.getAppContext();
      String var2 = null;
      var0 *= 1000L;
      int var6 = (int)(var0 / 1000L % 60L);
      int var7 = (int)(var0 / 60000L % 60L);
      int var8 = (int)(var0 / 3600000L);
      String var4 = var5.getString(R.string.call_in_seconds);
      String var3 = var5.getString(R.string.call_in_minutes);
      String var9 = var5.getString(R.string.call_in_hours);
      if(var0 < 60000L) {
         var2 = numberFormatting(var6) + " " + var4;
      } else if(var0 >= 60000L && var0 < 300000L) {
         var2 = numberFormatting(var7) + " " + var3 + " " + numberFormatting(var6) + " " + var4;
      } else if(var0 >= 300000L && var0 < 3600000L) {
         var2 = numberFormatting(var7) + " " + var3;
      } else if(var0 >= 3600000L) {
         var2 = numberFormatting(var8) + " " + var9 + " " + numberFormatting(var7) + " " + var3;
      }

      return var2;
   }

   public static String getCallDurationForArabic(long var0) {
      Context var4 = MyAndroidApplication.getAppContext();
      String var2 = null;
      var0 *= 1000L;
      int var8 = (int)(var0 / 1000L % 60L);
      int var6 = (int)(var0 / 60000L % 60L);
      int var7 = (int)(var0 / 3600000L);
      String var5 = var4.getString(R.string.call_in_seconds);
      String var3 = var4.getString(R.string.call_in_minutes);
      String var9 = var4.getString(R.string.call_in_hours);
      if(var0 < 60000L) {
         var2 = var5 + " " + numberFormatting(var8);
      } else if(var0 >= 60000L && var0 < 300000L) {
         var2 = var5 + " " + numberFormatting(var8) + " " + var3 + " " + numberFormatting(var6);
      } else if(var0 >= 300000L && var0 < 3600000L) {
         var2 = var3 + " " + numberFormatting(var6);
      } else if(var0 >= 3600000L) {
         var2 = var3 + " " + var3 + " " + var9 + " " + numberFormatting(var7);
      }

      return var2;
   }

   public static String getCallTime(long var0) {
      return getFormattedDate(var0, "h:mm a");
   }

   public static Calendar getCurrentDayBeginTime() {
      Calendar var0 = Calendar.getInstance();
      var0.set(11, 0);
      var0.set(12, 0);
      var0.set(13, 0);
      var0.set(14, 0);
      return var0;
   }

   public static String getFormattedDate(long var0, String var2) {
      Object var3 = null;
      MyAndroidApplication.getAppContext();

      try {
         var2 = (new SimpleDateFormat(var2, getApplicationLocale())).format(new Date(var0));
      } catch (Exception var4) {
         var2 = (String)var3;
      }

      return var2;
   }

   public static int getImageResourceId(String var0) {
      Context var2 = MyAndroidApplication.getAppContext();
      String var1 = var2.getPackageName();
      return var2.getResources().getIdentifier(var0, "drawable", var1);
   }

   @TargetApi(17)
   public static boolean getLayoutDirection(Context var0) {
      boolean var3 = false;
      int var1 = VERSION.SDK_INT;
      boolean var2 = var3;
      if(isSupportedLanguage(Locale.getDefault())) {
         var2 = var3;
         if(var1 >= 17) {
            var2 = var3;
            if(var0.getResources().getConfiguration().getLayoutDirection() == 1) {
               var2 = true;
            }
         }
      }

      return var2;
   }

   public static boolean isCurrentVersionICSAndAbove() {
      boolean var2 = false;

      int var1;
      try {
         var1 = VERSION.SDK_INT;
      } catch (Exception var3) {
         return var2;
      }

      if(var1 >= 14) {
         var2 = true;
      }

      return var2;
   }

   public static boolean isEmpty(List var0) {
      boolean var1 = false;
      if(var0 == null || var0.size() == 0) {
         var1 = true;
      }

      return var1;
   }

   public static boolean isNotEmpty(List var0) {
      boolean var1;
      if(isEmpty(var0)) {
         var1 = false;
      } else {
         var1 = true;
      }

      return var1;
   }

   public static boolean isOnline(Context var0) {
      NetworkInfo var2 = ((ConnectivityManager)var0.getSystemService("connectivity")).getActiveNetworkInfo();
      boolean var1;
      if(var2 != null && var2.isConnected()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   public static boolean isSupportedLanguage(Locale var0) {
      boolean var4 = false;
      String[] var1 = AppConstants.supportedLangs;
      int var3 = var1.length;

      for(int var2 = 0; var2 < var3; ++var2) {
         if(var1[var2].equalsIgnoreCase(var0.getLanguage())) {
            var4 = true;
            break;
         }
      }

      return var4;
   }

   public static String numberFormatting(int var0) {
      String var1;
      try {
         NumberFormat var3 = NumberFormat.getInstance(getApplicationLocale());
         var3.setGroupingUsed(false);
         var1 = var3.format((long)var0);
      } catch (Exception var2) {
         var1 = String.valueOf(var0);
      }

      return var1;
   }

   public static String removeEndZerosFromSize(String param0) {
      return param0;
   }

   public static String removePrefix(String var0, String var1) {
      String var2 = var0;
      if(containsData(var0)) {
         var2 = var0;
         if(containsData(var1)) {
            int var3 = var1.length();
            int var4 = var0.length();
            var2 = var0;
            if(var0.startsWith(var1)) {
               var2 = var0;
               if(var4 > var3) {
                  var2 = var0.substring(var3);
               }
            }
         }
      }

      return var2;
   }

   public static String removePrefixSuffix(String var0, String var1) {
      return removeSuffix(removePrefix(var0, var1), var1);
   }

   public static String removeSuffix(String var0, String var1) {
      String var2 = var0;
      if(containsData(var0)) {
         var2 = var0;
         if(containsData(var1)) {
            int var4 = var1.length();
            int var3 = var0.length();
            var2 = var0;
            if(var0.endsWith(var1)) {
               var2 = var0;
               if(var3 > var4) {
                  var2 = var0.substring(0, var3 - var4);
               }
            }
         }
      }

      return var2;
   }

   public static String seperateValueBySpace(String param0) {
      return param0;
   }

   public static String toLocaleBasedDecimalFormat(double var0) {
      String var2;
      try {
         DecimalFormat var4 = (DecimalFormat)NumberFormat.getNumberInstance(getApplicationLocale());
         var4.applyPattern("##.#");
         var2 = var4.format(var0);
      } catch (Exception var3) {
         var2 = String.valueOf(var0);
      }

      return var2;
   }

   public static String toLocaleBasedDecimalFormat(float var0) {
      String var1;
      try {
         DecimalFormat var3 = (DecimalFormat)NumberFormat.getNumberInstance(getApplicationLocale());
         var3.applyPattern("##.#");
         var1 = var3.format((double)var0);
      } catch (Exception var2) {
         var1 = String.valueOf(var0);
      }

      return var1;
   }

   public static String toLocaleBasedNumberConversion(double var0) {
      String var2;
      try {
         NumberFormat var4 = NumberFormat.getInstance(getApplicationLocale());
         var4.setGroupingUsed(false);
         var2 = var4.format(var0);
      } catch (Exception var3) {
         var2 = String.valueOf(var0);
      }

      return var2;
   }

   public static String toLocaleBasedNumberConversion(int var0) {
      String var1;
      try {
         NumberFormat var3 = NumberFormat.getInstance(getApplicationLocale());
         var3.setGroupingUsed(false);
         var1 = var3.format((long)var0);
      } catch (Exception var2) {
         var1 = String.valueOf(var0);
      }

      return var1;
   }

   public static void updateConfigurtionWithLocale() {
      try {
         Configuration var0 = new Configuration();
         Context var1 = MyAndroidApplication.getAppContext();
         if(!isSupportedLanguage(Locale.getDefault())) {
            var0.locale = getApplicationLocale();
            var1.getResources().updateConfiguration(var0, (DisplayMetrics)null);
         }
      } catch (Exception var2) {
         ;
      }

   }
}
