package com.info.myandroid.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class CameraDetail {
   private String cameraPosition = null;
   private ArrayList cameraResoutions = null;
   private int cameraStatus = 2;

   public String getCameraPosition() {
      return this.cameraPosition;
   }

   public ArrayList getCameraResoutions() {
      return this.cameraResoutions;
   }

   public int getCameraStatus() {
      return this.cameraStatus;
   }

   public CameraResolution getMaxCameraResolution() {
      Object var2 = null;
      CameraResolution var1 = (CameraResolution)var2;
      if(this.cameraResoutions != null) {
         var1 = (CameraResolution)var2;
         if(this.cameraResoutions.size() > 0) {
            var1 = (CameraResolution)Collections.max(this.cameraResoutions);
         }
      }

      return var1;
   }

   public float getMaxCameraResolutionInMegaPixels() {
      CameraResolution var2 = this.getMaxCameraResolution();
      float var1 = 0.0F;
      if(var2 != null) {
         var1 = (float)var2.getResolutionWidth() * (float)var2.getResolutionHeight() / 1000000.0F;
      }
      return var1;
   }

   public CameraResolution getSecondMaxCameraResolution() {
      Collections.sort(this.cameraResoutions);
      int var1 = this.cameraResoutions.size();
      if(var1 >= 2) {
         var1 -= 2;
      } else {
         --var1;
      }

      return (CameraResolution)this.cameraResoutions.get(var1);
   }

   public boolean isBackCamera() {
      boolean var2 = false;
      boolean var1 = var2;
      if(this.cameraPosition != null) {
         var1 = var2;
         if(this.cameraPosition.equals("CAMERA_POSITION_BACK")) {
            var1 = true;
         }
      }

      return var1;
   }

   public boolean isCameraDetailAvailable() {
      boolean var2 = false;
      boolean var1;
      if(this.cameraStatus == 1) {
         var1 = var2;
         if(this.cameraResoutions != null) {
            var1 = var2;
            if(this.cameraResoutions.size() > 0) {
               var1 = true;
            }
         }
      } else {
         var1 = var2;
         if(this.cameraStatus == 2) {
            var1 = true;
         }
      }

      return var1;
   }

   public void setCameraPosition(String var1) {
      this.cameraPosition = var1;
   }

   public void setCameraResoutions(ArrayList var1) {
      this.cameraResoutions = var1;
   }

   public void setCameraStatus(int var1) {
      this.cameraStatus = var1;
   }

   public String toString() {
      String var1 = "########## CameraDetail Begin ########## \nCamera Position: " + this.cameraPosition + "\n";
      String var2 = var1;
      if(this.cameraResoutions != null) {
         Iterator var4 = this.cameraResoutions.iterator();

         while(var4.hasNext()) {
            CameraResolution var3 = (CameraResolution)var4.next();
            if(var3 != null) {
               var1 = var1 + var3 + "\n";
            }
         }

         var2 = var1;
      }

      return var2 + "########## CameraDetail End ##########";
   }
}
