//package com.info.myandroid.utils;
//
//import android.view.View;
//import android.view.animation.AccelerateInterpolator;
//import android.view.animation.AlphaAnimation;
//import android.view.animation.Animation;
//import android.view.animation.AnimationSet;
//import android.view.animation.DecelerateInterpolator;
//import android.view.animation.Interpolator;
//import android.view.animation.TranslateAnimation;
//import android.view.animation.Animation.AnimationListener;
//import android.widget.ViewAnimator;
//
//
//@SuppressWarnings("ResourceType")
//public class AnimationFactory {
//
//   public static void fadeIn(View var0) {
//      if(var0 != null) {
//         var0.startAnimation(fadeInAnimation(500L, var0));
//      }
//
//   }
//
//   public static Animation fadeInAnimation(long var0, long var2) {
//      AlphaAnimation var4 = new AlphaAnimation(0.0F, 1.0F);
//      var4.setInterpolator(new DecelerateInterpolator());
//      var4.setDuration(var0);
//      var4.setStartOffset(var2);
//      return var4;
//   }
//
//   public static Animation fadeInAnimation(long var0, final View var2) {
//      Animation var3 = fadeInAnimation(500L, 0L);
//      var3.setAnimationListener(new AnimationListener() {
//         public void onAnimationEnd(Animation var1) {
//            var2.setVisibility(0);
//         }
//
//         public void onAnimationRepeat(Animation var1) {
//         }
//
//         public void onAnimationStart(Animation var1) {
//            var2.setVisibility(8);
//         }
//      });
//      return var3;
//   }
//
//   public static void fadeInThenOut(final View var0, long var1) {
//      if(var0 != null) {
//         var0.setVisibility(0);
//         AnimationSet var4 = new AnimationSet(true);
//         Animation[] var3 = fadeInThenOutAnimation(500L, var1);
//         var4.addAnimation(var3[0]);
//         var4.addAnimation(var3[1]);
//         var4.setAnimationListener(new AnimationListener() {
//            public void onAnimationEnd(Animation var1) {
//               var0.setVisibility(8);
//            }
//
//            public void onAnimationRepeat(Animation var1) {
//            }
//
//            public void onAnimationStart(Animation var1) {
//               var0.setVisibility(0);
//            }
//         });
//         var0.startAnimation(var4);
//      }
//
//   }
//
//   public static Animation[] fadeInThenOutAnimation(long var0, long var2) {
//      return new Animation[]{fadeInAnimation(var0, 0L), fadeOutAnimation(var0, var0 + var2)};
//   }
//
//   public static void fadeOut(View var0) {
//      if(var0 != null) {
//         var0.startAnimation(fadeOutAnimation(500L, var0));
//      }
//
//   }
//
//   public static Animation fadeOutAnimation(long var0, long var2) {
//      AlphaAnimation var4 = new AlphaAnimation(1.0F, 0.0F);
//      var4.setInterpolator(new AccelerateInterpolator());
//      var4.setStartOffset(var2);
//      var4.setDuration(var0);
//      return var4;
//   }
//
//   public static Animation fadeOutAnimation(long var0, final View var2) {
//      Animation var3 = fadeOutAnimation(500L, 0L);
//      var3.setAnimationListener(new AnimationListener() {
//          public void onAnimationEnd(Animation var1) {
//              var2.setVisibility(8);
//          }
//
//          public void onAnimationRepeat(Animation var1) {
//          }
//
//          public void onAnimationStart(Animation var1) {
//              var2.setVisibility(0);
//          }
//      });
//      return var3;
//   }
//
//   public static Animation[] flipAnimation(View var0, View var1, FlipDirection var2, long var3, Interpolator var5) {
//      float var7 = (float)var0.getWidth() / 2.0F;
//      float var6 = (float)var0.getHeight() / 2.0F;
//      FlipAnimation var8 = new FlipAnimation(var2.getStartDegreeForFirstView(), var2.getEndDegreeForFirstView(), var7, var6, 0.75F, FlipAnimation.ScaleUpDownEnum.SCALE_DOWN);
//      var8.setDuration(var3);
//      var8.setFillAfter(true);
//      Object var9;
//      if(var5 == null) {
//         var9 = new AccelerateInterpolator();
//      } else {
//         var9 = var5;
//      }
//
//      var8.setInterpolator((Interpolator)var9);
//      AnimationSet var10 = new AnimationSet(true);
//      var10.addAnimation(var8);
//      FlipAnimation var11 = new FlipAnimation(var2.getStartDegreeForSecondView(), var2.getEndDegreeForSecondView(), var7, var6, 0.75F, FlipAnimation.ScaleUpDownEnum.SCALE_UP);
//      var11.setDuration(var3);
//      var11.setFillAfter(true);
//      var9 = var5;
//      if(var5 == null) {
//         var9 = new AccelerateInterpolator();
//      }
//
//      var11.setInterpolator((Interpolator)var9);
//      var11.setStartOffset(var3);
//      AnimationSet var12 = new AnimationSet(true);
//      var12.addAnimation(var11);
//      return new Animation[]{var10, var12};
//   }
//
//   public static void flipTransition(ViewAnimator var0, FlipDirection var1) {
//      View var3 = var0.getCurrentView();
//      int var5 = var0.getDisplayedChild();
//      int var4 = (var5 + 1) % var0.getChildCount();
//      View var2 = var0.getChildAt(var4);
//      if(var4 < var5) {
//         var1 = var1.theOtherDirection();
//      }
//
//      Animation[] var6 = flipAnimation(var3, var2, var1, 250L, (Interpolator)null);
//      var0.setOutAnimation(var6[0]);
//      var0.setInAnimation(var6[1]);
//      var0.showNext();
//   }
//
//   public static Animation inFromLeftAnimation(long var0, Interpolator var2) {
//      TranslateAnimation var4 = new TranslateAnimation(2, -1.0F, 2, 0.0F, 2, 0.0F, 2, 0.0F);
//      var4.setDuration(var0);
//      Object var3 = var2;
//      if(var2 == null) {
//         var3 = new AccelerateInterpolator();
//      }
//
//      var4.setInterpolator((Interpolator)var3);
//      return var4;
//   }
//
//   public static Animation inFromRightAnimation(long var0, Interpolator var2) {
//      TranslateAnimation var4 = new TranslateAnimation(2, 1.0F, 2, 0.0F, 2, 0.0F, 2, 0.0F);
//      var4.setDuration(var0);
//      Object var3 = var2;
//      if(var2 == null) {
//         var3 = new AccelerateInterpolator();
//      }
//
//      var4.setInterpolator((Interpolator) var3);
//      return var4;
//   }
//
//   public static Animation inFromTopAnimation(long var0, Interpolator var2) {
//      TranslateAnimation var4 = new TranslateAnimation(2, 0.0F, 2, 0.0F, 2, -1.0F, 2, 0.0F);
//      var4.setDuration(var0);
//      Object var3 = var2;
//      if(var2 == null) {
//         var3 = new AccelerateInterpolator();
//      }
//
//      var4.setInterpolator((Interpolator) var3);
//      return var4;
//   }
//
//   public static Animation outToLeftAnimation(long var0, Interpolator var2) {
//      TranslateAnimation var4 = new TranslateAnimation(2, 0.0F, 2, -1.0F, 2, 0.0F, 2, 0.0F);
//      var4.setDuration(var0);
//      Object var3 = var2;
//      if(var2 == null) {
//         var3 = new AccelerateInterpolator();
//      }
//
//      var4.setInterpolator((Interpolator)var3);
//      return var4;
//   }
//
//   public static Animation outToRightAnimation(long var0, Interpolator var2) {
//      TranslateAnimation var4 = new TranslateAnimation(2, 0.0F, 2, 1.0F, 2, 0.0F, 2, 0.0F);
//      var4.setDuration(var0);
//      Object var3 = var2;
//      if(var2 == null) {
//         var3 = new AccelerateInterpolator();
//      }
//
//      var4.setInterpolator((Interpolator) var3);
//      return var4;
//   }
//
//   public static Animation outToTopAnimation(long var0, Interpolator var2) {
//      TranslateAnimation var4 = new TranslateAnimation(2, 0.0F, 2, 0.0F, 2, 0.0F, 2, -1.0F);
//      var4.setDuration(var0);
//      Object var3 = var2;
//      if(var2 == null) {
//         var3 = new AccelerateInterpolator();
//      }
//
//      var4.setInterpolator((Interpolator)var3);
//      return var4;
//   }
//
//   public enum FlipDirection {
//       // $FF: synthetic field
//       private int[] $SWITCH_TABLE$com$innovationm$myandroid$anim$AnimationFactory$FlipDirection;
//       private FlipDirection ENUM$VALUES[];
//       public FlipDirection LEFT_RIGHT;
//       public FlipDirection RIGHT_LEFT;
//
//       private FlipDirection(int code){
//
//       }
//       // $FF: synthetic method
//       int[] $SWITCH_TABLE$com$innovationm$myandroid$anim$AnimationFactory$FlipDirection() {
//           int[] var0 = $SWITCH_TABLE$com$innovationm$myandroid$anim$AnimationFactory$FlipDirection;
//           if (var0 == null) {
//               var0 = new int[values().length];
//
//               try {
//                   var0[LEFT_RIGHT.ordinal()] = 1;
//               } catch (NoSuchFieldError var3) {
//                   ;
//               }
//
//               try {
//                   var0[RIGHT_LEFT.ordinal()] = 2;
//               } catch (NoSuchFieldError var2) {
//                   ;
//               }
//
//               $SWITCH_TABLE$com$innovationm$myandroid$anim$AnimationFactory$FlipDirection = var0;
//           }
//
//           return var0;
//       }
//
//
//      public float getEndDegreeForFirstView() {
//         float var1;
//         switch($SWITCH_TABLE$com$innovationm$myandroid$anim$AnimationFactory$FlipDirection()[this.ordinal()]) {
//         case 1:
//            var1 = 90.0F;
//            break;
//         case 2:
//            var1 = -90.0F;
//            break;
//         default:
//            var1 = 0.0F;
//         }
//
//         return var1;
//      }
//
//      public float getEndDegreeForSecondView() {
//         return 0.0F;
//      }
//
//      public float getStartDegreeForFirstView() {
//         return 0.0F;
//      }
//
//      public float getStartDegreeForSecondView() {
//         float var1;
//         switch($SWITCH_TABLE$com$innovationm$myandroid$anim$AnimationFactory$FlipDirection()[this.ordinal()]) {
//         case 1:
//            var1 = -90.0F;
//            break;
//         case 2:
//            var1 = 90.0F;
//            break;
//         default:
//            var1 = 0.0F;
//         }
//
//         return var1;
//      }
//
//      public FlipDirection theOtherDirection() {
//         FlipDirection var1;
//         switch($SWITCH_TABLE$com$innovationm$myandroid$anim$AnimationFactory$FlipDirection()[this.ordinal()]) {
//         case 1:
//            var1 = RIGHT_LEFT;
//            break;
//         case 2:
//            var1 = LEFT_RIGHT;
//            break;
//         default:
//            var1 = null;
//         }
//
//         return var1;
//      }
//   }
//}
