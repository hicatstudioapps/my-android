package com.info.myandroid.utils;

import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.info.myandroid.MyAndroidApplication;

public class MyAppsTaskManager {
   public static List filterAppList(List var0, String var1) {
      ArrayList var2 = new ArrayList();
      if(var0 != null && var1 != null) {
         var1 = var1.toLowerCase(AppUtil.getApplicationLocale());
         Iterator var3 = var0.iterator();

         while(var3.hasNext()) {
            InstalledApplicationInfo var4 = (InstalledApplicationInfo)var3.next();
            String var5 = var4.getApplicationName();
            if(var5 != null && var5.toLowerCase(AppUtil.getApplicationLocale()).contains(var1)) {
               var2.add(var4);
            }
         }
      }

      return var2;
   }

   private static long getAppInstallationDateInMilliseconds(ResolveInfo var0, PackageManager var1) {
      Object var2 = null;

      PackageInfo var4;
      try {
         var4 = var1.getPackageInfo(var0.activityInfo.packageName, 0);
      } catch (NameNotFoundException var3) {
         var3.printStackTrace();
         var4 = (PackageInfo)var2;
      }

      return var4.firstInstallTime;
   }

   public static ArrayList getInstalledApplications(MyAppsSortFieldEnum var0, SortDirectionEnum var1) {
      ArrayList var4 = InstalledApplicationManager.getInstalledApplications();
      ArrayList var3 = new ArrayList();
      PackageManager var2 = MyAndroidApplication.getAppContext().getPackageManager();
      Iterator var12 = var4.iterator();

      while(var12.hasNext()) {
         ResolveInfo var5 = (ResolveInfo)var12.next();
         String var6 = getVisibleAppName(var5, var2);
         long var9 = getAppInstallationDateInMilliseconds(var5, var2);
         ActivityInfo var8 = var5.activityInfo;
         boolean var11 = isSystemApp(var8);
         InstalledApplicationInfo var7 = new InstalledApplicationInfo();
         var7.setAppPackageName(var8.packageName);
         var7.setAppMainActivityClassName(var8.name);
         var7.setApplicationName(var6);
         var7.setInstallationDate(var9);
         var7.setSystemApp(var11);
         var7.setResolveInfo(var5);
         var3.add(var7);
      }

      sortAppList(var0, var1, var3);
      return var3;
   }

   private static String getVisibleAppName(ResolveInfo var0, PackageManager var1) {
      ActivityInfo var4 = var0.activityInfo;
      String var3 = null;
      String var2 = var3;
      if(var4 != null) {
         CharSequence var7 = var4.loadLabel(var1);
         var2 = var3;
         if(var7 != null) {
            var2 = var7.toString();
         }
      }

      var3 = var2;
      if(var2 == null) {
         CharSequence var5 = var0.loadLabel(var1);
         var3 = var2;
         if(var5 != null) {
            var3 = var5.toString();
         }
      }

      String var6 = var3;
      if(var3 == null) {
         var6 = "";
      }

      return var6;
   }

   private static boolean isSystemApp(ActivityInfo var0) {
      boolean var1 = false;
      if((var0.applicationInfo.flags & 1) == 1) {
         var1 = true;
      }

      return var1;
   }

   public static void sortAppList(MyAppsSortFieldEnum var0, SortDirectionEnum var1, List var2) {
      if(var2 != null && var0 != null && var1 != null) {
         if(var0 == MyAppsSortFieldEnum.APPLICATION_NAME) {
            Collections.sort(var2, new AppNameComparator(var1));
         } else if(var0 == MyAppsSortFieldEnum.INSTALLATION_DATE) {
            Collections.sort(var2, new AppInstallationDateComparator(var1));
         }
      }

   }
}
