package com.info.myandroid.utils;

public enum MyAppsSortFieldEnum {
   APPLICATION_NAME(1),
   INSTALLATION_DATE(2);

   private final int code;

   private MyAppsSortFieldEnum(int var3) {
      this.code = var3;
   }

   public int getCode() {
      return this.code;
   }
}
