package com.info.myandroid.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.info.myandroid.MyAndroidApplication;
import com.info.myandroid.R;

@SuppressWarnings("ResourceType")
public class InstalledApplicationsAdapter extends BaseAdapter implements Filterable {
   private List currentApplicationList;
   private SortDirectionEnum currentSortDirection;
   private MyAppsSortFieldEnum currentSortField;
   private LayoutInflater layoutInflater = null;
   private String myAndroidAppPackageName = null;
   private List originalApplicationList;
   PackageManager packageManager = null;
   private int lastPosition=-1;

   public InstalledApplicationsAdapter(LayoutInflater var1, List var2) {
      this.currentSortField = MyAppsSortFieldEnum.APPLICATION_NAME;
      this.currentSortDirection = SortDirectionEnum.ASCENDING;
      Context var3 = MyAndroidApplication.getAppContext();
      this.packageManager = var3.getPackageManager();
      this.layoutInflater = var1;
      this.currentApplicationList = var2;
      this.originalApplicationList = var2;
      this.myAndroidAppPackageName = var3.getPackageName();
   }

   private void handleUninstallApp(View var1) {
      InstalledApplicationInfo var2 = (InstalledApplicationInfo)var1.getTag();
      if(var2.isSystemApp()) {
         this.showUninstallAppMessage();
      } else {
         this.startUninstallApp(var2.getAppPackageName());
      }

   }

   private boolean isOtherApp(String var1) {
      boolean var3 = true;
      boolean var2 = var3;
      if(var1 != null) {
         var2 = var3;
         if(var1.equals(this.myAndroidAppPackageName)) {
            var2 = false;
         }
      }

      return var2;
   }

   private void launchApplication(View var1) {
      InstalledApplicationInfo var2 = (InstalledApplicationInfo)var1.getTag();
      String var5 = var2.getAppPackageName();
      String var3 = var2.getAppMainActivityClassName();
      Intent var6 = new Intent();
      var6.setClassName(var5, var3);
      var6.addFlags(269484032);

      try {
         MyAndroidApplication.getAppContext().startActivity(var6);
      } catch (Exception var4) {
         var4.printStackTrace();
      }

   }

   private void showUninstallAppMessage() {
      Toast.makeText(MyAndroidApplication.getAppContext(), R.string.popup_message_cannot_uninstall, 1).show();
   }

   private void startUninstallApp(String var1) {
      Context var2 = MyAndroidApplication.getAppContext();
      Intent var4 = new Intent("android.intent.action.DELETE", Uri.parse("package:" + var1));
      var4.addFlags(270532608);
      if(var4 != null) {
         try {
            var2.startActivity(var4);
         } catch (Exception var3) {
            var3.printStackTrace();
         }
      }

   }

   public void cleanAdpater() {
      this.layoutInflater = null;
   }

   public int getCount() {
      int var1 = 0;
      if(this.currentApplicationList != null) {
         var1 = this.currentApplicationList.size();
      }

      return var1;
   }

   public List getCurrentApplicationList() {
      return this.currentApplicationList;
   }

   public Filter getFilter() {
      return new AppsFilter((AppsFilter)null);
   }

   public Object getItem(int var1) {
      Object var2 = null;
      if(this.currentApplicationList != null) {
         var2 = this.currentApplicationList.get(var1);
      }

      return var2;
   }

   public long getItemId(int var1) {
      return (long)var1;
   }

   public List getOriginalApplicationList() {
      return this.originalApplicationList;
   }

   public View getView(int var1, View var2, ViewGroup var3) {
      View var7 = var2;
      var2 = var2;
      if(var7 == null) {
         var2 = this.layoutInflater.inflate(R.layout.my_apps_list_item, (ViewGroup)null);
      }

      final InstalledApplicationInfo var8 = (InstalledApplicationInfo)this.getItem(var1);
      if(var8 != null) {
         ImageView var4 = (ImageView)var2.findViewById(R.id.imageViewAPKIcon);
         Drawable var5 = var8.getResolveInfo().loadIcon(this.packageManager);
         boolean var6 = this.isOtherApp(var8.getAppPackageName());
         if(var5 != null) {
            if(var5 instanceof BitmapDrawable) {
               var4.setImageBitmap(((BitmapDrawable)var5).getBitmap());
            } else {
               var4.setImageDrawable(var5);
            }
         }

         RelativeLayout var9 = (RelativeLayout)var2.findViewById(R.id.relativeLayoutAppListItemContainer);
         if(var6) {
            var9.setOnClickListener(new OnClickListener() {
               public void onClick(View var1) {
                  var1.setTag(var8);
                  InstalledApplicationsAdapter.this.launchApplication(var1);
               }
            });
         } else {
            var9.setOnClickListener((OnClickListener)null);
         }

            TextView var10 = (TextView)var2.findViewById(R.id.appname);
         String var11 = var8.getApplicationName();
         if(var11 != null) {
            var10.setText(var11.trim());
         }

         String var12 = AppUtil.getFormattedDate(var8.getInstallationDate(), "d MMM yy");
         ((TextView)var2.findViewById(R.id.textViewInstallationDate)).setText(var12);
         var4 = (ImageView)var2.findViewById(R.id.imageViewDelete);
         if(var6) {
            var4.setVisibility(0);
            var4.setOnClickListener(new OnClickListener() {
               public void onClick(View var1) {
                  var1.setTag(var8);
                  InstalledApplicationsAdapter.this.handleUninstallApp(var1);
               }
            });
         } else {
            var4.setVisibility(4);
         }
      }
      Animation animation = AnimationUtils.loadAnimation(MyAndroidApplication.getAppContext(), (var1 > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
      animation.setDuration(500);
      var2.startAnimation(animation);
      lastPosition = var1;
      return var2;
   }

   public void setApplicationList(List var1) {
      this.originalApplicationList = var1;
   }

   public void setCurrentApplicationList(List var1) {
      this.currentApplicationList = var1;
   }

   public void setCurrentSortCriteria(MyAppsSortFieldEnum var1, SortDirectionEnum var2) {
      this.currentSortField = var1;
      this.currentSortDirection = var2;
   }

   public void setLayoutInflater(LayoutInflater var1) {
      this.layoutInflater = var1;
   }

   private class AppsFilter extends Filter {
      private AppsFilter() {
      }

      // $FF: synthetic method
      AppsFilter(AppsFilter var2) {
         this();
      }

      protected FilterResults performFiltering(CharSequence var1) {
         FilterResults var2 = new FilterResults();
         if(InstalledApplicationsAdapter.this.originalApplicationList != null) {
            List var3;
            if(var1 != null && !var1.toString().equals("")) {
               var3 = MyAppsTaskManager.filterAppList(InstalledApplicationsAdapter.this.originalApplicationList, var1.toString());
            } else {
               var3 = InstalledApplicationsAdapter.this.originalApplicationList;
            }

            MyAppsTaskManager.sortAppList(InstalledApplicationsAdapter.this.currentSortField, InstalledApplicationsAdapter.this.currentSortDirection, var3);
            var2.values = var3;
            var2.count = var3.size();
         }

         return var2;
      }

      protected void publishResults(CharSequence var1, FilterResults var2) {
         if(var2 != null) {
            Object var3 = var2.values;
            if(var3 != null && var3 instanceof List) {
               InstalledApplicationsAdapter.this.currentApplicationList = (List)var3;
               InstalledApplicationsAdapter.this.notifyDataSetChanged();
            }
         }

      }
   }
}
