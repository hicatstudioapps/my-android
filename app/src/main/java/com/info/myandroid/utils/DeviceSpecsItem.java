package com.info.myandroid.utils;

public class DeviceSpecsItem {
   private String displayText;
   private int drawableId;
   private int itemType;

   public String getDisplayText() {
      return this.displayText;
   }

   public int getDrawableId() {
      return this.drawableId;
   }

   public int getItemType() {
      return this.itemType;
   }

   public void setDisplayText(String var1) {
      this.displayText = var1;
   }

   public void setDrawableId(int var1) {
      this.drawableId = var1;
   }

   public void setItemType(int var1) {
      this.itemType = var1;
   }
}
