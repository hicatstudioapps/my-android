package com.info.myandroid.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class CameraTaskManager {
   private static long calculateAverageCameraPictureSize(float var0) {
      return 0L;
   }

   public static DeviceCameras getDeviceCamerasInfo() {
      DeviceCameras var0 = null;
      boolean var2 = PersistenceManager.isCameraInfoAvailableFromPersistentCache();
      if(var2) {
         var0 = PersistenceManager.getDeviceCamerasInfoFromPersistentCache();
      }

      DeviceCameras var1;
      if(var2 && var0 != null) {
         var1 = var0;
         if(var0.isCameraInfoAvailable()) {
            return var1;
         }
      }

      var0 = DeviceCameraManager.getDeviceCamerasInfo();
      var1 = var0;
      if(var0 != null) {
         PersistenceManager.persistDeviceCameraInfo(var0);
         var1 = var0;
      }

      return var1;
   }

   private static ArrayList getMatchingPhotoCategoryInfo(ArrayList var0, ArrayList var1) {
      ArrayList var2 = new ArrayList();
      Iterator var3 = var1.iterator();

      while(var3.hasNext()) {
         PhotoCategoryInfo var4 = (PhotoCategoryInfo)var3.next();
         if(var0 != null && isPhotoResolutionOnCamera(var4, var0)) {
            var2.add(var4);
         }
      }

      return var2;
   }

   private static PhotoCategoryInfo getMaxPhotoCategoryInfo(ArrayList var0) {
      Object var2 = null;
      PhotoCategoryInfo var1 = (PhotoCategoryInfo)var2;
      if(var0 != null) {
         var1 = (PhotoCategoryInfo)var2;
         if(var0.size() > 0) {
            var1 = (PhotoCategoryInfo)Collections.max(var0);
         }
      }

      return var1;
   }

   private static PhotoCategoryInfo getMissingPhotoCategoryInfo(CameraDetail var0, ArrayList var1) {
      Object var5 = null;
      PhotoCategoryInfo var4 = (PhotoCategoryInfo)var5;
      if(var1 != null) {
         var4 = (PhotoCategoryInfo)var5;
         if(var1.size() > 0) {
            PhotoCategoryInfo var7 = (PhotoCategoryInfo)Collections.max(var1);
            float var3 = var7.getResolutionInPixels();
            CameraResolution var6 = var0.getMaxCameraResolution();
            float var2 = 0.0F;
            if(var6 != null) {
               var2 = var6.getResolutionInPixels();
            }

            var4 = (PhotoCategoryInfo)var5;
            if(var3 > var2) {
               var4 = var7;
            }
         }
      }

      return var4;
   }

   private static long getPhotoAverageFileSize(PhotoCategoryInfo var0, boolean var1) {
      long var3 = 0L;
      if(var0 != null) {
         String var2 = var0.getResolutionInWidthAndHeight();
         long var5 = PersistenceManager.getAverageSizeCameraPhoto(var2, var1);
         var3 = var5;
         if(var5 == 0L) {
            var5 = var0.getAverageSizePhoto();
            var3 = var5;
            if(var0.getCountOfPhotos() >= 25) {
               var3 = var5;
               if(var5 > 0L) {
                  PersistenceManager.persistAverageSizeCameraPhoto(var2, var1, var5);
                  var3 = var5;
               }
            }
         }
      }

      return var3;
   }

   private static PhotoCategoryInfo getPhotoCategoryInfoWithHighestPhotoCount(PhotoCategoryInfo var0, PhotoCategoryInfo var1) {
      if(var0 != null && var1 != null) {
         if(var0.compareTo(var1) <= 0) {
            var0 = var1;
         }
      } else if(var0 == null) {
         var0 = var1;
      }

      return var0;
   }

   public static PhotoRemainingContainer getRemainingPhotos(DeviceCameras var0) throws CameraDirectoryNotAvailableException {
      String var2 = DeviceManager.getCameraPhotoLocation();
      long var3 = PersistenceManager.getAvailableMemorySize(var2);
      PhotoRemainingContainer var1 = new PhotoRemainingContainer();
      if(var0 != null) {
         var1 = getRemainingPhotos(var0, CameraContentManager.getAggregatedPhotoInfo(var2), var3);
      }

      return var1;
   }

   private static PhotoRemainingContainer getRemainingPhotos(DeviceCameras var0, ArrayList var1, long var2) {
      PhotoCategoryInfo var4 = null;
      PhotoCategoryInfo var6 = null;
      Object var7 = null;
      PhotoRemainingContainer var9 = new PhotoRemainingContainer();
      boolean var13 = false;
      boolean var12 = var13;
      PhotoCategoryInfo var5 = var4;
      long var14;
      int var19;
      if(var0.getBackCameraStatus() == 1) {
         CameraDetail var8 = var0.getBackCameraDetail();
         var12 = var13;
         var5 = var4;
         if(var8 != null) {
            var5 = getMaxPhotoCategoryInfo(getMatchingPhotoCategoryInfo(var8.getCameraResoutions(), var1));
            var4 = var6;
            if(var1 != null) {
               var4 = var6;
               if(var1.size() > 0) {
                  var4 = getMissingPhotoCategoryInfo(var8, var1);
               }
            }

            var6 = getPhotoCategoryInfoWithHighestPhotoCount(var5, var4);
            var12 = var13;
            var5 = var6;
            if(var6 != null) {
               var14 = getPhotoAverageFileSize(var6, false);
               var12 = var13;
               var5 = var6;
               if(var14 > 0L) {
                  var19 = (int)(var2 / var14);
                  PhotoRemaining var17 = new PhotoRemaining();
                  var17.setCountPhotoRemaining(var19);
                  var17.setCameraPixels(var6.getResolutionInMegaPixels());
                  if(var4 != null) {
                     var17.setOverrideDeviceCameraResolution(true);
                     var17.setOverridenCameraPixels(var4.getResolutionInMegaPixels());
                  }

                  var9.setBackCameraPhotoRemaining(var17);
                  var12 = true;
                  var5 = var6;
               }
            }
         }
      }

      if(var0.getFrontCameraStatus() == 1) {
         CameraDetail var11 = var0.getFrontCameraDetail();
         if(var11 != null) {
            ArrayList var10 = getMatchingPhotoCategoryInfo(var11.getCameraResoutions(), var1);
            PhotoCategoryInfo var18 = getMaxPhotoCategoryInfo(var10);
            if(var12) {
               var6 = (PhotoCategoryInfo)var7;
               var4 = var18;
               if(var18 != null) {
                  var6 = (PhotoCategoryInfo)var7;
                  var4 = var18;
                  if(var18.equals(var5)) {
                     var4 = getSecondMaxPhotoCategoryInfo(var10);
                     var6 = (PhotoCategoryInfo)var7;
                  }
               }
            } else {
               var6 = (PhotoCategoryInfo)var7;
               var4 = var18;
               if(var0.getBackCameraStatus() == 2) {
                  var6 = getMissingPhotoCategoryInfo(var11, var1);
                  var4 = getPhotoCategoryInfoWithHighestPhotoCount(var18, var6);
               }
            }

            if(var4 != null) {
               var14 = getPhotoAverageFileSize(var4, true);
               if(var14 > 0L) {
                  var19 = (int)(var2 / var14);
                  PhotoRemaining var16 = new PhotoRemaining();
                  var16.setCountPhotoRemaining(var19);
                  var16.setCameraPixels(var4.getResolutionInMegaPixels());
                  if(var6 != null) {
                     var16.setOverrideDeviceCameraResolution(true);
                     var16.setOverridenCameraPixels(var6.getResolutionInMegaPixels());
                  }

                  var9.setFrontCameraPhotoRemaining(var16);
               }
            }
         }
      }

      return var9;
   }

   private static PhotoCategoryInfo getSecondMaxPhotoCategoryInfo(ArrayList var0) {
      Object var2 = null;
      PhotoCategoryInfo var1 = (PhotoCategoryInfo)var2;
      if(var0 != null) {
         var1 = (PhotoCategoryInfo)var2;
         if(var0.size() > 1) {
            Collections.sort(var0);
            var1 = (PhotoCategoryInfo)var0.get(var0.size() - 2);
         }
      }

      return var1;
   }

   private static boolean isPhotoResolutionOnCamera(PhotoCategoryInfo var0, ArrayList var1) {
      boolean var3 = false;
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         CameraResolution var4 = (CameraResolution)var2.next();
         if(var4 != null && (var4.getResolutionWidth() == var0.getPhotoWidth() && var4.getResolutionHeight() == var0.getPhotoHeight() || var4.getResolutionWidth() == var0.getPhotoHeight() && var4.getResolutionHeight() == var0.getPhotoWidth())) {
            var3 = true;
            break;
         }
      }

      return var3;
   }
}
