package com.info.myandroid.utils;

import android.content.pm.ResolveInfo;

public class InstalledApplicationInfo {
   private String appMainActivityClassName;
   private String appPackageName;
   private String applicationName;
   private long installationDate;
   private boolean isSystemApp;
   private ResolveInfo resolveInfo;

   public String getAppMainActivityClassName() {
      return this.appMainActivityClassName;
   }

   public String getAppPackageName() {
      return this.appPackageName;
   }

   public String getApplicationName() {
      return this.applicationName;
   }

   public long getInstallationDate() {
      return this.installationDate;
   }

   public ResolveInfo getResolveInfo() {
      return this.resolveInfo;
   }

   public boolean isSystemApp() {
      return this.isSystemApp;
   }

   public void setAppMainActivityClassName(String var1) {
      this.appMainActivityClassName = var1;
   }

   public void setAppPackageName(String var1) {
      this.appPackageName = var1;
   }

   public void setApplicationName(String var1) {
      this.applicationName = var1;
   }

   public void setInstallationDate(long var1) {
      this.installationDate = var1;
   }

   public void setResolveInfo(ResolveInfo var1) {
      this.resolveInfo = var1;
   }

   public void setSystemApp(boolean var1) {
      this.isSystemApp = var1;
   }
}
