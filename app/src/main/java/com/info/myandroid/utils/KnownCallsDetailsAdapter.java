package com.info.myandroid.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.info.myandroid.MyAndroidApplication;
import com.info.myandroid.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

public class KnownCallsDetailsAdapter extends BaseAdapter {
   private final DisplayImageOptions options;
   private ArrayList calls;
   private int currentFlipIndex = -1;
   private int height;
   private ImageLoader imageLoader = null;
   private LayoutInflater inflater;
   private boolean isCurrentFlipActive = false;
   private int previousFlipIndex = -1;
   Context context;
   public KnownCallsDetailsAdapter() {
      context = MyAndroidApplication.getAppContext();
      this.inflater = LayoutInflater.from(context);
      this.imageLoader = new ImageLoader(context, context.getResources().getDrawable(R.drawable.profile_pic));
      this.options = new DisplayImageOptions.Builder()
              .cacheInMemory(true)
              .showImageOnLoading(R.drawable.ic_launcher)
              .showImageForEmptyUri(R.drawable.ic_launcher)
              .showImageOnFail(R.drawable.ic_launcher)
              .bitmapConfig(Bitmap.Config.RGB_565)
              .build();
      this.calculateHeightOfCells();
   }

   private ViewHolder getViewHolderInstance(View var1) {
      ViewHolder var2 = new ViewHolder();
      var2.viewFlipper = (ViewFlipper)var1.findViewById(R.id.view_flipper);
      var2.contactImage = (ImageView)var1.findViewById(R.id.contact_image);
      var2.contactImage.setMaxHeight(height);
      var2.textViewShortName = (TextView)var1.findViewById(R.id.textViewContactShortName);
      var2.contactName = (TextView)var1.findViewById(R.id.contact_name_known);
      var2.contactNumber = (TextView)var1.findViewById(R.id.contact_number_known);
      var2.numberType = (ImageView)var1.findViewById(R.id.call_type_known);
      var2.totalCalls = (TextView)var1.findViewById(R.id.total_call_count);
      var2.textViewMissedCallCount = (TextView)var1.findViewById(R.id.textViewMissedCallCount);
      var2.textViewIncomingCallCount = (TextView)var1.findViewById(R.id.textViewIncomingCallCount);
      var2.textViewOutgoingCallCount = (TextView)var1.findViewById(R.id.textViewOutgoingCallCount);
      var2.contactNameFlip = (TextView)var1.findViewById(R.id.contact_name_known_flip);
      var2.contactNumberFlip = (TextView)var1.findViewById(R.id.contact_number_known_flip);
      return var2;
   }

   private void prepareCallCountSection(int var1, TextView var2, int var3, int var4) {
      if(var1 == 0) {
         var2.setText("");
         var2.setCompoundDrawablesWithIntrinsicBounds(0, 0, var4, 0);
      } else {
         var2.setText(AppUtil.numberFormatting(var1) + " ");
         var2.setCompoundDrawablesWithIntrinsicBounds(0, 0, var3, 0);
      }

   }

   private void prepareCallsHistoryView(ViewHolder var1, CallContactDetailsData var2, int var3) {
      int var6 = 0;
      var3 = 0;
      int var7 = 0;
      String var4 = var2.getName();
      String var5 = var2.getNumber();
      if(var4 == null) {
         var4 = "";
      }

      if(var5 == null) {
         var5 = "";
      }

      var1.contactNumberFlip.setText(var5);
      var1.contactNameFlip.setText(var4);
      ArrayList var13 = var2.getCallsMultipleDatas();
      int var12 = var13.size();

      int var8;
      int var11;
      for(var8 = 0; var8 < var12; var3 = var11) {
         CallDetailsInfo var15 = (CallDetailsInfo)var13.get(var8);
         int var9 = var7;
         int var10 = var6;
         var11 = var3;
         if(var15 != null) {
            var9 = var15.getCallType();
            if(var9 == 1) {
               var11 = var3 + 1;
               var10 = var6;
               var9 = var7;
            } else if(var9 == 2) {
               var9 = var7 + 1;
               var10 = var6;
               var11 = var3;
            } else {
               var10 = var6 + 1;
               var9 = var7;
               var11 = var3;
            }
         }

         ++var8;
         var7 = var9;
         var6 = var10;
      }

      var1.totalCalls.setText(AppUtil.numberFormatting(var6 + var7 + var3));
      CallDetailsInfo var14 = (CallDetailsInfo)var13.get(0);
      if(var14 != null) {
         var8 = var14.getCallType();
      } else {
         var8 = 0;
      }

      this.prepareCallCountSection(var6, var1.textViewMissedCallCount, R.drawable.perdiad, R.drawable.missed_call_grey);
      this.prepareCallCountSection(var3, var1.textViewIncomingCallCount, R.drawable.entrante, R.drawable.missed_call_grey);
      this.prepareCallCountSection(var7, var1.textViewOutgoingCallCount, R.drawable.realizada, R.drawable.dialled_call_grey);
      switch(var8) {
      case 1:
         var1.numberType.setImageResource(R.drawable.in);
         break;
      case 2:
         var1.numberType.setImageResource(R.drawable.out);
         break;
      default:
         var1.numberType.setImageResource(R.drawable.miss);
      }

   }

   private void prepareContactDetailView(ViewHolder var1, CallContactDetailsData var2, int var3) {
      Object var6 = null;
      String var4 = var2.getName();
      if(var4 == null) {
         var4 = "";
      }

      String var5 = var2.getNumber();
      if(var5 == null) {
         var5 = "";
      }

      var1.contactNumber.setText(var5);
      var1.contactName.setText(var4);
      String var7 = (String)var6;
      if(var4 != null) {
         if(var4.contains(" ")) {
            var7 = var4.substring(0, var4.indexOf(" "));
         } else {
            var7 = var4;
         }
      }
//      Bitmap  b=CallContentManager.getContactPhotoImage("");
//      if(b!=null)
//      var1.contactImage.setImageBitmap(b);
//      Glide.with(context).load(b).into(var1.contactImage);
      this.imageLoader.displayImage(var5, var1.contactImage, var7, var1.textViewShortName);
//         ViewGroup.LayoutParams param= var1.contactImage.getLayoutParams();
//         param.height=var1.viewFlipper.getLayoutParams().width;
//         var1.contactImage.setLayoutParams(param);
//      Uri u=getPhotoUri(fetchContactIdFromPhoneNumber(var1.contactNumber.getText().toString()));
//      if(u != null)
//         Glide.with(context).load(getPhotoUri(fetchContactIdFromPhoneNumber(var1.contactNumber.getText().toString()))).into(var1.contactImage);
//         var1.contactImage.setImageURI(getPhotoUri(fetchContactIdFromPhoneNumber(var1.contactNumber.getText().toString())));
//      Glide.with(context).load(getPhotoUri(fetchContactIdFromPhoneNumber(""))).into(var1.contactImage);

   }
   public long fetchContactIdFromPhoneNumber(String phoneNumber) {
      Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
              Uri.encode(phoneNumber));
      Cursor cursor = context.getContentResolver().query(uri,
              new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID },
              null, null, null);

      String contactId = "";

      if (cursor.moveToFirst()) {
         do {
            contactId = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup._ID));
         } while (cursor.moveToNext());
      }
      cursor.close();
      return Long.parseLong(contactId);
   }

   public Uri getPhotoUri(long contactId) {
      ContentResolver contentResolver = context.getContentResolver();

      try {
         Cursor cursor = contentResolver
                 .query(ContactsContract.Data.CONTENT_URI,
                         null,
                         ContactsContract.Data.CONTACT_ID
                                 + "="
                                 + contactId
                                 + " AND "

                                 + ContactsContract.Data.MIMETYPE
                                 + "='"
                                 + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                                 + "'", null, null);

         if (cursor != null) {
            if (!cursor.moveToFirst()) {
               cursor.close();
               return null; // no photo
            }
         } else {
            cursor.close();
            return null; // error in cursor process
         }

      } catch (Exception e) {
         e.printStackTrace();

         return null;
      }

      Uri person = ContentUris.withAppendedId(
              ContactsContract.Contacts.CONTENT_URI, contactId);
      return Uri.withAppendedPath(person,
              ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
   }
   private void setTileView(ViewHolder var1, int var2) {
      int var3 = this.getCurrentFlipIndex();
      if(var3 != -1 && var2 != var3) {
         var3 = this.getPreviousFlipIndex();
         if(var3 != -1 && var3 == var2) {
            if(var1.viewFlipper.getDisplayedChild() == 1) {
               //AnimationFactory.flipTransition(var1.viewFlipper, AnimationFactory.FlipDirection.LEFT_RIGHT);
            }
         } else if(var1.viewFlipper.getDisplayedChild() == 1) {
            var1.viewFlipper.setInAnimation((Animation)null);
            var1.viewFlipper.setOutAnimation((Animation)null);
            var1.viewFlipper.setDisplayedChild(0);
         }
      } else if(this.isCurrentFlipActive()) {
         if(var1.viewFlipper.getDisplayedChild() == 0) {
            var1.viewFlipper.setInAnimation((Animation)null);
            var1.viewFlipper.setOutAnimation((Animation)null);
            var1.viewFlipper.setDisplayedChild(1);
         }
      } else if(var1.viewFlipper.getDisplayedChild() == 1) {
         var1.viewFlipper.setInAnimation((Animation)null);
         var1.viewFlipper.setOutAnimation((Animation)null);
         var1.viewFlipper.setDisplayedChild(0);
      }

   }

   public void calculateHeightOfCells() {
      int ScreenHeightInPixels;
      int ScreenWidthInPixels;
      int call_details_grid_cell_spacing;
      int call_details_list_view_margin;
      try
      {
         Resources obj = MyAndroidApplication.getAppContext().getResources();
         call_details_grid_cell_spacing = (int) obj.getDimension(R.dimen.call_details_grid_cell_spacing);
         call_details_list_view_margin = (int)((Resources) (obj)).getDimension(R.dimen.call_details_list_view_margin);
         DeviceScreenSize screeSize = DeviceManager.getDeviceAvailableScreenSize();
         ScreenHeightInPixels = ((DeviceScreenSize) (screeSize)).getScreenHeightInPixels();
         ScreenWidthInPixels = ((DeviceScreenSize) (screeSize)).getScreenWidthInPixels();
      }
      catch (Exception exception)
      {
         return;
      }
      call_details_grid_cell_spacing = call_details_grid_cell_spacing * 2 + call_details_list_view_margin * 4;
//      if (ScreenWidthInPixels > ScreenHeightInPixels)
//      {
         height = (ScreenWidthInPixels - call_details_grid_cell_spacing) / 3;
         return;
//      }
//      height = ((ScreenWidthInPixels * 55) / 100 - call_details_grid_cell_spacing) / 3;
//      return;
   }

   public void cleanImageCache() {
      if(this.imageLoader != null) {
         this.imageLoader.cleanImageCache();
      }

   }

   public ArrayList getCallsInList() {
      return this.calls;
   }

   public int getCount() {
      int var1;
      if(this.calls != null) {
         var1 = this.calls.size();
      } else {
         var1 = 0;
      }

      return var1;
   }

   public int getCurrentFlipIndex() {
      return this.currentFlipIndex;
   }

   public CallContactDetailsData getItem(int var1) {
      CallContactDetailsData var2 = null;
      if(this.calls != null) {
         var2 = (CallContactDetailsData)this.calls.get(var1);
      }

      return var2;
   }

   public long getItemId(int var1) {
      return (long)var1;
   }

   public int getPreviousFlipIndex() {
      return this.previousFlipIndex;
   }

   public View getView(int var1, View var2, ViewGroup var3) {
      ViewHolder var5;
      View var6;
      if(var2 == null) {
         var6 = this.inflater.inflate(R.layout.call_details_known_detail_summary_item, (ViewGroup)null);
         //height=var6.getWidth();
         var6.setLayoutParams(new ViewGroup.LayoutParams(height, height));
         var5 = this.getViewHolderInstance(var6);
         var6.setTag(var5);
      } else {
         ViewHolder var4 = (ViewHolder)var2.getTag();
         var6 = var2;
         var5 = var4;
      }

      CallContactDetailsData var7 = this.getItem(var1);
      if(var7 != null) {
         this.prepareContactDetailView(var5, var7, var1);
         this.prepareCallsHistoryView(var5, var7, var1);
         this.setTileView(var5, var1);
      }
      //var5.viewFlipper.startFlipping();
      return var6;
   }

   public boolean isCurrentFlipActive() {
      return this.isCurrentFlipActive;
   }

   public void setCallsInList(ArrayList var1) {
      this.calls = var1;
   }

   public void setCurrentFlipActive(boolean var1) {
      this.isCurrentFlipActive = var1;
   }

   public void setCurrentFlipIndex(int var1) {
      this.currentFlipIndex = var1;
   }

   public void setPreviousFlipIndex(int var1) {
      this.previousFlipIndex = var1;
   }

   static class ViewHolder {
      public ImageView contactImage;
      public TextView contactName;
      public TextView contactNameFlip;
      public TextView contactNumber;
      public TextView contactNumberFlip;
      public ImageView numberType;
      public TextView textViewIncomingCallCount;
      public TextView textViewMissedCallCount;
      public TextView textViewOutgoingCallCount;
      public TextView textViewShortName;
      public TextView totalCalls;
      public ViewFlipper viewFlipper;
   }
}
