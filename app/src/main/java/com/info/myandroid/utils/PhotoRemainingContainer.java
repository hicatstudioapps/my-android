package com.info.myandroid.utils;

public class PhotoRemainingContainer {
   private PhotoRemaining backCameraPhotoRemaining;
   private PhotoRemaining frontCameraPhotoRemaining;

   public PhotoRemaining getBackCameraPhotoRemaining() {
      return this.backCameraPhotoRemaining;
   }

   public PhotoRemaining getFrontCameraPhotoRemaining() {
      return this.frontCameraPhotoRemaining;
   }

   public void setBackCameraPhotoRemaining(PhotoRemaining var1) {
      this.backCameraPhotoRemaining = var1;
   }

   public void setFrontCameraPhotoRemaining(PhotoRemaining var1) {
      this.frontCameraPhotoRemaining = var1;
   }
}
