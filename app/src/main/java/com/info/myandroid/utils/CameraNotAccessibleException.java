package com.info.myandroid.utils;

public class CameraNotAccessibleException extends Exception {
   private static final long serialVersionUID = 1L;

   public CameraNotAccessibleException() {
   }

   public CameraNotAccessibleException(Exception var1) {
      super(var1);
   }
}
