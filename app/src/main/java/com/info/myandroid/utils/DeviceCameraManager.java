package com.info.myandroid.utils;

import android.hardware.Camera;

import java.util.ArrayList;
import java.util.List;

public class DeviceCameraManager {

   private static void closeCamera(Camera var0) {
      if(var0 != null) {
         try {
            var0.release();
         } catch (Exception var1) {
            ;
         }
      }

   }

   private static CameraDetail getCameraDetail(int param0) {
      // $FF: Couldn't be decompiled
      android.hardware.Camera.CameraInfo camerainfo;
      camerainfo = new android.hardware.Camera.CameraInfo();
      Camera.getCameraInfo(param0, camerainfo);
      CameraDetail cameraDetail= new CameraDetail();

      if(camerainfo==null)
         return cameraDetail;
      if(camerainfo.facing== Camera.CameraInfo.CAMERA_FACING_FRONT)
         cameraDetail.setCameraPosition("CAMERA_POSITION_BACK");
      else
      cameraDetail.setCameraPosition("CAMERA_POSITION_FRONT");
      cameraDetail.setCameraStatus(3);
      try {
         cameraDetail.setCameraResoutions(getCameraResolution(cameraDetail,param0));
      } catch (CameraNotAccessibleException e) {
         e.printStackTrace();
      }
      return cameraDetail;
   }

   private static ArrayList getCameraResolution(CameraDetail param0, int param1) throws CameraNotAccessibleException {
      // $FF: Couldn't be decompiled
      Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
      Camera.getCameraInfo(param1, cameraInfo);
            ArrayList<CameraResolution> result= new ArrayList<>();
         Camera camera = Camera.open(param1);
         Camera.Parameters cameraParams = camera.getParameters();
         List<Camera.Size> pixelCountTemp = cameraParams.getSupportedPictureSizes();
          for (Camera.Size temp : pixelCountTemp) {
           result.add(new CameraResolution(temp.height,temp.width));
          }
         camera.release();
         param0.setCameraStatus(1);
      return result;
   }

   public static DeviceCameras getDeviceCamerasInfo() {

      DeviceCameras devicecameras= new DeviceCameras();
      CameraDetail cameradetail1;
       CameraDetail cameraDetail2;

      int numCam = Camera.getNumberOfCameras();
       if(numCam>1){
           cameradetail1=getCameraDetail(0);
           cameraDetail2=getCameraDetail(1);
           devicecameras.setBackCameraDetail(cameraDetail2);
           devicecameras.setFrontCameraStatus(cameradetail1.getCameraStatus());
           devicecameras.setBackCameraStatus(cameraDetail2.getCameraStatus());
           devicecameras.setFrontCameraDetail(cameradetail1);
       }
       else {
           cameradetail1 = getCameraDetail(0);
           devicecameras.setFrontCameraDetail(cameradetail1);
       }

      return devicecameras;
   }
}
