package com.info.myandroid.utils;

import java.util.ArrayList;

public class CallDetailsData {
   private ArrayList knownContactCallList = null;
   private ArrayList unknownContactCallList = null;

   public ArrayList getKnownContactCallList() {
      return this.knownContactCallList;
   }

   public ArrayList getUnknownContactCallList() {
      return this.unknownContactCallList;
   }

   public void setKnownContactCallList(ArrayList var1) {
      this.knownContactCallList = var1;
   }

   public void setUnknownContactCallList(ArrayList var1) {
      this.unknownContactCallList = var1;
   }
}
