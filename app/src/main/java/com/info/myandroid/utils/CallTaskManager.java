package com.info.myandroid.utils;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.info.myandroid.MyAndroidApplication;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

@SuppressWarnings("ResourceType")
public class CallTaskManager {
   // $FF: synthetic field
   private static int[] $SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow;

   // $FF: synthetic method
   static int[] $SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow() {
      int[] var0 = $SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow;
      if(var0 == null) {
         var0 = new int[CallTimeWindow.values().length];

         try {
            var0[CallTimeWindow.LAST_WEEK.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            var0[CallTimeWindow.TODAY.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            var0[CallTimeWindow.YESTERDAY.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         $SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow = var0;
      }

      return var0;
   }

   public static CallDetailsData analyzeCallLogs(CallTimeWindow param0) {
      // $FF: Couldn't be decompiled
      CallTimeWindow calltimewindow1;
      CallTimeWindow calltimewindow2;
      String s;
      String s1;
      ArrayList<CallContactDetailsData> arraylist;
      CallDetailsInfo calldetailsinfo;
      CallDetailsData calldetailsdata = new CallDetailsData();
      arraylist = new ArrayList();
      ArrayList arraylist1 = new ArrayList();
      long startTime = getCallWindowStartTime(param0);
      long endTime=getCallWindowEndTime(param0);
      HashMap<String,CallContactDetailsData> map= new HashMap<>();
      HashMap<String,CallContactDetailsData> unknow= new HashMap<>();
      Cursor cursor= MyAndroidApplication.getAppContext().getContentResolver().query(android.provider.CallLog.Calls.CONTENT_URI, null, "date BETWEEN ? AND ?", new String[]{
              String.valueOf(startTime), String.valueOf(endTime)
      }, "date DESC");
      int colunm_name=cursor.getColumnIndex("name");
      int colunm_number=cursor.getColumnIndex("number");
      int colunm_date=cursor.getColumnIndex("date");
      int colunm_type=cursor.getColumnIndex("type");
      int colunm_duration=cursor.getColumnIndex("duration");
      for (cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
         String name=cursor.getString(colunm_name);
         String number=cursor.getString(colunm_number);
         long date= cursor.getLong(colunm_date);
         calldetailsinfo= prepareCallDetailsInfoObject(cursor.getInt(colunm_type), date,cursor.getLong(colunm_duration));
         CallContactDetailsData data=(prepareCallContactDetailsObject(name, number));
         if( name==null || name.equals("")){
//            if(unknow.get(number)!=null)
//               ((CallContactDetailsData)unknow.get(number)).getCallsMultipleDatas().add(calldetailsinfo);
//            else
//            {
               ArrayList callInf= new ArrayList();
               callInf.add(calldetailsinfo);
               data.setCallsMultipleDatas(callInf);
            arraylist1.add(data);
              // unknow.put(number,data);
//            }
            continue;
         }
         if(map.get(number)!=null)
            ((CallContactDetailsData)map.get(number)).getCallsMultipleDatas().add(calldetailsinfo);
         else
         {
            ArrayList callInf= new ArrayList();
            callInf.add(calldetailsinfo);
            data.setCallsMultipleDatas(callInf);
            map.put(number,data);
         }
      }
      Iterator<CallContactDetailsData> it=map.values().iterator();
      while(it.hasNext())
      arraylist.add(it.next());
//      Iterator<CallContactDetailsData> it2=unknow.values().iterator();
//      while(it2.hasNext())
//         arraylist1.add(it2.next());
      calldetailsdata.setKnownContactCallList(arraylist);
      calldetailsdata.setUnknownContactCallList(arraylist1);
      return calldetailsdata;
   }

   private static long getCallWindowEndTime(CallTimeWindow var0) {
      Calendar var1 = AppUtil.getCurrentDayBeginTime();
      switch($SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow()[var0.ordinal()]) {
      case 1:
         var1.add(5, 1);
      case 2:
      default:
         break;
      case 3:
         var1.add(5, 1);
      }

      return var1.getTimeInMillis();
   }

   private static long getCallWindowStartTime(CallTimeWindow var0) {
      Calendar var1 = AppUtil.getCurrentDayBeginTime();
      switch($SWITCH_TABLE$com$innovationm$myandroid$util$CallTimeWindow()[var0.ordinal()]) {
      case 1:
      default:
         break;
      case 2:
         var1.add(5, -1);
         break;
      case 3:
         var1.add(5, -6);
      }

      return var1.getTimeInMillis();
   }

   private static Uri getContactPhotoUri(String param0, ContentResolver param1) {
      // $FF: Couldn't be decompiled
      return null;
   }

   private static int isDuplicateCall(ArrayList var0, String var1) {
      byte var6 = -1;
      boolean var5 = false;
      boolean var7 = false;
      String var2 = "";
      int var4 = var6;
      if(var1 != null) {
         if(var1.length() >= 10) {
            var2 = var1.substring(var1.length() - 10, var1.length());
         }

         Iterator var8 = var0.iterator();
         var4 = var6;
         var5 = var7;

         while(var8.hasNext()) {
            boolean var9;
            label27: {
               CallContactDetailsData var3 = (CallContactDetailsData)var8.next();
               if(!var1.equals(var3.getNumber())) {
                  var9 = var5;
                  if(!var2.equals(var3.getNumberLast10Digits())) {
                     break label27;
                  }
               }

               var9 = true;
            }

            int var10 = var4 + 1;
            var5 = var9;
            var4 = var10;
            if(var9) {
               var5 = var9;
               var4 = var10;
               break;
            }
         }
      }

      if(!var5) {
         var4 = -1;
      }

      return var4;
   }

   private static CallContactDetailsData prepareCallContactDetailsObject(String var0, String var1) {
      CallContactDetailsData var5 = new CallContactDetailsData();
      Object var4 = null;
      String var3 = (String)var4;
      if(var1 != null) {
         var3 = (String)var4;
         if(var1.length() >= 10) {
            var3 = var1.substring(var1.length() - 10, var1.length());
         }
      }

      var5.setName(var0);
      var5.setNumber(var1);
      var5.setNumberLast10Digits(var3);
      //var5.getCallsMultipleDatas().add(var2);
      return var5;
   }

   private static CallDetailsInfo prepareCallDetailsInfoObject(int var0, long var1, long var3) {
      CallDetailsInfo var5 = new CallDetailsInfo();
      var5.setCallType(var0);
      var5.setCallDate(var1);
      var5.setCallDuration(var3);
      return var5;
   }
}
