package com.info.myandroid.utils;

import java.util.Date;

public class PhotoCategoryInfo implements Comparable<PhotoCategoryInfo> {

   private long avgCameraPictureSize = 0L;
   private int countOfPhotos = 0;
   private long lastPhotoTakenOn = 0L;
   private int photoHeight = 0;
   private int photoWidth = 0;
   private float totalSizeOfPhotos = 0.0F;

   public PhotoCategoryInfo() {
   }

   public PhotoCategoryInfo(int var1, int var2) {
      this.photoWidth = var1;
      this.photoHeight = var2;
   }

   public int compareTo(PhotoCategoryInfo var1) {
      int var3 = 0;
      byte var2 = 0;
      if(var1 != null) {
         var3 = var1.getCountOfPhotos();
      }

      if(this.countOfPhotos > var3) {
         var2 = 1;
      } else if(this.countOfPhotos < var3) {
         var2 = -1;
      }

      return var2;
   }

   public boolean equals(Object var1) {
      PhotoCategoryInfo var4 = (PhotoCategoryInfo)var1;
      boolean var3 = false;
      boolean var2;
      if(this.photoWidth != var4.getPhotoWidth() || this.photoHeight != var4.getPhotoHeight()) {
         var2 = var3;
         if(this.photoWidth != var4.getPhotoHeight()) {
            return var2;
         }

         var2 = var3;
         if(this.photoHeight != var4.getPhotoWidth()) {
            return var2;
         }
      }

      var2 = true;
      return var2;
   }

   public long getAverageSizePhoto() {
      long var1 = 0L;
      if(this.countOfPhotos > 0) {
         var1 = (long)(this.totalSizeOfPhotos / (float)this.countOfPhotos);
      }

      return var1;
   }

   public long getAvgCameraPictureSize() {
      return this.avgCameraPictureSize;
   }

   public int getCountOfPhotos() {
      return this.countOfPhotos;
   }

   public long getLastPhotoTakenOn() {
      return this.lastPhotoTakenOn;
   }

   public int getPhotoHeight() {
      return this.photoHeight;
   }

   public int getPhotoWidth() {
      return this.photoWidth;
   }

   public float getResolutionInMegaPixels() {
      return (float)this.photoWidth * (float)this.photoHeight / 1000000.0F;
   }

   public float getResolutionInPixels() {
      return (float)this.photoWidth * (float)this.photoHeight;
   }

   public String getResolutionInWidthAndHeight() {
      return this.photoWidth + "x" + this.photoHeight;
   }

   public int hashCode() {
      return this.photoHeight + this.photoWidth;
   }

   public void increasePhotoCount() {
      ++this.countOfPhotos;
   }

   public void increaseTotalSize(int var1) {
      this.totalSizeOfPhotos += (float)var1;
   }

   public void setAvgCameraPictureSize(long var1) {
      this.avgCameraPictureSize = var1;
   }

   public void setCountOfPhotos(int var1) {
      this.countOfPhotos = var1;
   }

   public void setLastPhotoTakenOn(long var1) {
      this.lastPhotoTakenOn = var1;
   }

   public void setPhotoHeight(int var1) {
      this.photoHeight = var1;
   }

   public void setPhotoWidth(int var1) {
      this.photoWidth = var1;
   }

   public String toString() {
      return "#### PhotoInfo - " + this.photoWidth + " x " + this.photoHeight + " | " + new Date(this.lastPhotoTakenOn) + " | " + this.totalSizeOfPhotos + " | " + this.countOfPhotos + " | \n";
   }


}
