package com.info.myandroid.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.info.myandroid.MyAndroidApplication;

/**
 * Created by admin on 12/5/2015.
 */
public class CustomGridContainer extends RelativeLayout {
    public CustomGridContainer(Context context) {
        super(context);
    }

    public CustomGridContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomGridContainer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        MyAndroidApplication.height=getMeasuredWidth();
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
    }
}
