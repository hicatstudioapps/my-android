package com.info.myandroid.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.info.myandroid.MyAndroidApplication;
import com.info.myandroid.R;

import java.util.ArrayList;

@SuppressWarnings("ResourceType")
public class UnknownCallsDetailsAdapter extends BaseAdapter {
   private String callDate;
   private long callDateInMilliSeconds;
   private String callDuration;
   private long callDurationInMilliSeconds;
   private String callTime;
   private ArrayList calls;
   private String currentLocaleLangCode;
   private ImageView imageViewTypeOfCall;
   private LayoutInflater inflater;
   private String phoneNumber;
   private TextView textViewCallDate;
   private TextView textViewCallDuration;
   private TextView textViewCallTime;
   private TextView textViewPhoneNumber;

   public UnknownCallsDetailsAdapter() {
      Context var1 = MyAndroidApplication.getAppContext();
      this.currentLocaleLangCode = DeviceManager.getCurrentLanguageCodeOnDevice();
      this.inflater = LayoutInflater.from(var1);
   }

   public int getCount() {
      return this.calls.size();
   }

   public CallContactDetailsData getItem(int var1) {
      return (CallContactDetailsData)this.calls.get(var1);
   }

   public long getItemId(int var1) {
      return (long)var1;
   }

   public View getView(int var1, View var2, ViewGroup var3) {
      View var5 = var2;
      var2 = var2;
      if(var5 == null) {
         var2 = this.inflater.inflate(R.layout.call_details_unknown_detail_item, (ViewGroup)null);
      }

      this.textViewPhoneNumber = (TextView)var2.findViewById(R.id.textViewPhoneNumber);
      this.textViewCallDate = (TextView)var2.findViewById(R.id.textViewCallDate);
      this.textViewCallDuration = (TextView)var2.findViewById(R.id.textViewCallDuration);
      this.textViewCallTime = (TextView)var2.findViewById(R.id.textViewCallTime);
      this.imageViewTypeOfCall = (ImageView)var2.findViewById(R.id.imageViewTypeOfCall);
      CallContactDetailsData var4 = this.getItem(var1);
      ArrayList var6 = var4.getCallsMultipleDatas();
      this.phoneNumber = var4.getNumber();
      this.textViewPhoneNumber.setText(this.phoneNumber);
      if(var6 != null && var6.size() > 0) {
         this.textViewCallDate.setVisibility( 0);
         this.textViewCallDuration.setVisibility(0);
         this.imageViewTypeOfCall.setVisibility(0);
         CallDetailsInfo var7 = (CallDetailsInfo)var6.get(0);
         this.callDateInMilliSeconds = var7.getCallDate();
         this.callDate = AppUtil.getFormattedDate(this.callDateInMilliSeconds, "d MMM");
         this.textViewCallDate.setText(this.callDate);
         this.callDurationInMilliSeconds = var7.getCallDuration();
         if(this.currentLocaleLangCode.equalsIgnoreCase("ar")) {
            this.callDuration = AppUtil.getCallDurationForArabic(this.callDurationInMilliSeconds);
         } else {
            this.callDuration = AppUtil.getCallDuration(this.callDurationInMilliSeconds);
         }

         this.textViewCallDuration.setText(this.callDuration);
         this.callTime = AppUtil.getCallTime(this.callDateInMilliSeconds);
         this.textViewCallTime.setText(this.callTime);
         switch(var7.getCallType()) {
         case 1:
            this.imageViewTypeOfCall.setImageResource(R.drawable.in);
            break;
         case 2:
            this.imageViewTypeOfCall.setImageResource(R.drawable.out);
            break;
         case 3:
            this.imageViewTypeOfCall.setImageResource(R.drawable.miss);
            break;
         default:
            this.imageViewTypeOfCall.setImageResource(R.drawable.miss);
         }
      } else {
         this.textViewCallDate.setVisibility(4);
         this.textViewCallDuration.setVisibility(4);
         this.imageViewTypeOfCall.setVisibility(4);
      }

      return var2;
   }

   public void setCallsInList(ArrayList var1) {
      this.calls = var1;
   }
}
