package com.info.myandroid.utils;

import java.util.ArrayList;

public class CallContactDetailsData {
   @Override
   public boolean equals(Object o) {
      if(((CallContactDetailsData)o).getNumber().equals(this.getNumber()))
         return true;
      else
         return false;
   }

   private ArrayList<CallDetailsInfo> callsMultipleDatas = new ArrayList();
   private String name;
   private String number;
   private String numberLast10Digits;

   public ArrayList getCallsMultipleDatas() {
      return this.callsMultipleDatas;
   }

   public String getName() {
      return this.name;
   }

   public String getNumber() {
      return this.number;
   }

   public String getNumberLast10Digits() {
      return this.numberLast10Digits;
   }

   public void setCallsMultipleDatas(ArrayList var1) {
      this.callsMultipleDatas = var1;
   }

   public void setName(String var1) {
      this.name = var1;
   }

   public void setNumber(String var1) {
      this.number = var1;
   }

   public void setNumberLast10Digits(String var1) {
      this.numberLast10Digits = var1;
   }
}
