package com.info.myandroid.utils;

public enum CallContactType {
   KNOWN_CALL(1),
   UNKNOWN_CALL(2);

   private final int code;

   private CallContactType(int var3) {
      this.code = var3;
   }

   public int getCode() {
      return this.code;
   }
}
