package com.info.myandroid.utils;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.info.myandroid.R;
import com.lacostra.utils.notification.Permission.PermissionActivity;


public class BaseActivity extends PermissionActivity {
   private void rateApplication() {
      try {
         this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + this.getPackageName())));
      } catch (Exception var2) {
         ;
      }

   }

   private void shareMessage() {
      // $FF: Couldn't be decompiled
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
      AppUtil.updateConfigurtionWithLocale();
   }

   public boolean onCreateOptionsMenu(Menu var1) {
      this.getMenuInflater().inflate(R.menu.menu_menu_screen, var1);
      return true;
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      switch(var1.getItemId()) {
      case R.id.menu_item_share:
         this.shareMessage();
         break;
      case R.id.menu_item_rate:
         this.rateApplication();
      }

      return super.onOptionsItemSelected(var1);
   }
}
