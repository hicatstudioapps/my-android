package com.info.myandroid.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ViewFlipper;

public class ContentViewFlipper extends ViewFlipper {
   public ContentViewFlipper(Context var1) {
      super(var1);
   }

   public ContentViewFlipper(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   protected void onDetachedFromWindow() {
      try {
         super.onDetachedFromWindow();
      } catch (Exception var2) {
         this.stopFlipping();
      }

   }
}
