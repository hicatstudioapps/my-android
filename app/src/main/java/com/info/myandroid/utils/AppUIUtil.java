package com.info.myandroid.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.ProgressBar;
import android.widget.Toast;

public class AppUIUtil {
   public static void closeDialog(Dialog var0) {
      if(var0 != null) {
         try {
            if(var0.isShowing()) {
               var0.dismiss();
            }
         } catch (Exception var1) {
            ;
         }
      }

   }

   public static void hideProgressBar(ProgressBar var0) {
      if(var0 != null && var0.isShown()) {
         var0.setVisibility(4);
      }

   }

   public static AlertDialog showGenericAlertDialog(Context var0, String var1) {
      Builder var2 = new Builder(var0);
      var2.setMessage(var1);
      var2.setCancelable(false);
      var2.setPositiveButton(17039370, new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            var1.dismiss();
         }
      });
      return var2.show();
   }

   public static void showProgressBar(ProgressBar var0) {
      if(var0 != null) {
         var0.setVisibility(0);
      }

   }

   public static void showToastNotification(Context var0, String var1, String var2) {
      showToastNotification(var0, var1, var2, true);
   }

   public static void showToastNotification(Context var0, String var1, String var2, boolean var3) {
      byte var4;
      if(var3) {
         var4 = 1;
      } else {
         var4 = 0;
      }

      if(var2 != null) {
         Toast.makeText(var0, var2 + "\n\n" + var1, var4).show();
      } else {
         Toast.makeText(var0, var1, var4).show();
      }

   }
}
