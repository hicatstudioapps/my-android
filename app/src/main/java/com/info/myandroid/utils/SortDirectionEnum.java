package com.info.myandroid.utils;

public enum SortDirectionEnum {
   ASCENDING(1),
   DESCENDING(2);

   private final int code;

   private SortDirectionEnum(int var3) {
      this.code = var3;
   }

   public int getCode() {
      return this.code;
   }
}
