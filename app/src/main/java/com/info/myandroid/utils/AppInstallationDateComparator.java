package com.info.myandroid.utils;

import java.util.Comparator;

public class AppInstallationDateComparator implements Comparator<InstalledApplicationInfo> {
   private long currentInstallationDateInMillis;
   private long otherInstallationDateInMillis;
   private SortDirectionEnum sortDirectionEnum;

   public AppInstallationDateComparator(SortDirectionEnum var1) {
      this.sortDirectionEnum = var1;
   }

   public int compare(InstalledApplicationInfo var1, InstalledApplicationInfo var2) {
      byte var3 = 0;
      if(this.sortDirectionEnum == SortDirectionEnum.ASCENDING) {
         this.currentInstallationDateInMillis = var1.getInstallationDate();
         this.otherInstallationDateInMillis = var2.getInstallationDate();
      } else {
         this.currentInstallationDateInMillis = var2.getInstallationDate();
         this.otherInstallationDateInMillis = var1.getInstallationDate();
      }

      if(this.currentInstallationDateInMillis < this.otherInstallationDateInMillis) {
         var3 = -1;
      } else if(this.currentInstallationDateInMillis > this.otherInstallationDateInMillis) {
         var3 = 1;
      }

      return var3;
   }
}
