package com.info.myandroid.utils;

public class PhotoRemaining {
   private float cameraPixels;
   private int countPhotoRemaining = -1;
   private boolean overrideDeviceCameraResolution = false;
   private float overridenCameraPixels = 0.0F;

   public float getCameraPixels() {
      return this.cameraPixels;
   }

   public int getCountPhotoRemaining() {
      return this.countPhotoRemaining;
   }

   public float getOverridenCameraPixels() {
      return this.overridenCameraPixels;
   }

   public boolean isOverrideDeviceCameraResolution() {
      return this.overrideDeviceCameraResolution;
   }

   public void setCameraPixels(float var1) {
      this.cameraPixels = var1;
   }

   public void setCountPhotoRemaining(int var1) {
      this.countPhotoRemaining = var1;
   }

   public void setOverrideDeviceCameraResolution(boolean var1) {
      this.overrideDeviceCameraResolution = var1;
   }

   public void setOverridenCameraPixels(float var1) {
      this.overridenCameraPixels = var1;
   }
}
