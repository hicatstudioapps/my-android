package com.info.myandroid.utils;

public class CallDetailsInfo {
   private long callDate;
   private long callDuration;
   private int callType;

   public long getCallDate() {
      return this.callDate;
   }

   public long getCallDuration() {
      return this.callDuration;
   }

   public int getCallType() {
      return this.callType;
   }

   public void setCallDate(long var1) {
      this.callDate = var1;
   }

   public void setCallDuration(long var1) {
      this.callDuration = var1;
   }

   public void setCallType(int var1) {
      this.callType = var1;
   }
}
