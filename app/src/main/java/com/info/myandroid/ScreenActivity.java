package com.info.myandroid;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.info.myandroid.utils.AppUtil;
import com.info.myandroid.utils.DeviceManager;
import com.info.myandroid.utils.DeviceScreenSize;

import badabing.lib.apprater.AppRater;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class ScreenActivity extends AppCompatActivity {

    @InjectView(R.id.size)
    TextView size;
    @InjectView(R.id.resolution)
    TextView resolution;
    @InjectView(R.id.density)
    TextView density;
    private int deviceDensity;
    @InjectView(R.id.textView4)
    TextView aprox;
    private int deviceHeight;
    private int deviceWidth;
    private AdView adView;
    @InjectView(R.id.tut)
    View tut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen);
        AdRequest var13;
        try {
            this.adView = (AdView)this.findViewById(R.id.adView);
            var13 = (new AdRequest.Builder()).build();
            this.adView.loadAd(var13);
            this.adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    adView.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception var8) {
            ;
        }
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.screen_color));
        toolbar.setTitle(R.string.app_screen_size_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setSupportActionBar(toolbar);
        int densityInt=getDeviceDensity();
        if(densityInt <= 120) {
            density.setText(getString(R.string.low_density));
        } else if (densityInt <= 160) {
            density.setText(getString(R.string.medium_density));
        } else if(densityInt <= 240) {
            density.setText(getString(R.string.high_density));
        } else if(densityInt <= 320) {
            density.setText(getString(R.string.extra_high_density));
        } else {
            density.setText(getString(R.string.extra_extra_high));
        }
        aprox.setText(deviceDensity+" "+getString(R.string._approx_));

        DeviceScreenSize var2 = DeviceManager.getDevicePhysicalScreenSize();
        this.deviceHeight = var2.getScreenHeightInPixels();
        this.deviceWidth = var2.getScreenWidthInPixels();

        size.setText(getScreenSizesInInches()+ " "+getString(R.string._inches)+" "+getString(R.string._approx_));
        resolution.setText(this.deviceHeight+" x "+this.deviceWidth);
        tut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ScreenActivity.this,ScreenCaptureActivity.class));
            }
        });
    }
    private int getDeviceDensity() {
        DisplayMetrics var1 = new DisplayMetrics();
        int var2;
        if(this.deviceDensity == 0) {
            this.getWindowManager().getDefaultDisplay().getMetrics(var1);
            var2 = var1.densityDpi;
        } else {
            var2 = this.deviceDensity;
        }
        this.deviceDensity=var2;
        return var2;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu var1) {
        this.getMenuInflater().inflate(R.menu.menu_home_screen, var1);
        return true;

    }
    public boolean onOptionsItemSelected(MenuItem var1) {
        switch (var1.getItemId()) {
            case android.R.id.home:
                finish();
                break;

//      case 16908332:
//         this.toggleDrawer();
//         break;
//      case 2131493078:
//         this.toggleDrawer();
//         break;
            case R.id.menu_item_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                String text = String.format(getString(R.string.share_text), getString(R.string.app_name), "https://play.google.com/store/apps/details?id="+ getPackageName());
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.menu_item_rate:
                AppRater.rateNow(this);
                break;
        }

        return super.onOptionsItemSelected(var1);
    }
    public void rateApplication() {
        try {
            this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + this.getPackageName())));
        } catch (Exception var2) {
            Log.e("MyAndroid", "Error occured while launching play store for rating app", var2);
        }
    }


    public static void shareMessage() {
        // $FF: Couldn't be decompiled
    }
    public String getScreenSizesInInches() {
        DisplayMetrics var1 = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(var1);
        int var2;
        int var3;
        if(this.getResources().getConfiguration().orientation == 2) {
            var2 = (int)var1.xdpi;
            var3 = (int)var1.ydpi;
        } else {
            var3 = (int)var1.xdpi;
            var2 = (int)var1.ydpi;
        }

        return AppUtil.toLocaleBasedDecimalFormat(Math.sqrt(Math.pow((double) this.deviceWidth / (double) var3, 2.0D) + Math.pow((double) this.deviceHeight / (double) var2, 2.0D)));
    }
}
