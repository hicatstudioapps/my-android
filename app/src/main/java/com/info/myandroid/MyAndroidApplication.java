package com.info.myandroid;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MyAndroidApplication extends Application {
   private static Context appContext = null;
   public static InterstitialAd interstitialAd;
   AdRequest request;
   public static Typeface face;
   public static Typeface faceR;
   public static SharedPreferences preferences;
   public static int height=0;


   public static Context getAppContext() {
      return appContext;
   }

   private void handleScreenshotFeature() {
//      if(!PersistenceManager.isScreenshotOptionVerified()) {
//         Intent var1 = new Intent(this, DeviceInfoIntentService.class);
//         var1.putExtra("INTENT_SERVICE_TASK", "GET");
//         this.startService(var1);
//      }

   }

   public void onCreate() {
      super.onCreate();
      appContext=this;
      ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).build();
      ImageLoader.getInstance().init(config);
      interstitialAd=new InterstitialAd(this);
      interstitialAd.setAdUnitId("ca-app-pub-1147824671772507/8119073279");
      request= new AdRequest.Builder().build();
      interstitialAd.setAdListener(new AdListener() {
         @Override
         public void onAdClosed() {
            interstitialAd.loadAd(request);
         }
      });
      interstitialAd.loadAd(request);
//        if(getSharedPreferences("data",MODE_PRIVATE).getBoolean(Constants.CONTENT_READY,false))
//      face= Typeface.createFromAsset(getAssets(),"L.OTF");
//      faceR= Typeface.createFromAsset(getAssets(),"R.OTF");

   }

   public static void showInterstitial(){
      if(interstitialAd.isLoaded()){
         interstitialAd.show();
      }
   }
}
