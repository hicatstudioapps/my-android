package com.info.myandroid;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.info.myandroid.utils.DeviceSpecsItem;
import com.lacostra.utils.notification.Permission.Permission;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.CAMERA;import static android.Manifest.permission.READ_CONTACTS;

import static android.Manifest.permission.READ_CALL_LOG;


public class MenuScreenListAdapter extends RecyclerView.Adapter<MenuScreenListAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List listMenuItems;
    public Context context;
    int lastPosition = -1;

    public MenuScreenListAdapter(MenuActivity var1, ArrayList var2) {
        this.listMenuItems = var2;
        context = var1;
        this.inflater = var1.getLayoutInflater();
    }

    public int getCount() {
        return this.listMenuItems.size();
    }

    public Object getItem(int var1) {
        return this.listMenuItems.get(var1);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_screen_list_item, parent, false);
        return new ViewHolder(view);
    }

    private void setMobileDataEnabled(Context context, boolean enabled) {

        final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            final Class conmanClass = Class.forName(conman.getClass().getName());
            Method [] m=conmanClass.getDeclaredMethods();
            final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            iConnectivityManagerField.setAccessible(true);
            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
            final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
            Method[] mm=iConnectivityManagerClass.getDeclaredMethods();
            final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);
            setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBindViewHolder(MenuScreenListAdapter.ViewHolder holder, int position) {

        DeviceSpecsItem var7 = (DeviceSpecsItem) this.listMenuItems.get(position);
        switch (position) {
            case 0:
                ((TextView) holder.title.findViewById(R.id.textView18)).setText(var7.getDisplayText());
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                // holder.var4.setTextColor(context.getResources().getColor(R.color.app_color));
                // indicator.setBackgroundColor(context.getResources().getColor(R.color.app_color));
                holder.main.setVisibility(View.GONE);
                holder.title.setVisibility(View.VISIBLE);
                break;
            case 1://Wiffi
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.VISIBLE);
                WifiManager wifiManager = (WifiManager) MyAndroidApplication.getAppContext()
                        .getSystemService(Context.WIFI_SERVICE);
                if (wifiManager.isWifiEnabled())
                    ((CheckBox) holder.check).setChecked(true);
                else
                    ((CheckBox) holder.check).setChecked(false);
                ((CheckBox) holder.check).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        boolean x = isChecked;
                        setMobileDataEnabled(MyAndroidApplication.getAppContext(),true);

                        MenuScreenListAdapter.toggleWiFi(isChecked);
                        if (isChecked) {
                            Toast.makeText(MyAndroidApplication.getAppContext(), MyAndroidApplication.getAppContext().getString(R.string.wifi_on), Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(MyAndroidApplication.getAppContext(), MyAndroidApplication.getAppContext().getString(R.string.wifi_off), Toast.LENGTH_SHORT).show();

                    }
                });
                // holder.var4.setTextColor(context.getResources().getColor(R.color.storage_color));
                //indicator.setBackgroundColor(context.getResources().getColor(R.color.storage_color));
                break;
            case 2://bluethoo
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.VISIBLE);
                BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
                if (bluetooth.isEnabled())
                    ((CheckBox) holder.check).setChecked(true);
                else
                    ((CheckBox) holder.check).setChecked(false);
                ((CheckBox) holder.check).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
                        if (isChecked)
                            MenuScreenListAdapter.this.getAct().startActivityForResult(new Intent(
                                    BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
                        else {
                            bluetooth.disable();
                                Toast.makeText(MyAndroidApplication.getAppContext(), MyAndroidApplication.getAppContext().getString(R.string.blu_off), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                // holder.var4.setTextColor(context.getResources().getColor(R.color.call_color));
                //indicator.setBackgroundColor(context.getResources().getColor(R.color.call_color));
                break;
//            case 3://datos moviles
//                holder.var5.setImageResource(var7.getDrawableId());
//                holder.var4.setText(var7.getDisplayText());
//                holder.main.setVisibility(View.VISIBLE);
//                holder.title.setVisibility(View.GONE);
//                holder.check.setVisibility(View.VISIBLE);
//                //holder.var4.setTextColor(context.getResources().getColor(R.color.camera_color));
//                //indicator.setBackgroundColor(context.getResources().getColor(R.color.camera_color));
//                break;
            case 3://title
                ((TextView) holder.title.findViewById(R.id.textView18)).setText(var7.getDisplayText());
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.GONE);
                holder.title.setVisibility(View.VISIBLE);
                break;
            case 4:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                // holder.var4.setTextColor(context.getResources().getColor(R.color.app_color));
                // indicator.setBackgroundColor(context.getResources().getColor(R.color.app_color));
                // holder.main.setVisibility(View.GONE);
                // holder.title.setVisibility(View.VISIBLE);
                break;
            case 5:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                //  holder.var4.setTextColor(context.getResources().getColor(R.color.storage_color));
                // indicator.setBackgroundColor(context.getResources().getColor(R.color.storage_color));
                break;
            case 6:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                //holder.var4.setTextColor(context.getResources().getColor(R.color.call_color));
                // indicator.setBackgroundColor(context.getResources().getColor(R.color.call_color));
                break;
            case 7:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                // holder.var4.setTextColor(context.getResources().getColor(R.color.camera_color));
                // indicator.setBackgroundColor(context.getResources().getColor(R.color.camera_color));
                break;
            case 8:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                // holder.var4.setTextColor(context.getResources().getColor(R.color.internet));
                // indicator.setBackgroundColor(context.getResources().getColor(R.color.internet));
                break;
            case 9:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                // holder.var4.setTextColor(context.getResources().getColor(R.color.screen_size));
                // indicator.setBackgroundColor(context.getResources().getColor(R.color.screen_size));
                break;
            case 10:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                //  holder.var4.setTextColor(context.getResources().getColor(R.color.screen_density));
                //  indicator.setBackgroundColor(context.getResources().getColor(R.color.screen_density));
                break;
            case 11:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                // holder.var4.setTextColor(context.getResources().getColor(R.color.ram_color));
                //indicator.setBackgroundColor(context.getResources().getColor(R.color.ram_color));
                break;
            case 12:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                // holder.var4.setTextColor(context.getResources().getColor(R.color.version_color));
                // indicator.setBackgroundColor(context.getResources().getColor(R.color.version_color));
                break;
            case 13:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                //  holder.var4.setTextColor(context.getResources().getColor(R.color.fabricante_color));
                //  indicator.setBackgroundColor(context.getResources().getColor(R.color.fabricante_color));
                break;
            case 14:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                //  holder.var4.setTextColor(context.getResources().getColor(R.color.fabricante_color));
                //  indicator.setBackgroundColor(context.getResources().getColor(R.color.fabricante_color));
                break;
            case 15:
                holder.var5.setImageResource(var7.getDrawableId());
                holder.var4.setText(var7.getDisplayText());
                holder.main.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.GONE);
                holder.check.setVisibility(View.INVISIBLE);
                //  holder.var4.setTextColor(context.getResources().getColor(R.color.fabricante_color));
                //  indicator.setBackgroundColor(context.getResources().getColor(R.color.fabricante_color));
                break;

        }

    }

    public long getItemId(int var1) {
        return (long) var1;
    }

    @Override
    public int getItemCount() {
        return this.listMenuItems.size();
    }

    public static void toggleWiFi(boolean status) {
        WifiManager wifiManager = (WifiManager) MyAndroidApplication.getAppContext()
                .getSystemService(Context.WIFI_SERVICE);
        if (status == true && !wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        } else if (status == false && wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
    }

    public MenuActivity getAct() {
        return (MenuActivity) context;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView var5;
        TextView var4;
        View indicator;
        View title;
        View check;
        View main;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            var5 = (ImageView) itemView.findViewById(R.id.imageViewIcon);
            var4 = (TextView) itemView.findViewById(R.id.textViewName);
            indicator = itemView.findViewById(R.id.frame);
            title = itemView.findViewById(R.id.title_section);
            check = itemView.findViewById(R.id.toggleButton);
//        check.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View v) {
//              boolean c= ((CheckBox)v).isChecked();
//              if(!((CheckBox)v).isChecked()){
//                 Toast.makeText(MyAndroidApplication.getAppContext(),MyAndroidApplication.getAppContext().getString(R.string.wifi_on),Toast.LENGTH_SHORT).show();
//              }else
//                 Toast.makeText(MyAndroidApplication.getAppContext(),MyAndroidApplication.getAppContext().getString(R.string.wifi_off),Toast.LENGTH_SHORT).show();
//              MenuScreenListAdapter.toggleWiFi(!((CheckBox) v).isChecked());
////              Toast.makeText(MyAndroidApplication.getAppContext(),"Wffi",Toast.LENGTH_SHORT).show();
//              ((CheckBox)v).setChecked(!((CheckBox)v).isChecked());
//           }
//        });
            main = itemView.findViewById(R.id.main);

        }

        @Override
        public void onClick(View v) {
            switch (getAdapterPosition()) {
                case 1:
                    ((CheckBox) check).setChecked(!((CheckBox) check).isChecked());
                    break;
                case 2:
                    ((CheckBox) check).setChecked(!((CheckBox) check).isChecked());
                    break;
//                case 3:
//                    ((CheckBox) check).setChecked(!((CheckBox) check).isChecked());
//                    Toast.makeText(MyAndroidApplication.getAppContext(), "Data", Toast.LENGTH_SHORT).show();
//                    break;
                case 0:
                case 7:
                    if(Permission.hasPermission(context, READ_CONTACTS) && Permission.hasPermission(context,READ_CALL_LOG)){
                        Intent var7 = new Intent(getAct(), HomeActivity.class);
                        var7.putExtra("KEY_POSITION", ((DeviceSpecsItem) listMenuItems.get(getAdapterPosition())).getItemType());
                        getAct().startActivity(var7);}
                    else
                    {
                        ((MenuActivity)context).requestCall();
                    }
                    break;
                case 8:
                    if(Permission.hasPermission(context, CAMERA)){
                        Intent var7 = new Intent(getAct(), HomeActivity.class);
                    var7.putExtra("KEY_POSITION", ((DeviceSpecsItem) listMenuItems.get(getAdapterPosition())).getItemType());
                    getAct().startActivity(var7);}
                    else
                    {
                        ((MenuActivity)context).requestCamera();
                    }
                    break;
                case 3:
                    break;
                default:
                    Intent var7 = new Intent(getAct(), HomeActivity.class);
                    var7.putExtra("KEY_POSITION", ((DeviceSpecsItem) listMenuItems.get(getAdapterPosition())).getItemType());
                    getAct().startActivity(var7);
            }
        }
    }
}
